class Error(Exception):
   """Base class for other exceptions"""
   pass
class PageLoadError(Error):
   """Page did not load"""
   pass
# give more attributes compared to webSraping.py

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException, WebDriverException, ElementClickInterceptedException, ElementNotInteractableException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import Select
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
from requests.exceptions import ProxyError
from requests.exceptions import Timeout
from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry
from bs4 import BeautifulSoup

import traceback
import urllib.request
import json
import tesserocr
from tesserocr import PyTessBaseAPI as api
from tesserocr import PSM
from PIL import ImageFilter
from PIL import ImageEnhance
from PIL import Image

import random
import time
import datetime
import re
import string
import pandas as pd
import requests
import shutil
import glob
"""
List of data fields required as output: # would like to have all of them, if
not let's get all what we can get

1) Provider First Name: String
2) Provider Last Name: String
4) License Number: String
5) License State: String
6) License Type: String
7) License Effective Date
8) License Expiration Date
9) License Status
10) License Board Sanction: conresponding to notes in Mohit's resultFL_example
11) License As of Date
"""
## Write Class
import shutil, os
class Results:
    def __init__(self, state):
        num=0
        dtime=str(datetime.datetime.now().strftime("%Y%m%d"))
        self.fn = 'results/result'+state+'_'+dtime+'.txt'
        self.state = state
        cwd=os.getcwd()
        self.pf= cwd+'/'+self.fn
        
        if os.path.exists(self.pf):
            while os.path.exists(self.pf):
                num+=1
                self.fn = 'results/result'+state+'_'+dtime+'_'+str(num)+'.txt'
                self.pf= cwd+'/'+self.fn
        
    def w(self,data,method='open'):
        if method=='open':     
            with open(self.pf,'w', encoding='utf-8') as fp:
                fp.write(data+'\n')
                
        if method=='add':
            key_list=['num','state','fn','ln','license_number','license_state','first_name','','last_name','license_type','','expire_date','license_status','','board_sanction']
            with open(self.pf,'a+', encoding='utf-8') as fp:
                for key in key_list: 
                    try:
                        if key=='':
                            fp.write('|')
                        else:
                            code=str(data[key]) + '|'
                            fp.write(code.upper())
                    except TypeError as e:
                        print(e)
                        fp.write('|')
                fp.write('\n')

## Save webpage information
def save_info(driver,state,licnum,style='html'):
    dtime=str(datetime.datetime.now().strftime("%Y%m%d"))
    if style=='html':
        fn = 'screenshots/'+dtime+'_'+state+'_'+licnum+'.html'
        cwd=os.getcwd()
        pf= cwd+'/'+fn
        num=0
        if os.path.exists(pf):
            while os.path.exists(pf):
                num+=1
                fn = 'screenshots/'+dtime+'_'+state+'_'+licnum+'_'+str(num)+'.html'
                pf= cwd+'/'+fn
        
        html=driver.page_source
        try:
            with open(fn, "wb") as file:
                file.write(html.encode("ascii", "ignore"))
        except UnicodeEncodeError:
            with open(fn, "wb") as file:
                file.write(html.encode("utf-8", "ignore"))
            
    if style=='screenshot':
        fn = 'screenshots/'+dtime+'_'+state+'_'+licnum+'.png'
        cwd=os.getcwd()
        pf= cwd+'/'+fn
        num=0
        if os.path.exists(pf):
            while os.path.exists(pf):
                num+=1
                fn = 'screenshots/'+dtime+'_'+state+'_'+licnum+'_'+str(num)+'.png'
                pf= cwd+'/'+fn
            
        driver.save_screenshot(fn)
    
    print('Screenshot saved to '+fn+'\n')
    time.sleep(.5)
    return
                
## Start a dictionary to add to
def feature_dict(state,li_number, f_name, l_name):
    d={"last_name": l_name,
        "first_name": f_name,
        "license_type": '',  # corresponding to Occupation
        "license_number": li_number,
        "license_state": state,
        "effective_date": "",  # original issue date
        "expire_date": "",
        "license_status": "",
        "board_sanction": "",
        "License_As_Of_Date": datetime.datetime.now().strftime("%Y/%m/%d")}
    return d

## Get proxy for states that need it
def find_proxy(n=0,get_table=False):
    '''
    Gets a proxy from "https://www.us-proxy.org/" and returns it so it can be used with chrome's proxy settings
    Example....
    PROXY = find_proxy()# IP:PORT or HOST:PORT

    chrome_options = webdriver.ChromeOptions()
    chrome_options.add_argument('--proxy-server=%s' % PROXY)

    chrome = webdriver.Chrome(options=chrome_options)
    chrome.get("https://dhp.virginiainteractive.org/Lookup/Index")
    returns string IP:PORT
    '''
    url="https://www.us-proxy.org/"
    print('in findproxy')
    session = requests.Session()
    retry = Retry(connect=5, backoff_factor=0.5)
    adapter = HTTPAdapter(max_retries=retry)
    session.mount('http://', adapter)
    session.mount('https://', adapter)

    page = session.get(url)
    soup = BeautifulSoup(page.text, 'html.parser')
    table=soup.find_all('table')

    df = pd.read_html(str(table))[0]

    df_https=df.loc[df.Https=='yes'].reset_index(drop=True)

    df_proxy=df_https.loc[df_https.Anonymity!='elite proxy'].reset_index(drop=True)

    if n>=df_proxy.shape[0]:

        n=int(df_proxy.shape[0]-1)
    IP_PORT=str(df_proxy.iloc[n][0])+":"+str(int(df_proxy.iloc[n][1]))
        
    if get_table==True:

        return df_proxy

    return IP_PORT

# Change proxy so that we're not using the same one over and over
def change_proxy(current_proxy,start=0):
    '''
    Changes proxy to be different from current_proxy
    '''

    new_proxy=current_proxy
    df_https=find_proxy(get_table=True)
    i=start
    while (new_proxy==current_proxy) and i<=int(df_https.shape[0]-1):
        print('Finding new proxy')
        new_proxy=str(df_https.iloc[i][0])+":"+str(int(df_https.iloc[i][1]))
        i+=1
    return new_proxy
# Gets download speed
def downspeed(proxy=None):
    '''
    Downloads a file and determines the download speed in KB/s
    '''
    url = "http://speedtest.ftp.otenet.gr/files/test100k.db"
    print('Downloading ....')
    if proxy!=None:
        httpproxy='http://'+proxy
        proxy_dict = {
                  'http': httpproxy,
                  'https': httpproxy,
                    }
        start = time.time()

        try:
            session = requests.Session()
            retry = Retry(connect=5, backoff_factor=0.5)
            adapter = HTTPAdapter(max_retries=retry)
            session.mount('http://', adapter)
            session.mount('https://', adapter)
            file = session.get(url, proxies=proxy_dict,timeout=(1,2))
        except (Timeout,requests.exceptions.ConnectionError):
            print('Too slow')
            return 0
        end = time.time()
    else:
        start = time.time()
        try: 
            session = requests.Session()
            retry = Retry(connect=5, backoff_factor=0.5)
            adapter = HTTPAdapter(max_retries=retry)
            session.mount('http://', adapter)
            session.mount('https://', adapter)
            file = session.get(url, timeout=(1,2))
            
        except (Timeout,requests.exceptions.ConnectionError):
            print('Too slow')
            return 0
        end = time.time()
    print('Downloaded')
    time_difference = end - start
    file_size = int(file.headers['Content-Length'])/1000
    return int(round(file_size / time_difference)) #in KB/s

def get_proxy(url,speed=None,n=0):
    '''
    Cycles through proxies to find one with a faster download speed than the value given.
    '''

    if speed!=None:
        proxy=find_proxy(n)     
        
        try:
            print('Trying Proxy: ', proxy)
            dspeed=downspeed(proxy)
        except Timeout:
            dspeed=0

        a=0
        dspeeds=[]
        proxies=[]
        rand=random.randint(0,4)
        if n==3:
            rand=random.randint(3,10)  
        print('Proxy: ',proxy)
        while dspeed<int(speed):
            if a>4:
                maxspeed=max(dspeeds)
                proxy=proxies[dspeeds.index(maxspeed)]
                break
            print('Speed: '+str(dspeed)+' KB/s','/n')
            print(a,'- Trying proxy: ',proxy)
            
            proxies.append(proxy)
            dspeeds.append(dspeed)
            startnum=a+rand
            proxy=change_proxy(proxy,start=startnum)
            try:
                dspeed=downspeed(proxy)
            except:
                pass
            a+=1
        if 'maxspeed' in locals():
            dspeed=maxspeed
        print('Proxy download speed: '+str(dspeed)+' KB/s')
        return proxy
    else:

        proxy=find_proxy()
        a=0
        worked=False
        while worked==False:
            if a>4:
                proxy=None
                break

            try:
                session = requests.Session()
                retry = Retry(connect=5, backoff_factor=0.5)
                adapter = HTTPAdapter(max_retries=retry)
                session.mount('http://', adapter)
                session.mount('https://', adapter)
                r = session.get(url,timeout=(1,2))

#                 print(r.status_code)
                worked=True
            except (Timeout,requests.exceptions.ConnectionError):
                print(proxy)
                proxy=change_proxy(proxy,start=a)

            a+=1
        return proxy

# Get's a webdriver to steer
def get_driver(url=None, driver_params=[],n=0):
    '''
    Gets driver with parameters outlined in driver_params
    '''
    if n>0:
        driver_params.append('--proxy-server')
    options = webdriver.ChromeOptions()
    if '--headless' in driver_params:
        options.add_argument('--headless')
    if '--no-sandbox' in driver_params:
        options.add_argument('--no-sandbox')
    if '--disable_gpu' in driver_params:
        options.add_argument('--disable-gpu')
    if '--webproxy' in driver_params:
        driver = webdriver.Chrome(options=options)
        return driver
    if '--proxy-server' in driver_params:
        if url==None:
            url = 'https://www.google.com/'
        speed = 40  
        
        if n==0:
            PROXY='207.242.3.35:8080'
        else:
            PROXY=get_proxy(url,speed,n)
#         PROXY=get_proxy(url,speed,n)
        if PROXY!=None:
            options.add_argument('--proxy-server=%s' % PROXY)
        
    driver = webdriver.Chrome(options=options)
    return driver

def try_get_url(wbd,driver_params,url):
    '''
    Tries to open webpage and get results opens a new browser with a different proxy
    '''
    if '--webproxy' in driver_params:
        driver=wbd
        driver.get('https://proxysite.one/')
        time.sleep(pause_time*2)
        
        searchbox=driver.find_element_by_css_selector('#inputurl')
        searchbox.click()
        searchbox.clear()
        searchbox.send_keys(url)
        try:
            driver.find_element_by_id('myproxy-close-bottom').click()
            time.sleep(pause_time)
        except NoSuchElementException:
            try:
                driver.find_element_by_id('myproxy-close-bottom').click()
                time.sleep(1)
            except NoSuchElementException:
                pass
        driver.find_element_by_css_selector('#surfbtn').click()
        time.sleep(pause_time)
    else:
        
        driver=wbd
        try:
            driver.get(url)
            time.sleep(pause_time)
        except TimeoutException:
            try:
                driver.get(url)
                time.sleep(pause_time)
            except TimeoutException:
                pass
            pass
    n=1
    noreach=''
    noint=''
    access=''
    dns=''
    gateway=''
    try:
        gateway=driver.find_element_by_xpath('//*[@id="cf-error-details"]/div[1]/h1/span[1]').text
        print('Gateway Error')
    except NoSuchElementException:
        pass
    
    try:
        noreach=driver.find_element_by_xpath('//*[@id="main-message"]/h1/span').text
        print('Site cannot be reached')
        print(noreach)
    except NoSuchElementException:
        pass
    
    try:
        dns=driver.find_element_by_xpath('//*[@id="titles"]/h1').text
        print('DNS Error')
    except NoSuchElementException:
        pass
    
    try:
        access=driver.find_element_by_xpath('//*[@id="ERR_ACCESS_DENIED"]').text
        print('Access Denied')
    except NoSuchElementException:
        pass
    
    try:
        links=len(driver.find_elements_by_xpath("//a[@href]"))
    except NoSuchElementException:
        links=0
        print('No internet')

    proxychange=False
    try:
        noint=driver.find_element_by_xpath('//*[@id="main-message"]/h1/span').text
        print('No Internet')
    except (NoSuchElementException):
        pass
    if 'Error' in dns:
        print('DNS failed')
        proxychange=True
    if 'internet' in noint:
        print('No Internet')
        proxychange=True
    
    if 'Error' in access:
        print('Access Denied')
        proxychange=True
    
    if 'reached' in noreach:
        proxychange=True
        
    if 'Error' in access:
        print('Gateway Error')
        proxychange=True
        
    while (links<1) or (proxychange==True):
        if n>3:
            driver.quit()
            raise PageLoadError
            return None
        
        print(n,' Page not loading')
        driver.quit()
        driver = get_driver(url,driver_params,n)
        if '--webproxy' in driver_params:
            driver.get('https://proxysite.one/')
            time.sleep(pause_time)

            searchbox=driver.find_element_by_css_selector('#inputurl')
            searchbox.send_keys(url)

            driver.find_element_by_css_selector('#surfbtn').click()
            time.sleep(pause_time)
        else:
            try:
                driver.get(url)
                time.sleep(pause_time)
            except TimeoutException:
                pass
        links+=len(driver.find_elements_by_xpath("//a[@href]"))
        try:
            noreach=driver.find_element_by_xpath('//*[@id="main-message"]/h1/span').text
            print('Site cannot be reached')
        except NoSuchElementException:
            noreach=''
            pass
        try:
            noint=driver.find_element_by_xpath('//*[@id="main-message"]/h1/span').text
        except (NoSuchElementException):
            noint=''
            pass
        
        try:
            access=driver.find_element_by_xpath('//*[@id="ERR_ACCESS_DENIED"]').text
            print('Access Denied')
        except NoSuchElementException:
            access=''
            pass
        try:
            dns=driver.find_element_by_xpath('//*[@id="titles"]/h1').text
        except NoSuchElementException:
            dns=''
            pass
        try:
            gateway=driver.find_element_by_xpath('//*[@id="cf-error-details"]/div[1]/h1/span[1]').text
        except NoSuchElementException:
            gateway=''
            pass
    
        if ('internet' not in noint) and ('Error' not in access) and ('Error' not in dns) and ('reached' not in noreach) and ('Error' not in gateway):
            proxychange=False
            
        n+=1

    return driver

headers="num|state|fn|ln|SbwLicNum|SbwLicState|StateBoardFirst|StateBoardMiddle|StateBoardLast|SbwLicType|SbwLicType2|SbwLicExpDate|SbwLicStatus|SbwLicQualifier|Notes"

loop_break = 50
pause_time = 2 + random.random() * 2

# state of Arkansas
## Test_values li_number, f_name, l_name = "OT-A1315", "Wendi", "Parker"
def scrape_AR(sdata, driver_params, start=0, stop=True):
    try:
        state='AR'
        url = "http://www.armedicalboard.org/Public.aspx"
        urlsw="http://www.accessarkansas.org/swlb/search/index.html"
        driver = get_driver(url,driver_params)
        driver = try_get_url(driver,driver_params,url)
        time.sleep(pause_time)

        def single_scrape(wbd, li_number, f_name, l_name):
            pause_time = 2 + random.random() * 2
            d = feature_dict(state, li_number, f_name, l_name)
            driver=wbd

            wait = WebDriverWait(driver, 10)

            # select the license number radio button
            wait.until(EC.presence_of_element_located((By.ID,
                                                       'ctl00_MainContentPlaceHolder_ucVerifyLicense_rbVerifyLicenseSearch_0'))).click()

            # select the search box
            search_box = driver.find_element_by_id('ctl00_MainContentPlaceHolder_ucVerifyLicense_txtVerifyLicNumLastName')

            # fill search box with license number
            search_box.clear()
            search_box.send_keys(li_number)  # "OT-A1309"

            # click verify button
            driver.find_element_by_id('ctl00_MainContentPlaceHolder_ucVerifyLicense_btnVerifyLicense').click()
            time.sleep(pause_time)

            # extract the main content of the page

            content =  wait.until(EC.presence_of_element_located((By.ID,'ctl00_MainContentPlaceHolder_DetailLicenseVerification'))).text.split('\n')
            # no result case
            if "No Results" in content:
                d['license_type']='None Found'
                return d

            start_num=0
            lic_nums=[]
            for i,c in enumerate(content):
                if "LICENSE NUMBER:" in c.upper():
                    if (c.split(':')[1].split()[0].strip().upper() in li_number) | (li_number in c.split(':')[1].split()[0].strip().upper()):
                        start_num=i

                    lic_nums.append(i)

            end_num=-1
            if (len(lic_nums)>0) and (max(lic_nums)>start_num):
                end_num=[x for x in lic_nums if x>start_num][0]

            content1=content[start_num:end_num]

            for c in content:
                if "NAME:" in c.upper():
                    fullname=c.upper().split(':')[-1]
                    if (l_name.upper() not in fullname) & (f_name.upper() not in fullname):
                        d['license_type']='None Found'
                        return d
                    d['first_name'] = fullname.split(',')[0].split()[0].strip().upper()
                    d['last_name'] = fullname.split(',')[0].split()[-1].strip().upper()

                if "SPECIALTY" in c.upper():
                    d['license_type'] = c.split(':')[-1].strip().upper()

            for c in content1:

                # Extract License type
                if "LICENSE NUMBER:" in c.upper():
                    d['license_number'] = c.split(':')[1].split()[0].strip().upper()

                if "LICENSE STATUS:" in c.upper():
                    d['license_status'] = c.split(':')[1].split()[0].strip().upper()

                if "ISSUE DATE:" in c.upper():
                    d['effective_date'] = c.split(':')[-1].strip().upper()

                if "EXPIRATION DATE:" in c.upper():
                    d['expire_date'] = c.split(':')[-1].strip().upper()

                if 'LICENSE CATEGORY:' in c.upper():
                    d['board_sanction']=c.split(':')[-1].strip().upper()

                if "BOARD HISTORY" in c.upper():
                    d['board_sanction']=content[content.index(c)+2].strip().upper()
            
            save_info(driver,state,li_number)
            return d
        
        def single_scrape_sw(wbd, li_number, f_name, l_name):
            pause_time = 2 + random.random() * 2
            d = feature_dict(state, li_number, f_name, l_name)
            driver=wbd
            
            wait = WebDriverWait(driver, 10)

            # select the reset button
            
            wait.until(EC.presence_of_element_located((By.NAME,'Reset'))).click()

            # select the search box
            search_box = driver.find_element_by_xpath('/html/body/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr/td/table/tbody/tr[3]/td/form/table/tbody/tr[3]/td[2]/div/font/input')

            # fill search box with license number
            search_box.send_keys(l_name)  # "OT-A1309"

            # click verify button
            driver.find_element_by_name('Submit').click()
            time.sleep(pause_time)
            noresult=''
            try:
                results=driver.find_element_by_css_selector('body > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(2) > td > p > font > b').text
            except NoSuchElementException:
                pass
            if 'No' in results:
                return d

            table=driver.find_element_by_css_selector('body > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(2) > td > table').text.split('\n')

            searchname=l_name[0]+l_name[1:].lower()+', '+f_name[0]+f_name[1:].lower()
            link_num=[s[0] for s in table if searchname in s][0]

            link=driver.find_element_by_link_text(link_num).get_attribute('href')
            driver.get(link)
            time.sleep(pause_time)
            content=driver.find_element_by_css_selector('body > table > tbody > tr:nth-child(2) > td > table:nth-child(1) > tbody > tr:nth-child(2) > td > div > table').text.split('\n')

            for c in content:
                # Extract expire date
                if "Expiration" in c:
                    d['expire_date'] = c.split()[-1]
                # Extract license status
                if "Date Issued" in c:
                    d['effective_date'] = c.split('Date Issued')[-1]
                # Extract license type
                if 'Level' in c:
                    d['license_type']='Social Worker: '+c.split()[-1]


            if d['expire_date']!='': 
                currentdate = datetime.datetime.strptime(d['License_As_Of_Date'], "%Y/%m/%d")
                expiredate=datetime.datetime.strptime(d['expire_date'], "%m/%d/%Y")
                if currentdate>expiredate:
                    d['license_status']='Expired'
                else:
                    d['license_status']='Active'
            save_info(driver,state,li_number)
            return d
    
        sdata = sdata.astype(str)
        length=int(sdata.shape[0])
        if stop==True:
            stop=length

        # Write a file for the output

        file=Results(state)
        file.w(headers)
        result=[]  
        number_files=int(stop-start)
        for i in range(start, stop):
            print('processing '+state+ ': '+str(i+1)+' of '+str(number_files),'\n')
            
            if '--proxy-server' in driver_params:
                if i%loop_break==int(loop_break-1):
                    driver.quit()
                    driver = get_driver(url,driver_params)
                    driver = try_get_url(driver,driver_params,url)

            li_number = str(sdata.iloc[i].loc['num'])
            f_name = sdata.iloc[i].loc['fn']
            l_name = sdata.iloc[i].loc['ln']
            #extract the input parameters for the scraper function
            if (len(li_number.split('-')[0])==4) and (len(li_number.split('-')[-1])==1):
                try:
                    print('Social Worker','\n')
                    driver = try_get_url(driver,driver_params,urlsw)
                    output=single_scrape_sw(driver, li_number, f_name, l_name)
                except Exception as inst:

                    result.append((i,inst))
                    driver = try_get_url(driver,driver_params,url)
                    time.sleep(pause_time)
                    continue
            else:
                
                driver = try_get_url(driver,driver_params,url)
                time.sleep(pause_time)
                try:
                    output=single_scrape(driver, li_number, f_name, l_name)
                except Exception as inst:
                    print(inst)
                    result.append((i,inst))
                    driver = try_get_url(driver,driver_params,url)
                    time.sleep(pause_time)
                    continue

            output['num']=li_number
            output['fn']=f_name
            output['ln']=l_name
            output['state']=state

            result.append((i,output))        
            file.w(output,method='add')

        driver.quit()
        return result

    except Exception as inst:
        print('Scraper Exception - Something bad happened in scraper_function not single_scrape')
        print(inst)
        if 'driver' in locals():
            driver.quit()
        return True,inst
###################################################################################################################
####################################################################################################################
#####################################################################################################################

# state of Ohio
# we can add names attribute directly from variable 'names'
def scrape_OH(sdata, driver_params, start=0, stop=True):
    try:
        state='OH'
        url = "https://elicense.ohio.gov/OH_HomePage"
        driver = get_driver(url,driver_params)
        driver = try_get_url(driver,driver_params,url)
        time.sleep(pause_time)

        def single_scrape(wbd, li_number, f_name, l_name):
            pause_time = 2 + random.random() * 2
            d = feature_dict(state, li_number, f_name, l_name)
            driver=wbd

            # Select first name box
            fnbox=driver.find_element_by_css_selector('#j_id0\:j_id61\:firstName')
            fnbox.clear()
            fnbox.send_keys(f_name)

            # Select first name box
            lnbox=driver.find_element_by_css_selector('#j_id0\:j_id61\:lastName')
            lnbox.clear()
            lnbox.send_keys(l_name)
            noresults=''

            driver.find_element_by_xpath('//*[@id="j_id0:j_id61"]/div[1]/div[4]/input').click()
            time.sleep(pause_time)

            try:
                noresults=driver.find_element_by_css_selector('#results > tbody > tr > td').text
            except NoSuchElementException:
                pass

            if '0 records' in noresults:
                d['license_type']='None Found'
                return d

            driver.execute_script("window.scrollTo(0, 1000)")
            time.sleep(1)
            content=driver.find_element_by_css_selector('#results > tbody').text.split('\n')
            linknum=''
            for c in content: 
                if (li_number in c) or (c in li_number):
                    print('Found')
                    linknum=str(int((content.index(c)+2)/3))

            if len(linknum)==0:
                d['lincense_type']='License Not Found'
                print('License Not Found')
                return d

            try:
                css_path="#results > tbody > tr:nth-child(" + str(linknum) + ") > td.LicenseEndorsementNumber > div"
                driver.find_element_by_css_selector(css_path).click()
            except NoSuchElementException:
                driver.find_element_by_css_selector('#results > tbody > tr > td.LicenseEndorsementNumber > div').click()
            time.sleep(.5)
            driver.find_element_by_css_selector('#results > tbody > tr.expanded-detail > td > table > tbody > tr > td:nth-child(4) > a').click()
            time.sleep(pause_time)
            table=driver.find_element_by_id('verify-license').text.split('\n')
            if 'Status' in table:
                d['license_status']=table[table.index('Status')+1]
            if 'License Effective Date' in table:
                d['effective_date']=table[table.index('License Effective Date')+1]
            if 'License Expiration Date' in table:
                d['expire_date']=table[table.index('License Expiration Date')+1]
            if 'License Type' in table:
                d['license_type']=table[table.index('License Type')+1]
            if 'Board Action' in table:
                d['board_sanction']=table[table.index('Board Action')+1]

            save_info(driver,state,li_number)
            return d

        sdata = sdata.astype(str)
        length = int(sdata.shape[0])
        if stop == True:
            stop = length

    # Write a file for the output
        # Write a file for the output

        file=Results(state)
        file.w(headers)
        result=[]  
        number_files=int(stop-start)
        for i in range(start, stop):
            print('processing '+state+ ': '+str(i+1)+' of '+str(number_files),'\n')
            
            if '--proxy-server' in driver_params:
                if i%loop_break==int(loop_break-1):
                    driver.quit()
                    driver = get_driver(url,driver_params)
                    driver = try_get_url(driver,driver_params,url)
            
            li_number = str(sdata.iloc[i].loc['num'])
            f_name = sdata.iloc[i].loc['fn']
            l_name = sdata.iloc[i].loc['ln']
            #extract the input parameters for the scraper function
            try:
                output=single_scrape(driver, li_number, f_name, l_name)
            except Exception as inst:

                result.append((i,inst))
                driver = try_get_url(driver,driver_params,url)
                time.sleep(pause_time)
                continue

            output['num']=li_number
            output['fn']=f_name
            output['ln']=l_name
            output['state']=state
            result.append((i,output))        
            file.w(output,method='add')
            driver = try_get_url(driver,driver_params,url)
            time.sleep(pause_time)
        driver.quit()
        return result

    except Exception as inst:
        print('Scraper Exception - Something bad happened in scraper_function not single_scrape')
        print(inst)
        if 'driver' in locals():
            try:
                driver.quit()
            except:
                pass
        return True,inst

# state of MONTANA
# we can add names attribute directly from variable 'names'
def scrape_MT(sdata, driver_params, start=0, stop=True):
    try:
        state='MT'
        url = "https://ebiz.mt.gov/POL/GeneralProperty/PropertyLookUp.aspx?isLicensee=Y&TabName=APO"
        driver = get_driver(url,driver_params)
        driver = try_get_url(driver,driver_params,url)
        time.sleep(pause_time)

        def single_scrape(wbd, li_number, f_name, l_name):
            pause_time = 2 + random.random() * 2
            d = feature_dict(state, li_number, f_name, l_name)
            driver = wbd

            # select license number search box
            li_number_box = driver.find_element_by_xpath('//*[@id="ctl00_PlaceHolderMain_refLicenseeSearchForm_txtLicenseNumber"]')

            # fill license number search box
            li_number_box.clear()
            if '-' in li_number:
                li_number_box.send_keys(li_number)  # "BBH-LCPC-LIC-28307"

            # select first name search box
            first_name_box = driver.find_element_by_xpath('//*[@id="ctl00_PlaceHolderMain_refLicenseeSearchForm_txtFirstName"]')

            # fill fisrt name search box with first name
            first_name_box.clear()
            first_name_box.send_keys(f_name)  # "ROBERT"

            # select last name search box
            last_name_box = driver.find_element_by_xpath('//*[@id="ctl00_PlaceHolderMain_refLicenseeSearchForm_txtLastName"]')

            # fill last name search box with last name
            last_name_box.clear()
            last_name_box.send_keys(l_name)  # "SCHWAB"

            # select the search button and click
            driver.find_element_by_xpath('//*[@id="ctl00_PlaceHolderMain_btnNewSearch"]').click()
            time.sleep(pause_time)

            # Switch the new window
            driver.switch_to.default_content()
            time.sleep(pause_time)

            try:
                
                license_code = driver.find_element_by_xpath('//*[@id="ctl00_PlaceHolderMain_licenseeGeneralInfo_lblLicenseeNumber_value"]').text
                license_number= license_code.split('-')[-1]
                
                # first name and last name match
                license_type = driver.find_element_by_xpath('//*[@id="ctl00_PlaceHolderMain_licenseeGeneralInfo_lblLicenseeType_parentGrid"]')
                d['license_type'] = license_type.text.split('\n')[1]

                names = driver.find_element_by_xpath('//*[@id="ctl00_PlaceHolderMain_upGeneralInfo"]/div[3]/div[2]')
                names = names.text.split('\n')[-1]

                license_status = driver.find_element_by_css_selector('#ctl00_PlaceHolderMain_licenseeGeneralInfo_lblInsuranceCompany_value').text
                d['license_status'] = license_status.split('\n')[-1]

                effective_date = driver.find_element_by_css_selector('#ctl00_PlaceHolderMain_licenseeGeneralInfo_lblLicenseIssueDate_value').text
                d['effective_date'] = effective_date.split('\n')[-1]
                expire_date = driver.find_element_by_css_selector('#ctl00_PlaceHolderMain_licenseeGeneralInfo_lblExpirationDate_value').text
                d['expire_date'] = expire_date.split('\n')[-1]

                # click the actions link and get open the information
                driver.find_element_by_xpath('//*[@id="ctl00_PlaceHolderMain_shAttachment_imgCollapseOrExpand"]').click()
                # Switch to the frame
                driver.switch_to.frame('iframeAttachmentList')

                actions = driver.find_element_by_xpath('//*[@id="attachmentList_gdvAttachmentList"]/tbody').text
                d['board_sanction'] = actions.split('\n')[-1]
                
                save_info(driver,state,li_number)
                return d
            except:
                return d

        sdata = sdata.astype(str)
        length = int(sdata.shape[0])
        if stop == True:
            stop = length

        # Write a file for the output

        file=Results(state)
        file.w(headers)
        result=[]  
        number_files=int(stop-start)
        for i in range(start, stop):
            print('processing '+state+ ': '+str(i+1)+' of '+str(number_files),'\n')
            
            if '--proxy-server' in driver_params:
                if i%loop_break==int(loop_break-1):
                    driver.quit()
                    driver = get_driver(url,driver_params)
                    driver = try_get_url(driver,driver_params,url)
            
            li_number = str(sdata.iloc[i].loc['num'])
            f_name = sdata.iloc[i].loc['fn']
            l_name = sdata.iloc[i].loc['ln']
            #extract the input parameters for the scraper function
            try:
                output=single_scrape(driver, li_number, f_name, l_name)
            except Exception as inst:

                result.append((i,inst))
                driver = try_get_url(driver,driver_params,url)
                time.sleep(pause_time)
                continue

            output['num']=li_number
            output['fn']=f_name
            output['ln']=l_name
            output['state']=state
            result.append((i,output))        
            file.w(output,method='add')
            driver = try_get_url(driver,driver_params,url)
            time.sleep(pause_time)
        driver.quit()
        return result

    except Exception as inst:
        print('Scraper Exception - Something bad happened in scraper_function not single_scrape')
        print(inst)
        if 'driver' in locals():
            try:
                driver.quit()
            except:
                pass
        return True,inst

# state of IDAHO
def scrape_ID(sdata, driver_params, start=0, stop=True):
    try:
        state='ID'
        url = "https://isecure.bom.idaho.gov/BOMPublic/LPRBrowser.aspx"
        url_prof = "https://secure.ibol.idaho.gov/eIBOLPublic/LPRBrowser.aspx"
        driver = get_driver(url,driver_params)
        driver = try_get_url(driver,driver_params,url)
        time.sleep(pause_time)

        def single_scrape(wbd, li_number, f_name, l_name):
            d = feature_dict(state, li_number, f_name, l_name)
            driver=wbd
            # # select license number search box
            license_num_box = driver.find_element_by_xpath('//*[@id="ctl00_CPH1_txtsrcLicenseNo"]')
            # # fill search box with license number
            license_num_box.clear()
            license_num_box.send_keys(li_number)  # "PA-915"

            # select the "Start Search" button and click
            driver.find_element_by_id('ctl00_CPH1_btnGoFind').click()
            time.sleep(pause_time)
            # try to click through the search result
            try:
                namelink=driver.find_element_by_xpath('//*[@id="ctl00_CPH1_myDataGrid"]/tbody/tr[2]/td[3]/a')
                newlink=namelink.get_attribute('href')
                driver.get(newlink)
                time.sleep(pause_time)
            except NoSuchElementException:
                try:
                    link=driver.find_element_by_partial_link_text(li_number)
                    newlink=link.get_attribute('href')
                    driver.get(newlink)
                    time.sleep(pause_time)
                except NoSuchElementException:
                    d['license_type']='None Found'
                    return d
                

            # Extract the whole page content using BeautifulSoup
            content = driver.find_element_by_id('content')
            html = content.get_attribute("outerHTML")
            soup = BeautifulSoup(html, "html.parser")

            # find the license status
            status = soup.find(id='ctl00_CPH1_txtLicenseStatus')
            status = str(status).split('value=')[1]
            d['license_status'] = status.strip(string.punctuation)

            # find the license type
            license_type = soup.find(id='ctl00_CPH1_txtLicenseTypeDescription')
            license_type = str(license_type).split('value=')[1]
            d['license_type'] = license_type.strip(string.punctuation)

            # find effective date
            effective_date = soup.find(id='ctl00_CPH1_txtLicenseIssueDate')
            effective_date = str(effective_date).split('value=')[1]
            d['effective_date'] = effective_date.strip(string.punctuation)

            # Find expire date
            expire_date = soup.find(id='ctl00_CPH1_txtLicenseExpireDate')
            try:
                expire_date = str(expire_date).split('value=')[1]
                expire_date = expire_date.strip(string.punctuation)
            except IndexError:
                expire_date = ''
            d['expire_date'] = expire_date

            # Board sanction
            try:
                board_sanction = driver.find_element_by_css_selector(".lblLPRTitle").text
                d['board_sanction'] = board_sanction.strip().split('-')[1]
            except IndexError:
                pass
            
            save_info(driver,state,li_number)
            
            return d
        
        def single_scrape_prof(wbd, li_number, f_name, l_name):
            pause_time = 2 + random.random() * 2
            short_pause=random.randint(1,5) / 10 + random.random()
            d = feature_dict(state, li_number, f_name, l_name)
            driver=wbd

            # # select license number search box
            license_num_box = driver.find_element_by_xpath('//*[@id="ctl00_CPH1_txtsrcLicenseNo"]')
            # # fill search box with license number
            license_num_box.clear()
            license_num_box.send_keys(li_number)  # "PA-915"

            # select the "Start Search" button and click
            driver.find_element_by_id('ctl00_CPH1_btnGoFind').click()
            time.sleep(pause_time)
            # try to click through the search result
            try:
                namelink=driver.find_element_by_xpath('//*[@id="ctl00_CPH1_myDataGrid"]/tbody/tr[2]/td[3]/a')
                newlink=namelink.get_attribute('href')
                driver.get(newlink)
                time.sleep(pause_time)
            except NoSuchElementException:
                try:
                    link=driver.find_element_by_partial_link_text(li_number)
                    newlink=link.get_attribute('href')
                    driver.get(newlink)
                    time.sleep(pause_time)
                except NoSuchElementException:
                    d['license_type']='None Found'
                    return d


            # Extract the whole page content using BeautifulSoup
            content = driver.find_element_by_id('content')
            html = content.get_attribute("outerHTML")
            soup = BeautifulSoup(html, "html.parser")

            # find the name
            fullname = soup.find(id='ctl00_CPH1_txtLicenseeName')
            fullname = str(fullname).split('value=')[1].strip(string.punctuation).strip()

            if (l_name.upper() not in fullname.upper()) & (f_name.upper() not in fullname.upper()):
                d['license_type']='None Found'
                return d

            d['first_name']=fullname.split()[0].strip().upper()
            d['last_name']=fullname.split()[-1].strip().upper()


            # find the license status
            status = soup.find(id='ctl00_CPH1_txtLicenseStatus')
            status = str(status).split('value=')[1]
            d['license_status'] = status.strip(string.punctuation)

            # find the license type
            license_type = soup.find(id='ctl00_CPH1_txtLicenseTypeDescription')
            license_type = str(license_type).split('value=')[1]
            d['license_type'] = license_type.strip(string.punctuation)

            # find effective date
            effective_date = soup.find(id='ctl00_CPH1_txtOriginalLicenseDate')
            effective_date = str(effective_date).split('value=')[1]
            d['effective_date'] = effective_date.strip(string.punctuation)

            # Find expire date
            expire_date = soup.find(id='ctl00_CPH1_txtLicenseExpDate')
            try:
                expire_date = str(expire_date).split('value=')[1]
                expire_date = expire_date.strip(string.punctuation).strip()
            except IndexError:
                expire_date = ''
            d['expire_date'] = expire_date

            # Board sanction
            try:
                board_sanction = driver.find_element_by_id("ctl00_CPH1_pnlNoDisciplinary").text
                d['board_sanction'] = board_sanction.split('\n')[1].strip()
            except IndexError:
                pass
            
            save_info(driver,state,li_number)
            
            return d


        sdata = sdata.astype(str)
        length = int(sdata.shape[0])
        if stop == True:
            stop = length

        # Write a file for the output

        file=Results(state)
        file.w(headers)
        result=[]  
        number_files=int(stop-start)
        for i in range(start, stop):
            print('processing '+state+ ': '+str(i+1)+' of '+str(number_files),'\n')
            
            if '--proxy-server' in driver_params:
                if i%loop_break==int(loop_break-1):
                    driver.quit()
                    driver = get_driver(url,driver_params)
                    driver = try_get_url(driver,driver_params,url)
            
            li_number = str(sdata.iloc[i].loc['num'])
            f_name = sdata.iloc[i].loc['fn']
            l_name = sdata.iloc[i].loc['ln']
            #extract the input parameters for the scraper function
            try:
                output=single_scrape(driver, li_number, f_name, l_name)
            except Exception as inst:
                print(inst)
                result.append((i,inst))
                driver = try_get_url(driver,driver_params,url)
                time.sleep(pause_time)
                continue

                
            if output['license_status']=='':
                driver = try_get_url(driver,driver_params,url_prof)
                time.sleep(pause_time)
                try:
                    output=single_scrape_prof(driver, li_number, f_name, l_name)
                except Exception as inst:
                    print(inst)
                    result.append((i,inst))
                    driver = try_get_url(driver,driver_params,url)
                    time.sleep(pause_time)
                    continue
                    
                    
            output['num']=li_number
            output['fn']=f_name
            output['ln']=l_name
            output['state']=state
            result.append((i,output))        
            file.w(output,method='add')
            driver = try_get_url(driver,driver_params,url)
            time.sleep(pause_time)
        driver.quit()
        return result

    except Exception as inst:
        print('Scraper Exception - Something bad happened in scraper_function not single_scrape')
        print(inst)
        if 'driver' in locals():
            driver.quit()
        return True,inst

###############################################################################
###############################################################################
###############################################################################

# with "Additional Public Information" did not
def scrape_VA(sdata, driver_params, start=0, stop=True):
    try:
        state='VA'
        driver_params.append('--webproxy')
        url = "https://dhp.virginiainteractive.org/Lookup/Index"
        driver = get_driver(url,driver_params)
        driver = try_get_url(driver,driver_params,url)
        time.sleep(pause_time)

        def single_scrape(wbd, li_number, f_name, l_name):
            pause_time = 2 + random.random() * 2
            d = feature_dict(state, li_number, f_name, l_name)
            
            try:
                driver.find_element_by_partial_link_text('Back to License').click()
            except NoSuchElementException:
                pass

            # processing li_number
            li_number = ''.join(c for c in li_number if c.isdigit())
            if len(li_number) > 10:
                return d
            # padding leading zeros to make up 10 total digits printf-style formatting

            li_number = "%010d" % (int(li_number),)

            # select the license number search box
            license_num_search_box = driver.find_element_by_xpath('//*[@id="LicenseNo"]')

            # fill search box with license number
            license_num_search_box.clear()
            license_num_search_box.send_keys(li_number)

            driver.find_element_by_xpath('/html/body/div[3]/div[2]/div[6]/div[2]/form[1]/div/div[2]/input[1]').click()
            time.sleep(pause_time)
            stop=''
            try:
                stop=driver.find_elements_by_xpath('/html/body/div[2]/div[2]/div[4]')[0].text   
            except (IndexError, NoSuchElementException):
                pass
            if 'sorry' in stop:
                print('Website Blocked')
                d['license_number']='retry'
                return d

            try:
                searchname=l_name[0]+l_name[1:].lower()
                result = driver.find_element_by_partial_link_text(searchname).click()
            except NoSuchElementException:
                try:
                    searchname=l_name
                    result = driver.find_element_by_partial_link_text(searchname).click()
                    time.sleep(pause_time)
                except NoSuchElementException:
                    return d

            content = driver.find_element_by_css_selector(".panel-body").text
            content = content.split("\n")
            # Extract effective date
            for c in content:
                if "Initial License Date" in c:
                    d['effective_date'] = c.split()[-1]

                # Extract expire date
                if "Expire Date" in c:
                    d['expire_date'] = c.split()[-1]

                # Extract license status
                if "License Status" in c:
                    d['license_status'] = c.split('License Status')[1].strip()

                # Extract License type
                if "Occupation" in c:
                    d['license_type'] = c.split("Occupation")[1].strip()

                # extract the "Additional Public Information" for sanction
                if 'Additional Public Information*' in c:
                    d['board_sanction'] = c.split('Additional Public Information*')[1].strip()
            
            save_info(driver,state,li_number)
            return d

        sdata = sdata.astype(str)
        length = int(sdata.shape[0])
        if stop == True:
            stop = length

        # Write a file for the output

        file=Results(state)
        file.w(headers)
        result=[]  
        number_files=int(stop-start)
        n=1
        for i in range(start, stop):
            print('processing '+state+ ': '+str(i+1)+' of '+str(number_files),'\n')
            
            if '--proxy-server' in driver_params:
                if i%loop_break==int(loop_break-1):
                    driver.quit()
                    driver = get_driver(url,driver_params)
                    driver = try_get_url(driver,driver_params,url)
                    
            li_number = str(sdata.iloc[i].loc['num'])
            f_name = sdata.iloc[i].loc['fn']
            l_name = sdata.iloc[i].loc['ln']
            #extract the input parameters for the scraper function
            try:
                output=single_scrape(driver, li_number, f_name, l_name)
            except Exception as inst:
                print('in running exception')
                result.append((i,inst))
                driver = try_get_url(driver,driver_params,url)
                time.sleep(pause_time)
                continue
            if output['license_number']=='retry':
                print('output retry')
                i-=1
                if n>4:
                    i+=1
                    continue
                
                n+=1
                driver.quit()
                print('after driverclose')
                driver = get_driver(url,driver_params,n)
                driver = try_get_url(driver,driver_params,url)
                
                
            output['num']=li_number
            output['fn']=f_name
            output['ln']=l_name
            output['state']=state
            result.append((i,output))        
            file.w(output,method='add')

        driver.quit()
        return result

    except Exception as inst:
        print('Scraper Exception - Something bad happened in scraper_function not single_scrape')
        print(inst)
        if 'driver' in locals():
            try:
                driver.quit()
            except:
                pass
        return True,inst

###############################################################################
###############################################################################
###############################################################################
# State of West Virginia

# with "Additional Public Information" did not
# li_number is 5 digit number string.
def scrape_WV(sdata, driver_params, start=0, stop=True):
    try:
        state='WV'
        url = "https://wvbom.wv.gov/public/search/details.asp"
        driver = get_driver(url,driver_params)
        driver = try_get_url(driver,driver_params,url)
        time.sleep(pause_time)

        def single_scrape(wbd, li_number, f_name, l_name):
            pause_time = 2 + random.random() * 2
            d = feature_dict(state, li_number, f_name, l_name)
            driver=wbd
            # select the license type for searching
            driver.find_element_by_xpath('//*[@id="selectType"]/option[4]').click()

            # select the license number search box
            license_num_search_box = driver.find_element_by_xpath('//*[@id="inputLicenseNumber"]')

            # fill search box with license number
            license_num_search_box.clear()
            license_num_search_box.send_keys(li_number)

            # click the search button
            driver.find_element_by_xpath('//*[@id="licNo"]/div/div[2]/button').click()
            time.sleep(pause_time)
            # this link is for no result
            if driver.find_element_by_xpath('//*[@id="form"]/div[1]').text == 'No results found':
                return d
            
            try:
                table=driver.find_element_by_xpath('//*[@id="form"]/table/tbody').text.split('\n')
                found=False
                for t in range(len(table)):
                    elems=[x.upper() for x in table[t].replace(',','').split(' ')]
                    if (f_name.upper() in elems) and (l_name.upper() in elems):
                        found=True
                        row_num=t
                if found==True:
                    xpath='//*[@id="form"]/table/tbody/tr['+str(row_num+1)+']/td[1]'
                    driver.find_element_by_xpath(xpath).click()
                else:
                    return d
                
            except NoSuchElementException as inst:
                pass


            time.sleep(pause_time)
            result = driver.find_element_by_xpath('//*[@id="body"]/div[1]/div[2]/div')

            result = result.text.split('\n')

            # extract name
            first_name = result[3].split()[0]
            last_name = result[3].split()[1] or result[3].split()[2]
            if first_name.lower() != f_name.lower() and (result[3].split()[1].lower() != l_name.lower() or \
                                                         result[3].split()[2].lower() != l_name.lower()):
                return d

            content_idx = result.index('License History') + 2
            d['license_type'] = result[content_idx].split(li_number)[0].strip()
            d['license_status'] = result[content_idx].split()[-3]
            d['effective_date'] = result[content_idx].split()[-2]
            d['expire_date'] = result[content_idx].split()[-1]
            board_action_idx = result.index("Discipline/Board Action History") + 1
            d['board_sanction'] = result[board_action_idx]

            save_info(driver,state,li_number)
            return d
        
        sdata = sdata.astype(str)
        length = int(sdata.shape[0])
        if stop == True:
            stop = length

        # Write a file for the output

        file=Results(state)
        file.w(headers)
        result=[]  
        number_files=int(stop-start)
        for i in range(start, stop):
            print('processing '+state+ ': '+str(i+1)+' of '+str(number_files),'\n')
            
            if '--proxy-server' in driver_params:
                if i%loop_break==int(loop_break-1):
                    driver.quit()
                    driver = get_driver(url,driver_params)
                    driver = try_get_url(driver,driver_params,url)
            
            li_number = str(sdata.iloc[i].loc['num'])
            f_name = sdata.iloc[i].loc['fn']
            l_name = sdata.iloc[i].loc['ln']
            #extract the input parameters for the scraper function
            try:
                output=single_scrape(driver, li_number, f_name, l_name)
            except Exception as inst:

                result.append((i,inst))
                driver = try_get_url(driver,driver_params,url)
                time.sleep(pause_time)
                continue

            output['num']=li_number
            output['fn']=f_name
            output['ln']=l_name
            output['state']=state
            result.append((i,output))        
            file.w(output,method='add')
            driver = try_get_url(driver,driver_params,url)
            time.sleep(pause_time)
        driver.quit()
        return result

    except Exception as inst:
        print('Scraper Exception - Something bad happened in scraper_function not single_scrape')
        print(inst)
        if 'driver' in locals():
            try:
                driver.quit()
            except:
                pass
        return True,inst

#################################################################################
#################################################################################
#################################################################################
### State of Texas

def scrape_TX(sdata, driver_params, start=0, stop=True):
    try:
        state='TX'
        url="https://public.tmb.state.tx.us/HCP_Search/SearchInput.aspx"
        url_ap="https://app.bon.texas.gov/verify/?type=ap"
        url_rn='https://app.bon.texas.gov/verify/?type=rn'
        url_prof='https://vo.licensing.hpc.texas.gov/datamart/selSearchType.do'
        url_pt='https://www.ptot.texas.gov/page/pt-license-search'
        url_opt='http://www.tob.texas.gov/licensesearch/' # Not made yet
        driver = get_driver(url,driver_params)
        def single_scrape(wbd, li_number, f_name, l_name):
            pause_time = 2 + random.random() * 2
            d = feature_dict(state, li_number, f_name, l_name)
            driver=wbd

            wait = WebDriverWait(driver, 10)

            # click the button
            try:
                driver.find_element_by_css_selector('#BodyContent_btnAccept').click()
            except:
                pass

            license_num_box_link = 'BodyContent_tbLicense'
            first_name_box_link = 'BodyContent_tbFirstName'
            last_name_box_link = 'BodyContent_tbLastName'
            search_button_link = 'BodyContent_btnSearch'

            # select the license number search box
            license_num_search_box = driver.find_element_by_id(license_num_box_link)

            # fill search box with license number
            license_num_search_box.clear()
            license_num_search_box.send_keys(li_number)

            # select the last name search box
            last_name_search_box = driver.find_element_by_id(last_name_box_link)

            # fill search box with last name
            last_name_search_box.clear()
            last_name_search_box.send_keys(l_name)

            # select the first name search box
            first_name_search_box = driver.find_element_by_id(first_name_box_link)

            # fill search box with first name
            first_name_search_box.clear()
            first_name_search_box.send_keys(f_name)

            # click the search button
            driver.find_element_by_id(search_button_link).click()
            wait.until(EC.element_to_be_clickable((By.CSS_SELECTOR,'#Form1 > div.wrapper1 > div.navbar > div > ul > li:nth-child(2) > a')))

            result = driver.find_element_by_id('BodyContent_pnlResults')
            
            # if search result is empty, just return
            if result.text == '':
                d['license_type']='None Found'
                return d

            # click the search result link
            searchnames=[f_name,l_name,l_name+', '+f_name]
            found=False
            for name in searchnames[::-1]:
                try:
                    driver.find_element_by_partial_link_text(name).click()
                    time.sleep(pause_time)
                    found=True
                    break
                except NoSuchElementException:
                    continue    

            if found ==False:
                d['license_type']='None Found'
                return d


            # extract the license type field

            d["license_type"] = driver.find_element_by_xpath('/html/body/font/table/tbody/tr['
                                                                '2]/td/table/tbody/tr/td/h2').text.strip()
            content = driver.find_element_by_xpath('/html/body/font/table/tbody/tr[2]/td/table/tbody/tr/td/table[2]')
            content = content.text.split('\n')

            for c in content:
                if "Registration Date: " in c:
                    d['effective_date'] = c.split()[-1]

                # If Registration date not there it looks for Permit status
                if d['effective_date'] == '':
                    if "Permit Status Date:" in c:
                        d['effective_date'] = c.split('Date:')[1].strip()

                # Extract expire date
                if "Expiration Date" in c:
                    d['expire_date'] = c.split()[-1]                    

                # Extract license status
                if "Registration Status" in c:
                    d['license_status'] = c.split('Registration Status: ')[1].split('Registration')[0].strip()

                # If Registration status not there it looks for Permit status
                if d['license_status'] == '':
                    if "Permit Status" in c:
                        d['license_status'] = c.split('Permit Status: ')[1].strip()

                # extract the "Additional Public Information" for sanction
                if 'Board Action' in c:
                    idx = content.index(c)
                    d['board_sanction'] = content[idx + 1].strip()

            save_info(driver,state,li_number)
            return d

        def single_scrape_nurses(wbd, li_number, f_name, l_name):
            pause_time = 2 + random.random() * 2
            d = feature_dict(state, li_number, f_name, l_name)
            driver=wbd
            # click searchbox
            try:
                driver.find_element_by_xpath('//*[@id="LicNumber"]').click() # input selector
                time.sleep(1)
            except:
                pass

            # select the license number search box
            l_num_field = driver.find_element_by_xpath('//*[@id="LicNumber"]')
            l_num_field.send_keys(li_number)
            time.sleep(1)

            # scroll to bring it into view then click search
            driver.execute_script("window.scrollTo(0, 200)")
            driver.find_element_by_xpath('//*[(@id = "searchLICNUM")]').click() # click the submit button
            time.sleep(1)

            if 'Total Finds: 0' in driver.find_element_by_xpath('//*[@id="totalcount"]').text:
                return d
            else:
                name=driver.find_element_by_class_name('nurseNames').text
                if (f_name not in name and l_name not in name):
                    return d
                d['first_name']=name.split()[0]
                d['last_name']=name.split()[-1]
                d['license_number']= driver.find_element_by_xpath('//*[@id="printContainer"]/div[2]/table[1]/tbody/tr[2]/td[2]').text
                d['first_name']= driver.find_element_by_xpath('//*[@id="printContainer"]/div[2]/div[1]/h4').text
                d['license_type']= driver.find_element_by_xpath('//*[@id="printContainer"]/div[2]/table[1]/tbody/tr[1]/td[2]').text
                d['expire_date']= driver.find_element_by_xpath('//*[@id="printContainer"]/div[2]/table[1]/tbody/tr[6]/td[2]').text
                d['license_status']= driver.find_element_by_xpath('//*[@id="printContainer"]/div[2]/table[1]/tbody/tr[5]/td[2]').text

            save_info(driver,state,li_number)
            return d
        
        def single_scrape_prof(wbd, li_number, f_name, l_name):
            pause_time = 2 + random.random() * 2
            short_pause=random.randint(1,5) / 10 + random.random()
            d = feature_dict(state, li_number, f_name, l_name)
            driver=wbd

            wait = WebDriverWait(driver, 3)
            # link=driver.find_element_by_partial_link_text('Public Search').get_attribute('href')

            wait.until(EC.element_to_be_clickable((By.PARTIAL_LINK_TEXT,'License Number'))).click()

            license_num_box_link = 'licNumber'
            # first_name_box_link = 'firstName'
            # last_name_box_link = 'lastName'
            search_button_link = 'search'

            # select the license number search box

            license_num_search_box = driver.find_element_by_id(license_num_box_link)

            # fill search box with license number
            license_num_search_box.clear()
            license_num_search_box.send_keys(li_number)

            # Change results per page
            res_pp=driver.find_element_by_id('rowsPerPage')
            res_pp.send_keys('20')

            # click the search button
            driver.find_element_by_name(search_button_link).click()
            
            wait.until(EC.element_to_be_clickable((By.NAME,'back')))
            try:
                if 'NO RESULTS' in driver.find_element_by_css_selector('#frm1 > table:nth-child(2) > tbody').text:
                    d['license_type']='None Found'
                    return d
            except NoSuchElementException:
                pass

            searchnames=[f_name,l_name,l_name+', '+f_name]
            found=False
            for name in searchnames[::-1]:
                try:
                    namelink=driver.find_element_by_partial_link_text(name)
                    newlink=namelink.get_attribute('href')
                    driver.get(newlink)
                    time.sleep(pause_time)
                    found=True
                    break
                except NoSuchElementException:
                    continue    

            if found ==False:
                d['license_type']='None Found'
                return d

            content=wait.until(EC.presence_of_element_located((By.ID,'contentBox')),message='noelm').text.split('\n')
            start_num=0

            for c in content:
                if "NAME:" in c.upper():
                    fullname=content[content.index(c)+1].upper()
                    d['last_name'] = fullname.split(',')[0].strip().upper()
                    d['first_name'] = fullname.split(',')[-1].strip().upper()      

                # Extract License type
                if "LICENSE NUMBER:" in c.upper():
                    d['license_number'] = c.split(':')[1].split()[0].strip().upper()

                if "LICENSE STATUS:" in c.upper():
                    d['license_status'] = content[content.index(c)+1].strip().upper()

                if "LICENSE TYPE:" in c.upper():
                    d['license_type'] = content[content.index(c)+1].strip().upper()

                if "EFFECTIVE RANK DATE:" in c.upper():
                    d['effective_date'] = content[content.index(c)+1].strip().upper()

                if "EXPIRY DATE:" in c.upper():
                    d['expire_date'] = content[content.index(c)+1].strip().upper()

                if "DISCIPLINARY ACTIONS" in c.upper():
                    if len(c.strip())<21:
                        d['board_sanction']=content[content.index(c)+1].strip().upper()

            save_info(driver,state,li_number)
            return d
        
        def single_scrape_pt(wbd, li_number, f_name, l_name):
            pause_time = 2 + random.random() * 2
            d = feature_dict(state, li_number, f_name, l_name)
            driver=wbd

            wait = WebDriverWait(driver, 10)

            driver.execute_script("window.scrollTo(0, 1000)")

            # select the license number search box
            license_num_search_box = driver.find_element_by_id('searchForm_license')

            # fill search box with license number
            license_num_search_box.clear()
            license_num_search_box.send_keys(li_number)

            # click the search button
            driver.find_element_by_css_selector('#search-by-license-form > dl > dd:nth-child(4) > button').click()
            results=''
            try:
                results=driver.find_element_by_id('ptot-search-form-errors').text.upper()
            except NoSuchElementException:
                pass
            
            if 'NO RESULT' in results:
                d['license_type']='None Found'
                return d
            

            wait.until(EC.element_to_be_clickable((By.ID,'new-search')))


            content = driver.find_element_by_id('results-block').text.split('\n')

            fullname=content[0].split('-')[0].strip().upper()
            if (l_name.upper() not in fullname.upper()) & (f_name.upper() not in fullname.upper()):
                d['license_type']='None Found name'
                return d

            for c in content:

                # Extract License Number
                if "LICENSE NUMBER:" in c.upper():
                    d['license_type'] = c.split(':')[-1].strip().upper()

                # Extract License Type
                if "LICENSE TYPE:" in c.upper():
                    d['license_type'] = c.split(':')[-1].strip().upper()

                # Extract effective date
                if "ISSUED" in c.upper():
                    d['effective_date'] = c.split(':')[-1].strip().upper()         

                # Extract license status
                if "STATUS:" in c.upper():
                    info=c.split(':')[-1].strip().upper()
                    d['license_status'] = info.split('-')[0]
                    d['expire_date'] = info.split('-')[-1]

                # extract the "Additional Public Information" for sanction
                if 'DISCIPLINARY ACTION' in c.upper():
                    d['board_sanction'] = c.split(':')[-1].strip().upper()  

            save_info(driver,state,li_number)
            return d

        sdata = sdata.astype(str)
        length = int(sdata.shape[0])
        if stop == True:
            stop = length
                
        # Write a file for the output

        file=Results(state)
        file.w(headers)
        result=[]  
        number_files=int(stop-start)
        for i in range(start, stop):
            print('processing '+state+ ': '+str(i+1)+' of '+str(number_files),'\n')
            
            if '--proxy-server' in driver_params:
                if i%loop_break==int(loop_break-1):
                    driver.quit()
                    driver = get_driver(url,driver_params)
                    driver = try_get_url(driver,driver_params,url)
            
            li_number = str(sdata.iloc[i].loc['num'])
            f_name = sdata.iloc[i].loc['fn']
            l_name = sdata.iloc[i].loc['ln']
            if 'AP' in li_number:
                driver= try_get_url(driver,driver_params,url_ap)
                time.sleep(pause_time)
                try:
                    output=single_scrape_nurses(driver, li_number, f_name, l_name)
                except Exception as inst:
                    print(i,'ap',inst)
                    result.append((i,inst))
                    driver = try_get_url(driver,driver_params,url)
                    time.sleep(pause_time)
                    continue
            else:
                driver = try_get_url(driver,driver_params,url)
                time.sleep(pause_time)
                try:
                    output=single_scrape(driver, li_number, f_name, l_name)
                except Exception as inst:
                    print(i,'physician',inst)
                    result.append((i,inst))
                    driver = try_get_url(driver,driver_params,url)
                    time.sleep(pause_time)
                    continue

                if output['license_status']=="":
                    driver = try_get_url(driver,driver_params,url_prof)
                    time.sleep(pause_time)
                    try:
                        output=single_scrape_prof(driver, li_number, f_name, l_name)
                    except Exception as inst:
                        print(i,'prof',inst)
                        result.append((i,inst))
                        driver = try_get_url(driver,driver_params,url)
                        time.sleep(pause_time)
                        continue
                        
                if output['license_status']=="":
                    driver = try_get_url(driver,driver_params,url_rn)
                    time.sleep(pause_time)
                    try:
                        output=single_scrape_nurses(driver, li_number, f_name, l_name)
                    except Exception as inst:
                        print(i,'rn',inst)
                        result.append((i,inst))
                        driver = try_get_url(driver,driver_params,url)
                        time.sleep(pause_time)
                        continue
                        
                if output['license_status']=="":
                    driver = try_get_url(driver,driver_params,url_pt)
                    time.sleep(pause_time)
                    try:
                        output=single_scrape_pt(driver, li_number, f_name, l_name)
                    except Exception as inst:
                        print(i,li_number,'pt',inst)
                        result.append((i,inst))
                        driver = try_get_url(driver,driver_params,url)
                        time.sleep(pause_time)
                        continue
                        
            output['num']=li_number
            output['fn']=f_name
            output['ln']=l_name
            output['state']=state
            result.append((i,output))        
            file.w(output,method='add')
        driver.quit()
        return result

    except Exception as inst:
        print('Scraper Exception - Something bad happened in scraper_function not single_scrape')
        print(inst)
        if 'driver' in locals():
            try:
                driver.quit()
            except:
                pass
        return True,inst
    
    

# # # # with "Additional Public Information" did not
def scrape_MI(sdata, driver_params, start=0, stop=True):
    try:
        state='MI'
        url = "https://aca3.accela.com/MILARA/GeneralProperty/PropertyLookUp.aspx?isLicensee=Y&TabName=APO"
        url_prof="https://val.apps.lara.state.mi.us/License/Search"
        driver = get_driver(url,driver_params)
        driver = try_get_url(driver,driver_params,url)
        time.sleep(pause_time)

        def single_scrape(wbd, li_number, f_name, l_name):
            pause_time = 2 + random.random() * 2
            d = feature_dict(state, li_number, f_name, l_name)
            driver=wbd

            former_num_search_box=driver.find_element_by_css_selector('#ctl00_PlaceHolderMain_refLicenseeSearchForm_txtBusiLicense')
            license_num_search_box = driver.find_element_by_xpath('//*[@id="ctl00_PlaceHolderMain_refLicenseeSearchForm_txtLicenseNumber"]')
            #             ln_search_box= driver.find_element_by_name('ctl00$PlaceHolderMain$refLicenseeSearchForm$txtLastName')
            former_num_search_box.clear()
            license_num_search_box.clear()
            #             ln_search_box.clear()
            license_num_search_box.send_keys(li_number)
            #             ln_search_box.send_keys(l_name)
            time.sleep(pause_time)
            driver.find_element_by_css_selector('#ctl00_PlaceHolderMain_btnNewSearch').click() # click the submit button
            time.sleep(pause_time)
            try:
                ltype = driver.find_element_by_xpath('//*[@id="ctl00_PlaceHolderMain_licenseeGeneralInfo_lblLicenseeType_value"]').text
                lnum = driver.find_element_by_xpath('//*[@id="ctl00_PlaceHolderMain_licenseeGeneralInfo_lblLicenseeNumber_value"]').text
                exp_date = driver.find_element_by_xpath('//*[@id="ctl00_PlaceHolderMain_licenseeGeneralInfo_lblExpirationDate_value"]').text
                lstatus = driver.find_element_by_xpath('//*[@id="ctl00_PlaceHolderMain_licenseeGeneralInfo_lblBusinessName2_value"]').text
                full_name= driver.find_element_by_id('ctl00_PlaceHolderMain_licenseeGeneralInfo_lblContactName_value').text.upper()

                if (f_name.upper() not in full_name) and (l_name.upper() not in full_name):
                    d['license_type']='None Found'
                    return d   

                d['first_name']=full_name.split()[0]
                d['last_name']=full_name.split()[-1]
                d['license_type'] = ltype
                d['license_number'] = lnum
                d['expire_date'] = exp_date
                d['license_status'] = lstatus

            except NoSuchElementException:
                print('License not current')
                former_num_search_box=driver.find_element_by_css_selector('#ctl00_PlaceHolderMain_refLicenseeSearchForm_txtBusiLicense')
                license_num_search_box = driver.find_element_by_xpath('//*[@id="ctl00_PlaceHolderMain_refLicenseeSearchForm_txtLicenseNumber"]')
            #                 ln_search_box= driver.find_element_by_name('ctl00$PlaceHolderMain$refLicenseeSearchForm$txtLastName')
                former_num_search_box.clear()
                license_num_search_box.clear()
                former_num_search_box.send_keys(li_number)
            #                 ln_search_box.send_keys(l_name)
                driver.find_element_by_css_selector('#ctl00_PlaceHolderMain_btnNewSearch').click() # click the submit button
                time.sleep(pause_time)
                try:
                    ltype = driver.find_element_by_xpath('//*[@id="ctl00_PlaceHolderMain_licenseeGeneralInfo_lblLicenseeType_value"]').text
                    lnum = driver.find_element_by_xpath('//*[@id="ctl00_PlaceHolderMain_licenseeGeneralInfo_lblLicenseeNumber_value"]').text
                    exp_date = driver.find_element_by_xpath('//*[@id="ctl00_PlaceHolderMain_licenseeGeneralInfo_lblExpirationDate_value"]').text
                    lstatus = driver.find_element_by_xpath('//*[@id="ctl00_PlaceHolderMain_licenseeGeneralInfo_lblBusinessName2_value"]').text
                    full_name= driver.find_element_by_id('ctl00_PlaceHolderMain_licenseeGeneralInfo_lblContactName_value').text.upper()

                    if (f_name.upper() not in full_name) and (l_name.upper() not in full_name):
                        d['license_type']='None Found'
                        return d   

                    d['first_name']=full_name.split()[0]
                    d['last_name']=full_name.split()[-1]
                    d['effective_date']='OLD LICENSE'
                    d['license_type'] = ltype
                    d['license_number'] = lnum
                    d['expire_date'] = exp_date
                    d['license_status'] = 'MI OLD LICENSE'
                    d['board_sanction'] = 'OLD LICENSE'

                except NoSuchElementException:
                    print('No License Found','\n')

                    d = feature_dict(state, li_number, f_name, l_name)
                    return d
            
            save_info(driver,state,li_number)
            return d
        
        def single_scrape_prof(wbd, li_number, f_name, l_name):
            pause_time = 2 + random.random() * 2
            d = feature_dict(state, li_number, f_name, l_name)
            driver=wbd
            driver.execute_script("window.scrollTo(0, 200)")
            time.sleep(pause_time)
            license_button=driver.find_element_by_xpath('//*[@id="LicenseNumber"]')
#             ln_button=driver.find_element_by_name('LastName')
            license_button.clear()
#             ln_button.clear()
            license_button.send_keys(li_number)
#             ln_button.send_keys(l_name)
            
            driver.find_element_by_xpath('//*[(@id = "searchButton")]').click() # click the submit button
            time.sleep(pause_time)
            try:
                if 'No' in driver.find_element_by_xpath('//*[(@id = "searchResults")]').text:
                    return d
            except NoSuchElementException:
                pass
            
            try:
                driver.find_element_by_xpath('//*[@id="searchResults"]/div[1]/table/tbody/tr[1]/td[4]/a').click()
            except NoSuchElementException:
                return d

            try:

                ltype = driver.find_element_by_xpath('//*[@id="licenseType"]/span[2]').text
                lnum = driver.find_element_by_xpath('//*[@id="permanentId"]/span[2]').text
                exp_date = driver.find_element_by_xpath('//*[@id="licenseExpirationDate"]/span[2]').text
                lstatus = driver.find_element_by_xpath('//*[@id="licenseStatus"]/span[2]').text
                full_name=driver.find_element_by_css_selector('#personName > span.detailItem').text
                
                if (f_name.upper() not in full_name) and (l_name.upper() not in full_name):
                    return d  
                d['first_name']=full_name.split()[0]
                d['last_name']=full_name.split()[-1]
                d['license_type'] = ltype
                d['license_number'] = lnum
                d['expire_date'] = exp_date
                d['license_status'] = lstatus

            except NoSuchElementException:
                return d

            try:

                issue_date= driver.find_element_by_xpath('//*[@id="licenseIssueDate"]/span[2]').text
                complaint=driver.find_element_by_xpath('//*[@id="complaintsAndDisciplineContainer"]/div/div/span[2]').text
                board_actions=driver.find_element_by_xpath('//*[@id="complaintsAndDisciplineContainer"]/span[2]').text

                d['effective_date'] = issue_date
                d['board_sanction'] = complaint +' '+board_actions


            except NoSuchElementException:
                pass
            
            save_info(driver,state,li_number)
            return d

        def single_scrape_pt(wbd, li_number, f_name, l_name):
            pause_time = 2 + random.random() * 2
            d = feature_dict(state, li_number, f_name, l_name)
            driver=wbd
            driver.execute_script("window.scrollTo(0, 200)")
            time.sleep(pause_time)
            license_button=driver.find_element_by_xpath('//*[@id="LicenseNumber"]')
#             ln_button=driver.find_element_by_name('LastName')
            license_button.clear()
#             ln_button.clear()
            license_button.send_keys(li_number)
#             ln_button.send_keys(l_name)
            
            driver.find_element_by_xpath('//*[(@id = "searchButton")]').click() # click the submit button
            time.sleep(pause_time)
            try:
                if 'No' in driver.find_element_by_xpath('//*[(@id = "searchResults")]').text:
                    return d
            except NoSuchElementException:
                pass
            
            try:
                driver.find_element_by_xpath('//*[@id="searchResults"]/div[1]/table/tbody/tr[1]/td[4]/a').click()
            except NoSuchElementException:
                return d

            try:

                ltype = driver.find_element_by_xpath('//*[@id="licenseType"]/span[2]').text
                lnum = driver.find_element_by_xpath('//*[@id="permanentId"]/span[2]').text
                exp_date = driver.find_element_by_xpath('//*[@id="licenseExpirationDate"]/span[2]').text
                lstatus = driver.find_element_by_xpath('//*[@id="licenseStatus"]/span[2]').text
                full_name=driver.find_element_by_css_selector('#personName > span.detailItem').text
                
                if (f_name.upper() not in full_name) and (l_name.upper() not in full_name):
                    return d  
                d['first_name']=full_name.split()[0]
                d['last_name']=full_name.split()[-1]
                d['license_type'] = ltype
                d['license_number'] = lnum
                d['expire_date'] = exp_date
                d['license_status'] = lstatus

            except NoSuchElementException:
                return d

            try:

                issue_date= driver.find_element_by_xpath('//*[@id="licenseIssueDate"]/span[2]').text
                complaint=driver.find_element_by_xpath('//*[@id="complaintsAndDisciplineContainer"]/div/div/span[2]').text
                board_actions=driver.find_element_by_xpath('//*[@id="complaintsAndDisciplineContainer"]/span[2]').text

                d['effective_date'] = issue_date
                d['board_sanction'] = complaint +' '+board_actions


            except NoSuchElementException:
                pass
            
            save_info(driver,state,li_number)
            return d
        sdata = sdata.astype(str)
        length = int(sdata.shape[0])
#         print('Number files = ',length)
        if stop == True:
            stop = length

        # Write a file for the output

        file=Results(state)
        file.w(headers)
        result=[]  
        number_files=int(stop-start)
        for i in range(start, stop):
            print('processing '+state+ ': '+str(i+1)+' of '+str(number_files),'\n')
            
            if '--proxy-server' in driver_params:
                if i%loop_break==int(loop_break-1):
                    driver.quit()
                    driver = get_driver(url,driver_params)
                    driver = try_get_url(driver,driver_params,url)
            
            li_number = str(sdata.iloc[i].loc['num'])
            f_name = sdata.iloc[i].loc['fn']
            l_name = sdata.iloc[i].loc['ln']
            driver = try_get_url(driver,driver_params,url)
            time.sleep(pause_time)

            #extract the input parameters for the scraper function
            try:
                
                output=single_scrape(driver, li_number, f_name, l_name)
            except Exception as inst:

                result.append((i,inst))
                driver = try_get_url(driver,driver_params,url)
                time.sleep(pause_time)
                continue
                
            if output['license_status']=='':
                driver= try_get_url(driver,driver_params,url_prof)
                time.sleep(pause_time)
                try:
                    output=single_scrape_prof(driver, li_number, f_name, l_name)
                except Exception as inst:

                    result.append((i,inst))
                    driver = try_get_url(driver,driver_params,url)
                    time.sleep(pause_time)
                    continue
                

            output['num']=li_number
            output['fn']=f_name
            output['ln']=l_name
            output['state']=state
            result.append((i,output))        
            file.w(output,method='add')

        driver.quit()
        return result

    except Exception as inst:
        print('Scraper Exception - Something bad happened in scraper_function not single_scrape')
        print(inst)
        if 'driver' in locals():
            try:
                driver.quit()
            except:
                pass
        return True,inst
    
####################################################################################################################################################################################################################################################################################################################################
### Oklahoma

def scrape_OK(sdata, driver_params, start=0, stop=True):
    try:
        state='OK'
        url = "http://www.okmedicalboard.org/search"
        url_soc="https://pay.apps.ok.gov/medlic/social/licensee_search.php"
        url_opt="https://optometry.ok.gov/search.htm" # not added
        
        driver_params.append('--webproxy')
        driver = get_driver(url,driver_params)

        def single_scrape(wbd, li_number, f_name, l_name):
            pause_time = 2 + random.random() * 2
            d = feature_dict(state, li_number, f_name, l_name)
            driver=wbd

            # select the last name search box
            l_name_search_box = driver.find_element_by_css_selector('#lname')
            l_name_search_box.send_keys(l_name)
            # select the last name search box
            f_name_search_box = driver.find_element_by_css_selector('#fname')
            f_name_search_box.send_keys(f_name)

            #click 
            submit_box=driver.find_element_by_xpath('//*[@id="advanced-search"]/tbody/tr[16]/td[2]/input[1]').click()
            time.sleep(pause_time)         

            #If no result found
            results = driver.find_element_by_xpath('//*[@id="center"]/div/p[2]').text
            if 'No results found' in results:
                d['license_type']='None Found'
                return d

            # they exist click their name
            result_name=driver.find_element_by_xpath('//*[@id="center"]/div/table/tbody/tr[2]/td[1]').click()
            time.sleep(pause_time) 

            # extract table
            table=driver.find_element_by_xpath('//*[@id="center"]/div/table/tbody').text.split('\n')
            for c in table:
                if "License Type" in c:
                    d['license_type'] = c.split(':')[-1].strip()
                if "Status Class" in c:
                    d['license_status'] = c.split(':')[-1].strip()
                if "Dated" in c:
                    d['effective_date'] = c.split(':')[-1].strip()
                if "Disciplinary History" in c:
                    d['board_sanction'] = c.split(':')[-1].strip()
                if "Expires" in c:
                    d['expire_date'] = c.split(':')[-1].strip()
            
            save_info(driver,state,li_number)
            return d
        
        def single_scrape_soc(wbd, li_number, f_name, l_name):
            pause_time = 2 + random.random() * 2
            d = feature_dict(state, li_number, f_name, l_name)
            driver=wbd

            license_num_box_link = 'LICENSE_NUMBER'
            first_name_box_link = 'FNAME'
            last_name_box_link = 'LNAME'
            search_button_link = 'SEARCH_BUTTON'

            # select the license number search box
            license_num_search_box = driver.find_element_by_name(license_num_box_link)

            # fill search box with license number
            license_num_search_box.clear()
            license_num_search_box.send_keys(li_number)

            # # select the lastname search box
            last_name_search_box = driver.find_element_by_name(last_name_box_link)

            # # fill search box with last name
            last_name_search_box.clear()
            last_name_search_box.send_keys(l_name)

            # select the first name search box
            first_name_search_box = driver.find_element_by_name(first_name_box_link)

            # fill search box with first name
            first_name_search_box.clear()
            first_name_search_box.send_keys(f_name)

            # click the search button
            driver.find_element_by_id(search_button_link).click()
            time.sleep(pause_time)

            results=driver.find_element_by_css_selector('body > div > div > div.content_wrapper > div.content_wrapper2 > div.body_content > div.print_wrapper > form > table').text.split('\n')

            found=False
            for i,el in enumerate(results):
                if i==0:
                    continue
                if (li_number and l_name in el.upper()):
                    found=True
                    break
            if not found:
                d['license_type']='None found'
                return d
            resultid='LICENSEE_'+str(i)
            try:
                driver.find_element_by_id(resultid).click()
            except NoSuchElementException:
                resultid='LICENSEE_0'
                driver.find_element_by_id(resultid).click()
            driver.find_element_by_id('VIEW_BUTTON').click()
            time.sleep(pause_time)

            content=driver.find_element_by_name('search_details').text.split('\n')

            for c in content:
                if 'Name' in c:
                    fullname=content[content.index(c)+1]
                    d['last_name']=fullname.split()[-1].strip().strip(',')
                    d['first_name']=fullname.split()[0].strip().strip(',')

                # Extract License type
                if "Level" in c:
                    d['license_type'] = content[content.index(c)+1].strip()

                if "License Number:" in c:
                    d['license_number'] = content[content.index(c)+1].strip()

                if "Status" in c:
                    d['license_status'] = content[content.index(c)+1].strip()

                if "Date First Licensed" in c:
                    d['effective_date'] = content[content.index(c)+1].strip()

                # Extract expire date
                if "Expiration Date" in c:
                    d['expire_date'] = content[content.index(c)+1].strip()

                # Extract license status
                if "License Status" in c:
                    d['license_status'] = content[content.index(c)+1].strip().strip()

                if "  Disciplinary Action" in c:
                    d['board_sanction'] = content[content.index(c)+1].strip()

            save_info(driver,state,li_number)
            return d
            
        sdata = sdata.astype(str)
        length = int(sdata.shape[0])
        if stop == True:
            stop = length

        # Write a file for the output

        file=Results(state)
        file.w(headers)
        result=[]  
        number_files=int(stop-start)
        for i in range(start, stop):
            print('processing '+state+ ': '+str(i+1)+' of '+str(number_files),'\n')
            
            if '--proxy-server' in driver_params:
                if i%loop_break==int(loop_break-1):
                    driver.quit()
                    driver = get_driver(url,driver_params)
                    driver = try_get_url(driver,driver_params,url)
                    
            li_number = str(sdata.iloc[i].loc['num'])
            f_name = sdata.iloc[i].loc['fn']
            l_name = sdata.iloc[i].loc['ln']
            if '--webproxy' not in driver_params:
                driver_params.append('--webproxy')
                driver = get_driver(url,driver_params)
            
            driver = try_get_url(driver,driver_params,url)
            time.sleep(pause_time)
            
            try:
                output=single_scrape(driver, li_number, f_name, l_name)
            except Exception as inst:
                result.append((i,inst))
                driver = try_get_url(driver,driver_params,url)
                time.sleep(pause_time)
                continue
                
            if output['license_status']=='':
#                 driver.quit()
#                 driver_params.remove('--webproxy')
#                 driver = get_driver(url,driver_params)
                driver = try_get_url(driver,driver_params,url_soc)
                time.sleep(pause_time)
                try:
                    output=single_scrape_soc(driver, li_number, f_name, l_name)
                except Exception as inst:
                    result.append((i,inst))
                    driver = try_get_url(driver,driver_params,url)
                    time.sleep(pause_time)
                    continue
                

            output['num']=li_number
            output['fn']=f_name
            output['ln']=l_name
            output['state']=state
            result.append((i,output))        
            file.w(output,method='add')

        driver.quit()
        return result

    except Exception as inst:
        print('Scraper Exception - Something bad happened in scraper_function not single_scrape')
        print(inst)
        if 'driver' in locals():
            try:
                driver.quit()
            except:
                pass
        return True,inst
    
def scrape_MN(sdata, driver_params, start=0, stop=True):
    try:
        state='MN'
        url = "http://docfinder.docboard.org/mn/df/mndf.htm"
        urlother=["phy","soc","chi","otp","opt"]
        urlbase=".hlb.state.mn.us/#/services/onlineLicenseSearch"
        url_nur='https://mbn.hlb.state.mn.us/#/services/onlineEntitySearch'

        driver = get_driver(url,driver_params)

        def single_scrape(wbd, li_number, f_name, l_name):
            pause_time = 2 + random.random() * 2
            d = feature_dict(state, li_number, f_name, l_name)
            driver=wbd

            wait = WebDriverWait(driver, 10)
            try:
                options=driver.find_element_by_xpath('/html/body/blockquote/b/form/font/select').text.split('\n')[1:]
            except Error as err:
                print(err)
                time.sleep(60)
            specialities=[s.split()[0] for s in options]
            # select the license_number search box

            specialty_order=[4,5,2,7,8,9,1,6,0,3,10]
            specialities_sorting = sorted(zip(specialities, specialty_order), key=lambda x: x[1])
            specialities_ordered = [item[0] for item in specialities_sorting][:3]
            specialities_ordered.append('zz')
            for s in specialities_ordered:
                if s=='zz':
                    return d
                VOLDEMORT=False
                linum=li_number
                addtype=False
                if not li_number[0].isupper(): # If starting string is not a letter
                    addtype=True
                    linum=s+li_number
                li_search_box = wait.until(EC.element_to_be_clickable((By.XPATH,'/html/body/blockquote/b/form/font/input[6]')),message='phys lisearch')
                li_search_box.clear()
                li_search_box.send_keys(linum)
                start_search=driver.find_element_by_xpath('/html/body/blockquote/b/form/font/input[7]').click()
                time.sleep(pause_time)
                result=''
                sol=0
                try:
                    result = driver.find_element_by_xpath('/html/body/b/b/b').text
                except NoSuchElementException:
                    pass
                if 'not found' in result:
                    driver.get(url)
                    if addtype==True:
                        continue
                    else:
                        return d
                else:
                    try:
                        table=driver.find_element_by_xpath('/html/body/b/b/b/center/b/center[1]/table').text.split('\n')
                        sol=1
                    except NoSuchElementException:
            #             try:
                        table=driver.find_element_by_css_selector('body > b > b > b > center > form > select').text.split('\n')
                        sol=2
            #             break
            #             except NoSuchElementException:
            #                 pass
                        pass
                    if sol==0:
                        driver.get(url)
                        time.sleep(pause_time)
                        continue
                    if sol==1:
                        name_check=' '.join([x.split('(')[0].upper() for x in table])
                        if (f_name in name_check) and (l_name in name_check):
                            break
                        elif f_name in name_check:
                            VOLDEMORT=True
                            break
                        else:
                            driver.get(url)
                            continue
                    if sol==2:
                        name_check=' '.join([x.split('(')[0].upper() for x in table])
                        found=False
                        for j,name in enumerate(name_check):
                            if (f_name in name) and (l_name in name):
                                found=True
                                found_num=j+1
                                break
                            elif f_name in name_check:
                                found=True
                                VOLDEMORT=True
                                found_num=j
                                break
                        if found==False:
                            driver.get(url)
                            continue
                        if found==True:
                            break

            if sol==2:
                person_found='body > b > b > b > center > form > select > option:nth-child('+str(found_num)+')'
                driver.find_element_by_css_selector(person_found).click()
                driver.find_element_by_css_selector('body > b > b > b > center > form > input[type=submit]:nth-child(7)').click()
                table=driver.find_element_by_xpath('/html/body/b/b/b/center/b/center[1]/table').text.split('\n')
                time.sleep(pause_time)

            if VOLDEMORT==True:
                d["last_name"]=name_check[-1]

            for c in table:
                if "License Type" in c:
                    d['license_type'] = c.split('Type')[-1].strip()
                if "License Status" in c:
                    d['license_status'] = c.split('Status')[-1].strip()
                if "Original License" in c:
                    d['effective_date'] = c.split()[-1].strip()
                if "Disciplinary Action" in c:
                    d['board_sanction'] = c.split()[-1].strip()
                if "License Expiration Date" in c:
                    d['expire_date'] = c.split()[-1].strip()

            save_info(driver,state,li_number)
            return d
        
        def single_scrape_other(wbd, li_number, f_name, l_name):
            pause_time = 2 + random.random() * 2
            d = feature_dict(state, li_number, f_name, l_name)
            driver = wbd
            wait = WebDriverWait(driver, 10)

            li_number=''.join(i for i in li_number if i.isdigit())
            # select the license_number search box
            licbox=driver.find_element_by_css_selector('#licenseNumber')
            licbox.clear()
            licbox.send_keys(li_number)

            driver.find_element_by_css_selector('.btn-sm').click()            
            time.sleep(pause_time*2)

            results = driver.find_element_by_xpath('/html/body/div[2]/div[2]/div[3]/div/fieldset/div/div/div[2]/fieldset/div[3]/div/div/div[2]').text

            if 'NO RESULTS' in results.upper():
                return d
            try:
                searchname=l_name[0]+l_name[1:].lower()+', '+f_name[0]+f_name[1:].lower()
                driver.find_element_by_partial_link_text(searchname).click()
            except NoSuchElementException:
                try:
                    searchname=l_name[0]+l_name[1:].lower()
                    driver.find_element_by_partial_link_text(searchname).click()
                except:
                    return d
                pass
            loaded='.panel-body'
            wait.until(EC.text_to_be_present_in_element((By.CSS_SELECTOR,loaded),'Printable View'),message='oth wait')

            table=driver.find_element_by_css_selector('.panel-body').text.split('\n')
            for c in table:

                if 'NAME' in c.upper():
                    fullname=table[table.index(c)+1].split('-')[0].strip()
                    d['first_name']=fullname.split(',')[-1].strip().upper()
                    d['last_name']=fullname.split(',')[0].strip().upper()

                if 'TYPE' in c.upper():
                    if 'DATE' not in c.upper():
                        d['license_number']=table[table.index(c)+1].split('-')[0].strip().upper()
                        d['license_type']=table[table.index(c)+1].split('-')[1].strip().upper()

                if 'STATUS' in c.upper():
                    if len(c)<7:
                        d['license_status']=table[table.index(c)+1].strip().upper().split('(')[0].strip()

                if 'LAST RENEWED' in c.upper():
                    d['effective_date']=table[table.index(c)+1].strip().upper()

                if 'EXPIRATION' in c.upper():
                    d['expire_date']=table[table.index(c)+1].strip().upper()

                if 'DATE ACTION TYPE' in c.upper():
                    d['board_sanction']=table[table.index(c)+1].strip().upper()

            save_info(driver,state,li_number)
            return d
        
        def single_scrape_nur(wbd, li_number, f_name, l_name):
            pause_time = 2 + random.random() * 2
            short_pause=random.randint(1,5) / 10 + random.random()
            d = feature_dict(state, li_number, f_name, l_name)
            driver=wbd

            wait = WebDriverWait(driver, 10)

            li_number = re.sub('\D', '', li_number)

            if 'R' in li_number:
                li_number = li_number.replace('R','').strip()

            license_num_box_link = 'licenseNumber'
            # first_name_box_link = 'firstName'
            last_name_box_link = 'lastName'
            # search_button_link = 'body > div:nth-child(2) > div.container.layout-content-area > div:nth-child(3) > div > fieldset > div > div > div > div > div.form-horizontal > fieldset > div.form-group > div > div > div > button'

            # select the license number search box

            license_num_search_box = driver.find_element_by_id(license_num_box_link)

            # fill search box with license number
            license_num_search_box.clear()
            license_num_search_box.send_keys(li_number)

            # select the lastname search box
            last_name_search_box = driver.find_element_by_id(last_name_box_link)

            # fill search box with last name
            last_name_search_box.clear()
            last_name_search_box.send_keys(l_name,'\uE007')

            noresults='body > div:nth-child(2) > div.container.layout-content-area > div:nth-child(3) > div > fieldset > div > div > div > div > div:nth-child(3) > div > div > div.form-horizontal > fieldset > div > div > div > div.pull-right.ng-scope'
            noresults=wait.until(EC.visibility_of_element_located((By.CSS_SELECTOR,noresults)),message='nur noresult').text
            if 'NO RESULTS' in noresults.upper():
                d['license_type']='None Found'
                return d

            results='body > div:nth-child(2) > div.container.layout-content-area > div:nth-child(3) > div > fieldset > div > div > div > div > div:nth-child(3) > div > div > div.ng-scope > div:nth-child(2) > uib-accordion > div > div'
            names_content=wait.until(EC.element_to_be_clickable((By.CSS_SELECTOR,results)),message='nur name').text.split('\n')
            found=False
            for c in names_content:
                if (l_name.upper() in c.upper()) | (', '+f_name.upper() in c.upper()):
                    driver.find_element_by_css_selector(results).click()
                    found=True

            if found==False:
                d['license_type']='None Found'
                return d

            content_link='body > div:nth-child(2) > div.container.layout-content-area > div:nth-child(3) > div > fieldset > div > div > div > div > div:nth-child(3)'
            wait.until(EC.text_to_be_present_in_element((By.CSS_SELECTOR,content_link),'Results'),message='nur wait1')

            person_link='body > div:nth-child(2) > div.container.layout-content-area > div:nth-child(3) > div > fieldset > div > div > div > div > div:nth-child(3) > div > div > div.ng-scope > div:nth-child(2) > uib-accordion > div > div:nth-child('
            found=False
            a=1
            while (found == False) and (a<4):
                clicker=person_link+str(a)+')'
                driver.find_element_by_css_selector(clicker).click()
                wait.until(EC.text_to_be_present_in_element((By.CSS_SELECTOR,content_link),'First Name'),message='nur wait2')

                text_link='body > div:nth-child(2) > div.container.layout-content-area > div:nth-child(3) > div > fieldset > div > div > div > div > div:nth-child(3) > div > div'
                content=driver.find_element_by_css_selector(text_link).text.split('\n')
                lic_number_stops=[]
                start=0
                stop=-1
                for i,c in enumerate(content):
                    if 'FIRST NAME' in c.upper():
                        d['first_name'] = content[content.index(c)+1].strip().upper()

                    if 'LAST NAME' in c.upper():
                        d['last_name'] = content[content.index(c)+1].strip().upper()

                    if li_number in c:
                        start=i

                    if 'LICENSE NUMBER' in c.upper():
                        lic_number_stops.append(i)

                    if 'DISCIPLINE TYPE' in c.upper():
                        d['board_sanction']=content[content.index(c)+1].strip().upper()

                fullname=d['first_name']+' '+d['last_name']
                if (l_name.upper() not in fullname.upper()) & (f_name.upper() not in fullname.upper()):
                    a+=1
                    continue

                found=True
                for j,num in enumerate(lic_number_stops):
                    if num>start+5:
                        stop=j
                        break
                if stop>0:
                    stop_pt=lic_number_stops[stop]-4
                else: 
                    stop_pt=stop

                start_pt=start-5
                content1=content[start_pt:stop_pt]

                d['license_status'] = content1[1].strip().upper()

                for c in content1:

                    # Extract License type
                    if "LICENSE NUMBER" in c.upper():
                        d['license_number'] = content1[content1.index(c)+1].split('#')[-1].strip().upper()        

                    if "LICENSE TYPE" in c.upper():
                        d['license_type'] = content1[content1.index(c)+1].strip().upper()  

                    if "ISSUE DATE" in c.upper():
                        d['effective_date'] = content1[content1.index(c)+1].strip().upper()  

                    if "EXPIRE DATE" in c.upper():
                        d['expire_date'] = content1[content1.index(c)+1].strip().upper()     

            save_info(driver,state,li_number)
            return d
            
        sdata = sdata.astype(str)
        length = int(sdata.shape[0])
        if stop == True:
            stop = length

        # Write a file for the output

        file=Results(state)
        file.w(headers)
        result=[]  
        number_files=int(stop-start)
        for i in range(start, stop):
            print('processing '+state+ ': '+str(i+1)+' of '+str(number_files),'\n')
            
            if '--proxy-server' in driver_params:
                if i%loop_break==int(loop_break-1):
                    driver.quit()
                    driver = get_driver(url,driver_params)
                    driver = try_get_url(driver,driver_params,url)
                    
            li_number = str(sdata.iloc[i].loc['num'])
            f_name = sdata.iloc[i].loc['fn']
            l_name = sdata.iloc[i].loc['ln']
            output=feature_dict(state, li_number, f_name, l_name)
            tried_nur=False
            if li_number[0].strip()=='R':
                tried_nur=True
                driver = try_get_url(driver,driver_params,url_nur)
                time.sleep(pause_time)
                try:
                    
                    output=single_scrape_nur(driver, li_number, f_name, l_name)

                except Exception as inst:
                    print(i,inst,'nur',li_number)
                    result.append((i,inst))
                    driver = try_get_url(driver,driver_params,url)
                    time.sleep(pause_time)
                    continue
                
            if output['license_status']=='':
                driver = try_get_url(driver,driver_params,url)
                time.sleep(pause_time)
                try:
                    output=single_scrape(driver, li_number, f_name, l_name)

                except Exception as inst:
                    print(i,inst,'phys',li_number)
                    result.append((i,inst))
                    driver = try_get_url(driver,driver_params,url)
                    time.sleep(pause_time)
                    continue

                if (output['license_status']=='') & (tried_nur==False):
                    driver = try_get_url(driver,driver_params,url_nur)
                    time.sleep(pause_time)

                    try:

                        output=single_scrape_nur(driver, li_number, f_name, l_name)

                    except Exception as inst:
                        print(i,inst,'nur',li_number)
                        result.append((i,inst))
                        driver = try_get_url(driver,driver_params,url)
                        time.sleep(pause_time)
                        continue    
                
                if (output['license_status']=='') & (not li_number[0].isupper()):
                    job=0
                    while (output['license_status']=='') and (job<4):
                        urls='https://'+urlother[job]+urlbase
                        driver = try_get_url(driver,driver_params,urls)
                        time.sleep(pause_time)
                        try:
                            output=single_scrape_other(driver, li_number, f_name, l_name)
                        except Exception as inst:
                            print(i,inst,'oth',li_number)
                            result.append((i,inst))
                            driver = try_get_url(driver,driver_params,url)
                            time.sleep(pause_time)
                            job+=1
                            continue
                        job+=1                  

            output['num']=li_number
            output['fn']=f_name
            output['ln']=l_name
            output['state']=state
            result.append((i,output))        
            file.w(output,method='add')
            driver = try_get_url(driver,driver_params,url)
            time.sleep(pause_time)
            
        driver.quit()
        return (result)

    except Exception as inst:
        print('Scraper Exception - Something bad happened in scraper_function not single_scrape')
        print(inst)
        if 'driver' in locals():
            try:
                driver.quit()
            except:
                pass
        return (True,inst)

###############################################################################
###############################################################################
###############################################################################
# State of Florida

# with "Additional Public Information" did not
# li_number is 5 digit number string.
def scrape_FL(sdata, driver_params, start=0, stop=True):
    try:
        state='FL'
        url = "https://appsmqa.doh.state.fl.us/MQASearchServices/HealthCareProviders"
        driver = get_driver(url,driver_params)

        def single_scrape(wbd, li_number, f_name, l_name):
            pause_time = 2 + random.random() * 2
            d = feature_dict(state, li_number, f_name, l_name)
            driver=wbd
            # select the license box for searching
            if li_number[:4]=='ARNP':
                li_number='APRN'+li_number[4:]
            licbox=driver.find_element_by_css_selector('#SearchDto_LicenseNumber')
            licbox.click()
            licbox.send_keys(li_number)

            # select the firstname
            fnbox = driver.find_element_by_css_selector('#SearchDto_FirstName')
            fnbox.click()
            fnbox.send_keys(f_name)

            # select the lastname
#             lnbox = driver.find_element_by_css_selector('#SearchDto_LastName')
#             lnbox.send_keys(l_name)

            # click the search button
            driver.find_element_by_xpath('//*[@id="content"]/div/form/fieldset/p/input').click()
            time.sleep(pause_time)
            multiple=False
            try:
                driver.find_element_by_link_text(li_number)
                multiple=True
            except NoSuchElementException:
                pass
            if multiple==True:
                try:
                    driver.find_element_by_link_text(li_number).click()
                    time.sleep(pause_time)
                except NoSuchElementException:
                    d['license_type']='None Found'
                    return d
            try:
                driver.find_element_by_id('NoResultsModal')
                d['license_type']='None Found'
                return d
            except NoSuchElementException:
                pass

            table=driver.find_element_by_css_selector('fieldset').text.split('\n')
            litype=table[table.index('Profession')+1]
            linum=table[table.index('License')+1]
            listat=table[table.index('License Status')+1]
            effdat=table[table.index('License Original Issue Date')+1]
            expirdat=table[table.index('License Expiration Date')+1]
            boardsan=table[table.index('Discipline on File')+1]

            d['license_type']=litype
            d['license_number']=linum
            d['license_status']=listat
            d['effective_date']=effdat
            d['expire_date']=expirdat
            d['board_sanction']=boardsan
            
            save_info(driver,state,li_number)
            return d
            
        sdata = sdata.astype(str)
        length = int(sdata.shape[0])
        if stop == True:
            stop = length

        # Write a file for the output

        file=Results(state)
        file.w(headers)
        result=[]  
        number_files=int(stop-start)
        for i in range(start, stop):
            print('processing '+state+ ': '+str(i+1)+' of '+str(number_files),'\n')
            
            if '--proxy-server' in driver_params:
                if i%loop_break==int(loop_break-1):
                    driver.quit()
                    driver = get_driver(url,driver_params)
                    driver = try_get_url(driver,driver_params,url)
            
            li_number = str(sdata.iloc[i].loc['num'])
            f_name = sdata.iloc[i].loc['fn']
            l_name = sdata.iloc[i].loc['ln']
            
            driver = try_get_url(driver,driver_params,url)
            time.sleep(pause_time)

            try:
                output=single_scrape(driver, li_number, f_name, l_name)
            except Exception as inst:
                print(inst)
                result.append((i,inst))
                driver = try_get_url(driver,driver_params,url)
                time.sleep(pause_time)
                continue

            output['num']=li_number
            output['fn']=f_name
            output['ln']=l_name
            output['state']=state
            result.append((i,output))        
            file.w(output,method='add')

        driver.quit()
        return result

    except Exception as inst:
        print('Scraper Exception - Something bad happened in scraper_function not single_scrape')
        print(inst)
        if 'driver' in locals():
            try:
                driver.quit()
            except:
                pass
        return True,inst
    
###############################################################################
###############################################################################
###############################################################################
# State of Maine

# with "Additional Public Information" did not
# li_number is 5 digit number string.
def scrape_ME(sdata, driver_params, start=0, stop=True):
    try:
        state='ME'
        url = "https://www.pfr.maine.gov/ALMSOnline/ALMSQuery/SearchIndividual.aspx?Board=376&AspxAutoDetectCookieSupport=1"
        driver = get_driver(url,driver_params)

        def single_scrape(wbd, li_number, f_name, l_name):
            pause_time = 2 + random.random() * 2
            d = feature_dict(state, li_number, f_name, l_name)
            driver=wbd

            wait = WebDriverWait(driver, 10)

            #Select all
            select=driver.find_element_by_css_selector('#scRegulator')
            select.send_keys('ALL')

            #license box
            licbox=driver.find_element_by_css_selector('#scLicenseNo')
            licbox.clear()
            licbox.send_keys(li_number)

            # select the firstname
            fnbox = driver.find_element_by_css_selector('#scFirstName')
            fnbox.clear()
            fnbox.send_keys(f_name)

            # select the lastname
            lnbox = driver.find_element_by_css_selector('#scLastName')
            lnbox.clear()
            lnbox.send_keys(l_name)

            # click the search button
            driver.find_element_by_css_selector('#btnSearch').click()
            time.sleep(pause_time)

            # Check if someone is found
            try:
                if 'PLEASE' in driver.find_element_by_id('validationSummary').text.upper():
                    d['license_type']='None Found'
                    return d
            except NoSuchElementException:
                pass

            searchnames=[f_name,l_name,l_name+', '+f_name]
            found=False
            for name in searchnames[::-1]:
                try:
                    namelink=wait.until(EC.element_to_be_clickable((By.PARTIAL_LINK_TEXT,name)),message='name')
                    newlink=namelink.get_attribute('href')
                    driver.get(newlink)
                    time.sleep(pause_time)
                    found=True
                    break
                except TimeoutException:
                    continue    

            if found ==False:
                d['license_type']='None Found'
                return d

            litype=''
            effdat=''

            content=driver.find_element_by_id('detailPage').text.split('\n')
            start_num=0
            lic_nums=[]
            for i,c in enumerate(content):
                if "LICENSE NUMBER:" in c.upper():
                    if (content[i+1].strip().upper() in li_number.strip().upper()) | (li_number.strip().upper() in content[i+1]):
                        start_num=i
                    lic_nums.append(i)

            end_num=-1
            if (len(lic_nums)>0) and (max(lic_nums)>start_num):
                end_num=[x for x in lic_nums if x>start_num][0]

            table=content[start_num:end_num]

            try:
                table2=table[table.index('License Type Start Date End Date')+1].split()
                if len(table2[0:-2])>1:
                    litype=' '.join(table2[0:-2])
                else:
                    litype=table2[0]
                effdat=table2[-2]
                expirdat=table2[-1]
            except: 
                expirdat=table[table.index('Expiration Date:')+1]

            linum=table[table.index('License Number:')+1]
            listat=table[table.index('Status:')+1]
            try:
                boardsan=table[table.index('License/Disciplinary Action')+2]
                d['board_sanction']=boardsan
            except (NoSuchElementException, ValueError):
                pass
            d['license_type']=litype.upper()
            d['license_number']=linum.upper()
            d['license_status']=listat.upper()
            d['effective_date']=effdat.upper()
            d['expire_date']=expirdat.upper()

            save_info(driver,state,li_number)
            return d
            
        sdata = sdata.astype(str)
        length = int(sdata.shape[0])
        if stop == True:
            stop = length

        # Write a file for the output

        file=Results(state)
        file.w(headers)
        result=[]  
        number_files=int(stop-start)
        for i in range(start, stop):
            print('processing '+state+ ': '+str(i+1)+' of '+str(number_files),'\n')
            
            if '--proxy-server' in driver_params:
                if i%loop_break==int(loop_break-1):
                    driver.quit()
                    driver = get_driver(url,driver_params)
                    driver = try_get_url(driver,driver_params,url)
            
            li_number = str(sdata.iloc[i].loc['num'])
            f_name = sdata.iloc[i].loc['fn']
            l_name = sdata.iloc[i].loc['ln']
            
            driver = try_get_url(driver,driver_params,url)
            time.sleep(pause_time)
            
            try:
                output=single_scrape(driver, li_number, f_name, l_name)
            except Exception as inst:

                result.append((i,inst))
                driver = try_get_url(driver,driver_params,url)
                time.sleep(pause_time)
                continue

            output['num']=li_number
            output['fn']=f_name
            output['ln']=l_name
            output['state']=state
            result.append((i,output))        
            file.w(output,method='add')

        driver.quit()
        return result

    except Exception as inst:
        print('Scraper Exception - Something bad happened in scraper_function not single_scrape')
        print(inst)
        if 'driver' in locals():
            try:
                driver.quit()
            except:
                pass
        return True,inst

###############################################################################
###############################################################################
###############################################################################
# State of CA

# with "Additional Public Information" did not
# li_number is 5 digit number string.
def scrape_CA(sdata, driver_params, start=0, stop=True):
    try:
        state='CA'
        url = "https://search.dca.ca.gov"
        driver = get_driver(url,driver_params)

        def single_scrape(wbd, li_number, f_name, l_name):
            pause_time = 2 + random.random() * 2
            d = feature_dict(state, li_number, f_name, l_name)
            driver=wbd
            driver.execute_script("window.scrollTo(0, 500)")    
            try:
                # Clear previous results    
                driver.find_element_by_css_selector('#clearHome').click()

                # select the license box
                linum_box = driver.find_element_by_css_selector("#licenseNumber")

                # fill search box with license number
                linum_box.send_keys(li_number)  # "OT-A1309"

                # select the first name box
                fn_box = driver.find_element_by_css_selector("#firstName")

                # fill fn box with first name
                fn_box.send_keys(f_name)  # "JAMES"

                # select the last name box
                ln_box = driver.find_element_by_css_selector("#lastName")

                # fill ln box with last name
                ln_box.send_keys(l_name)  # "MCINTYRE"

                # click verify button
                driver.find_element_by_css_selector("#srchSubmitHome").click()
                time.sleep(pause_time)

                moreDetailsurl = driver.find_element_by_id("mD0").get_attribute('href')
                driver.get(moreDetailsurl)
                time.sleep(pause_time)
            except NoSuchElementException:
                return d

            try:
                d['license_number'] = str(driver.find_element_by_id("licDetail").text.split(':')[1]).strip("b'")
                details = driver.find_element_by_class_name("detailContainer")
                d['license_type']=details.find_element_by_id("licType").text.split(':')[1].strip("b'")
                d['license_status']=details.find_element_by_id("primaryStatus").text.split(':')[1].strip("b'")
                
                dates = driver.find_element_by_class_name("meta")
                issue_date = dates.find_element_by_id("issueDate").text
                exp_date = dates.find_element_by_id("expDate").text

                d['effective_date']=issue_date
                d['expire_date']=exp_date
            except NoSuchElementException:
                return d
            
            save_info(driver,state,li_number)
            return d
        
        sdata = sdata.astype(str)
        length = int(sdata.shape[0])
        if stop == True:
            stop = length

        # Write a file for the output

        file=Results(state)
        file.w(headers)
        result=[]  
        number_files=int(stop-start)
        for i in range(start, stop):
            print('processing '+state+ ': '+str(i+1)+' of '+str(number_files),'\n')
            
            if '--proxy-server' in driver_params:
                if i%loop_break==int(loop_break-1):
                    driver.quit()
                    driver = get_driver(url,driver_params)
                    driver = try_get_url(driver,driver_params,url)
            
            li_number = str(sdata.iloc[i].loc['num'])
            f_name = sdata.iloc[i].loc['fn']
            l_name = sdata.iloc[i].loc['ln']
            
            driver = try_get_url(driver,driver_params,url)
            time.sleep(pause_time)
            
            try:
                output=single_scrape(driver, li_number, f_name, l_name)
            except Exception as inst:

                result.append((i,inst))
                driver = try_get_url(driver,driver_params,url)
                time.sleep(pause_time)
                continue

            output['num']=li_number
            output['fn']=f_name
            output['ln']=l_name
            output['state']=state
            result.append((i,output))        
            file.w(output,method='add')

        driver.quit()
        return result

    except Exception as inst:
        print('Scraper Exception - Something bad happened in scraper_function not single_scrape')
        print(inst)
        if 'driver' in locals():
            try:
                driver.quit()
            except:
                pass
        return True,inst

##################################################
#################################################
####Nevada

def scrape_NV(sdata, driver_params, start=0, stop=True):
    try:
        state='NV'
        url="https://nsbme.mylicense.com/verification/Search.aspx"
        url_soc="https://services.socwork.nv.gov/verify/index.asp"
        driver = get_driver(url,driver_params)
        
        def single_scrape(wbd, li_number, f_name, l_name):
            pause_time = 2 + random.random() * 2
            d = feature_dict(state, li_number, f_name, l_name)
            driver=wbd

            # select the license number search box
            license_num_search_box = driver.find_element_by_css_selector('#t_web_lookup__license_no')

            # fill search box with license number
            license_num_search_box.clear()
            license_num_search_box.send_keys(li_number)

            # select the last name search box
            last_name_search_box = driver.find_element_by_css_selector('#t_web_lookup__last_name')

            # fill search box with last name
            last_name_search_box.clear()
            last_name_search_box.send_keys(l_name)

            # select the first name search box
            first_name_search_box = driver.find_element_by_css_selector('#t_web_lookup__first_name')

            # fill search box with first name
            first_name_search_box.clear()
            first_name_search_box.send_keys(f_name)

            # click the search button
            driver.find_element_by_css_selector('#sch_button').click()
            time.sleep(pause_time)

            try:
                searchname=l_name+', '+f_name[0]+f_name[1:].lower()
                link=driver.find_element_by_partial_link_text(searchname).get_attribute('href')
                driver.get(link)
            except NoSuchElementException:
                d['license_type']='None Found'
                return d

            content = driver.find_element_by_css_selector('#TheForm > table').text.split('\n')
            for c in content:

                # Extract License Type
                if "License Type:" in c:
                    d['license_type'] = c.split('License Type: ')[-1]

                # Extract effective date
                if "Issue Date:" in c:
                    d['effective_date'] = c.split('Date:')[1].split()[0]   

                # Extract expire date
                if "Expiration Date:" in c:
                    d['expire_date'] = c.split('Expiration Date')[-1].replace(':','')

                # Extract license status
                if "Status:" in c:
                    d['license_status'] = c.split('Status: ')[-1]

                # extract the "Additional Public Information" for sanction
                if 'BOARD ACTIONS' in c:
                    idx = content.index(c)
                    d['board_sanction'] = content[idx + 1].strip()

            save_info(driver,state,li_number)
            return d

        def single_scrape_soc(wbd, li_number, f_name, l_name):
            pause_time = 2 + random.random() * 2
            d = feature_dict(state, li_number, f_name, l_name)
            driver=wbd

            # select the license number search box
            license_num_search_box = driver.find_element_by_css_selector('#inputLicenseNo')

            # fill search box with license number
            license_num_search_box.clear()
            license_num_search_box.send_keys(li_number)

            # select the last name search box
            last_name_search_box = driver.find_element_by_css_selector('#inputLastName')

            # fill search box with last name
            last_name_search_box.clear()
            last_name_search_box.send_keys(l_name)

            # select the first name search box
            first_name_search_box = driver.find_element_by_css_selector('#inputFirstName')

            # fill search box with first name
            first_name_search_box.clear()
            first_name_search_box.send_keys(f_name)

            # click the search button
            driver.find_element_by_css_selector('#form > div > button.btn.btn-primary').click()
            time.sleep(pause_time*3)
            noresult=''
            try:
                noresult=driver.find_element_by_css_selector('#form > div.error.alert.alert-warning').text
            except NoSuchElementException:
                pass

            if len(noresult)>0:
                return d

            try:
                searchname=l_name+' '+f_name
                driver.find_element_by_partial_link_text(searchname).click()
            except NoSuchElementException:
                pass

            results=driver.find_element_by_css_selector("#printcontent > table:nth-child(4) > tbody").text.split()

            d['license_type']=results[0]
            d['effective_date']=results[2]
            d['expire_date']=results[3]
            d['license_status']=results[4]
            try:
                d['board_sanction']=driver.find_element_by_css_selector('#printcontent > table:nth-child(6) > tbody > tr:nth-child(1) > td:nth-child(1)').text
            except NoSuchElementException:
                pass
        
            save_info(driver,state,li_number)
            return d
        
        sdata = sdata.astype(str)
        length = int(sdata.shape[0])
        if stop == True:
            stop = length
                
        # Write a file for the output

        file=Results(state)
        file.w(headers)
        result=[]  
        number_files=int(stop-start)
        for i in range(start, stop):
            print('processing '+state+ ': '+str(i+1)+' of '+str(number_files),'\n')
            
            if '--proxy-server' in driver_params:
                if i%loop_break==int(loop_break-1):
                    driver.quit()
                    driver = get_driver(url,driver_params)
                    driver = try_get_url(driver,driver_params,url)
            
            li_number = str(sdata.iloc[i].loc['num'])              
            f_name = sdata.iloc[i].loc['fn']
            l_name = sdata.iloc[i].loc['ln']
            if 'RN' in li_number:
                output=feature_dict(state, li_number, f_name, l_name)
                output['license_type']='Nurse'
                output['num']=li_number
                output['fn']=f_name
                output['ln']=l_name
                output['state']=state
                result.append((i,output))
                file.w(output,method='add')
                continue
            
            driver = try_get_url(driver,driver_params,url)
            time.sleep(pause_time)
            try:
                output=single_scrape(driver, li_number, f_name, l_name)
            except Exception as inst:

                result.append((i,inst))
                driver = try_get_url(driver,driver_params,url)
                time.sleep(pause_time)
                continue

            # if output['license_status']=="":
                # driver_params_soc=driver_params.copy()
                # driver_params_soc.append('--webproxy')
                # driver = try_get_url(driver,driver_params_soc,url_soc)
                # time.sleep(pause_time)
                # try:
                    # output=single_scrape_soc(driver, li_number, f_name, l_name)
                    # driver.quit()
                    # driver = get_driver(url,driver_params)
                # except Exception as inst:

                    # result.append((i,inst))
                    # driver.quit()
                    # driver = get_driver(url,driver_params)
                    # driver = try_get_url(driver,driver_params,url)
                    # time.sleep(pause_time)
                    # continue
                        
            output['num']=li_number
            output['fn']=f_name
            output['ln']=l_name
            output['state']=state
            result.append((i,output))        
            file.w(output,method='add')
            driver = try_get_url(driver,driver_params,url)
            time.sleep(pause_time)
        driver.quit()
        return result

    except Exception as inst:
        print('Scraper Exception - Something bad happened in scraper_function not single_scrape')
        print(inst)
        if 'driver' in locals():
            try:
                driver.quit()
            except:
                pass
        return True,inst
        

###############################################################################
###############################################################################
###############################################################################
# State of NY

def scrape_NY(sdata, driver_params, start=0, stop=True):
    try:
        state='NY'
        url = "http://www.op.nysed.gov/opsearches.htm#licno"
        driver = get_driver(url,driver_params)
        driver = try_get_url(driver,driver_params,url)
        short_pause=random.randint(1,5) / 10 + random.random()
        time.sleep(pause_time)

        def single_scrape(wbd, li_number, f_name, l_name):
            pause_time = 2 + random.random() * 2
            d = feature_dict(state, li_number, f_name, l_name)
            driver=wbd

            driver.execute_script("window.scrollTo(0, 500)")

            license_num_box_link = '//*[@id="content_column"]/div[2]/form/center[2]/input'
            search_button_link = '//*[@id="content_column"]/div[2]/form/center[3]/input[1]'

            licTypedict={
                'MDR':'Medicine ',
                'PAS':'Physician Assistant',
                'RNU':'Nurse, ',
                'SWK':'Social Worker (',
                'DPM':'Podiatrist',
                'PSY':'Psychologist',
                'DCH':'Chiropractor',
                'PHT':'Physical Therapist',
                'OPT':'Optometrist',
                'DEN':'Dentist ',
                'AUD':'Audiologist',
                }
            all_options=[]
            for licType in licTypedict.keys():
                el=driver.find_element_by_css_selector('#content_column > div:nth-child(11) > form > center:nth-child(1) > select')
                options=el.find_elements_by_tag_name('option')
                opt_ls=[]
                for choice in options:
                    opt_ls.append(choice.text)

            #                 print('trying ',licType,'\n')
                if licType=='RNU':
                    RNU_index=opt_ls.index('Nurse, Practical')
                    opt_ls.pop(RNU_index)
                    options.pop(RNU_index)

                if licType=='PAS':
                    PAS_index=opt_ls.index('Registered Physician Assistant')
                    opt_ls.pop(PAS_index)
                    options.pop(PAS_index)

                if licType=='PHT':
                    PHT_index=opt_ls.index('Physical Therapist Assistant')
                    opt_ls.pop(PHT_index)
                    options.pop(PHT_index)


                checkoptions=[opt_ls.index(s) for s in opt_ls if licTypedict[licType] in s]

                for check in checkoptions:

                    try:
                        el=driver.find_element_by_css_selector('#content_column > div:nth-child(11) > form > center:nth-child(1) > select')
                        profoptions=el.find_elements_by_tag_name('option')

                        # Clear previous results    
                        driver.find_element_by_xpath('//*[@id="content_column"]/div[2]/form/center[3]/input[2]').click()

            #             # Select option
                        profoptions[check].click()

            #             all_options.append(profoptions[check])
            #         except NoSuchElementException:
            #             pass


                        # License number box   
                        linum_box = driver.find_element_by_xpath(license_num_box_link)
                        linum_box.send_keys(li_number)

                        # Search box  
                        search_box = driver.find_element_by_xpath('//*[@id="content_column"]/div[2]/form/center[3]/input[1]').click()
                        time.sleep(short_pause)

                        content = driver.find_element_by_xpath('//*[@id="content_column"]').text.split('\n')
                    except NoSuchElementException:
                        driver.get(url)
                        time.sleep(short_pause)
                        continue

                    found=False
                    for c in content:
                        if "Name" in c:
                            if (f_name in c) and (l_name in c):
                                found=True
                                break

                    if found==True:
                        break
                    else:
                        driver.get(url)
                        time.sleep(short_pause)
                if found==True:
                    break
                else:
                    driver.get(url)
                    time.sleep(short_pause)
            if found==False:
                d['license_type']='None Found'
                return d

            for c in content:
                if "Profession :" in c:
                    d['license_type'] = c.split(':')[-1].strip()
                if "Status :" in c:
                    d['license_status'] = c.split(':')[-1].strip()
                if "Registered " in c:
                    date=c.split(':')[-1].strip()
                    if len(date)>0:
                        date_month=str(int(date.split('/')[0])+1)
                        date_year=str(int(date.split('/')[-1]))
                        if int(date_month)>12:
                            date_month='01'
                            date_year=str(int(date_year)+1)

                        if len(date_month)==1:
                            date_month='0'+date_month

                        if len(date_year)==1:
                            date_year='0'+date_year

                        date_updated=str(date_month)+'/01/'+str(date_year)
                        d['expire_date'] = date_updated

                if "Disciplinary History" in c:
                    d['board_sanction'] = c.split(':')[-1].strip()
                if "Expires" in c:
                    date=c.split(':')[-1].strip()
                    if len(date)>0:
                        date_month=str(int(date.split('/')[0])+1)
                        date_year=str(int(date.split('/')[-1]))
                        if int(date_month)>12:
                            date_month='01'
                            date_year=str(int(date_year)+1)

                        if len(date_month)==1:
                            date_month='0'+date_month

                        if len(date_year)==1:
                            date_year='0'+date_year

                        date_updated=str(date_month)+'/01/'+str(date_year)
                        d['expire_date'] = date_updated

                if "Date of Licensure" in c:
                    d['effective_date']=c.split(':')[-1].strip()

            save_info(driver,state,li_number)
            return d
            
        sdata = sdata.astype(str)
        length = int(sdata.shape[0])
        if stop == True:
            stop = length

        # Write a file for the output

        file=Results(state)
        file.w(headers)
        result=[]  
        number_files=int(stop-start)
        for i in range(start, stop):
            print('processing '+state+ ': '+str(i+1)+' of '+str(number_files),'\n')
            
            if '--proxy-server' in driver_params:
                if i%loop_break==int(loop_break-1):
                    driver.quit()
                    driver = get_driver(url,driver_params)
                    driver = try_get_url(driver,driver_params,url)
            
            li_number = str(sdata.iloc[i].loc['num'])
            f_name = sdata.iloc[i].loc['fn']
            l_name = sdata.iloc[i].loc['ln']
            #extract the input parameters for the scraper function
            try:
                driver = try_get_url(driver,driver_params,url)
                time.sleep(pause_time/2)
                output=single_scrape(driver, li_number, f_name, l_name)
            except Exception as inst:

                result.append((i,inst))
                driver = try_get_url(driver,driver_params,url)
                time.sleep(pause_time)
                continue

            output['num']=li_number
            output['fn']=f_name
            output['ln']=l_name
            output['state']=state
            result.append((i,output))        
            file.w(output,method='add')

        driver.quit()
        return result

    except Exception as inst:
        print('Scraper Exception - Something bad happened in scraper_function not single_scrape')
        print(inst)
        if 'driver' in locals():
            try:
                driver.quit()
            except:
                pass
        return True,inst
#######################################################################
#######################################################################
#######################################################################
# ## New Jersey

def scrape_NJ(sdata, driver_params, start=0, stop=True):
    try:
        state='NJ'
        url = "http://newjersey.mylicense.com/Verification/Search.aspx?facility=N"
        driver = get_driver(url,driver_params)
        driver = try_get_url(driver,driver_params,url)
        short_pause=random.randint(1,5) / 10 + random.random()
        time.sleep(pause_time)

        def single_scrape(wbd, li_number, f_name, l_name):
            pause_time = 2 + random.random() * 2
            d = feature_dict(state, li_number, f_name, l_name)
            driver=wbd

            pause_time = 2 + random.random() * 2

            license_num_box_link = '//*[@id="t_web_lookup__license_no"]'
            first_name_box_link = '//*[@id="t_web_lookup__first_name"]'
            last_name_box_link = '//*[@id="t_web_lookup__last_name"]'
            search_button_link = '//*[@id="sch_button"]'

            # Select the clear box
            driver.find_element_by_xpath('//*[@id="clear"]').click()

            # select the license number search box
            license_num_search_box = driver.find_element_by_xpath(license_num_box_link)

            # fill search box with license number

            license_num_search_box.send_keys(li_number)

            # select the lastname search box
            last_name_search_box = driver.find_element_by_xpath(last_name_box_link)

            # fill search box with last name

            last_name_search_box.send_keys(l_name)

            # select the first name search box
            first_name_search_box = driver.find_element_by_xpath(first_name_box_link)

            # fill search box with first name

            first_name_search_box.send_keys(f_name)

            # click the search button
            driver.find_element_by_xpath(search_button_link).click()
            time.sleep(pause_time)

            ln_links=driver.find_elements_by_partial_link_text(l_name.strip())
            fn_links=driver.find_elements_by_partial_link_text(f_name.strip())
            found=False
            for ln_link in ln_links:
                for fn_link in fn_links: 
                    if ln_link==fn_link:
                        found=True
                        ln_link.click()
                        time.sleep(pause_time)
            if found==False:
                d['license_type']='None Found'
                return d

            try:
                d['first_name']=driver.find_element_by_id('full_name').text.split(' ')[0]

                d['last_name']=driver.find_element_by_id('full_name').text.split(' ')[-1]
                lic_type = driver.find_element_by_id('license_type').text.split(',')[0]

                d['license_type']=lic_type

                d['license_number']=driver.find_element_by_id('license_no').text
                d['license_status']=driver.find_element_by_id('sec_lic_status').text
                d['expire_date']=driver.find_element_by_id('expiration_date').text
                d['effective_date']=driver.find_element_by_id('issue').text

                try:
                    d['board_sanction']=driver.find_element_by_xpath('//*[@id="TheForm"]/table/tbody/tr[2]/td/table[2]/tbody/tr[16]/td/span/table').text.replace('\n','')
                except NoSuchElementException:
                    d['board_sanction']=''
                
                save_info(driver,state,li_number)
                return d
            except NoSuchElementException:
                
                return d

            
        sdata = sdata.astype(str)
        length = int(sdata.shape[0])
        if stop == True:
            stop = length

        # Write a file for the output

        file=Results(state)
        file.w(headers)
        result=[]  
        number_files=int(stop-start)
        for i in range(start, stop):
            print('processing '+state+ ': '+str(i+1)+' of '+str(number_files),'\n')
            
            if '--proxy-server' in driver_params:
                if i%loop_break==int(loop_break-1):
                    driver.quit()
                    driver = get_driver(url,driver_params)
                    driver = try_get_url(driver,driver_params,url)
            
            li_number = str(sdata.iloc[i].loc['num'])
            f_name = sdata.iloc[i].loc['fn']
            l_name = sdata.iloc[i].loc['ln']
            #extract the input parameters for the scraper function
            try:
                output=single_scrape(driver, li_number, f_name, l_name)
            except Exception as inst:

                result.append((i,inst))
                driver = try_get_url(driver,driver_params,url)
                time.sleep(pause_time)
                continue

            output['num']=li_number
            output['fn']=f_name
            output['ln']=l_name
            output['state']=state
            result.append((i,output))        
            file.w(output,method='add')
            driver = try_get_url(driver,driver_params,url)
            time.sleep(pause_time)
        driver.quit()
        return result

    except Exception as inst:
        print('Scraper Exception - Something bad happened in scraper_function not single_scrape')
        print(inst)
        if 'driver' in locals():
            try:
                driver.quit()
            except:
                pass
        return True,inst

#######################################################################
#######################################################################
#######################################################################
## MD

def scrape_MD(sdata, driver_params, start=0, stop=True):
    try:
        state='MD'
        url = 'https://www.mbp.state.md.us/bpqapp/'
        urlnur = "http://lookup.mbon.org/verification/Search.aspx"
        urlbase = 'https://mdbnc.health.maryland.gov/'
        urlother = ['bsweVerification/default.aspx','bptVerification/default.aspx']
        driver = get_driver(url,driver_params)
        time.sleep(pause_time)

        def single_scrape(wbd, li_number, f_name, l_name):
            pause_time = 2 + random.random() * 2
            d = feature_dict(state, li_number, f_name, l_name)
            driver=wbd

            if len(li_number)!=6:
                if (len(li_number)==8) and ('00' in li_number):
                    li_number=li_number.replace('00','',1)
                elif (len(li_number)==7) and ('0' in li_number):
                    li_number=li_number.replace('0','',1)
                else:
                    d['license_type']='None Found physician'
                    return d

            license_num_box_link = 'Lic_No'
            search_button_link = 'btnLICNO'

            # select the license number search box
            license_num_search_box = driver.find_element_by_id(license_num_box_link)

            # fill search box with license number

            license_num_search_box.send_keys(li_number)

            # click the search button
            driver.find_element_by_id(search_button_link).click()
            time.sleep(pause_time)

            try:
                fullname=driver.find_element_by_id('lblName').text
                if l_name.split()[0].upper() not in fullname.upper():
                    d['license_type']='None Found physician'
                    return d
                d['last_name']=fullname.split()[-1].strip()
                d['first_name']=fullname.split()[0].strip()
                driver.find_element_by_id('btnLICNO2').click()
                time.sleep(pause_time)
            except NoSuchElementException as e:
                d['license_type']='None Found physician'
                return d

            content=driver.find_element_by_css_selector('#form1 > div.container > div.col-sm-8.blog-main > div:nth-child(1)').text.split('\n')

            for c in content:
                c1=c
                c=c.upper()
                if "LICENSE TYPE" in c:
                    d['license_type'] = c.split(':')[-1].strip()
                if "LICENSE NUMBER" in c:
                    d['license_number'] = c.split(':')[-1].split()[0].strip()

                if "LICENSE STATUS" in c:
                    d['license_status'] = c.split(':')[-1].strip()

                if "LICENSE EXPIRATION" in c:
                    d['expire_date'] = c.split('EXPIRATION:')[-1].strip()

                if "LICENSE ISSUED" in c:
                    d['expire_date'] = c.split('ISSUED:')[0].strip()

            try:
                board=driver.find_element_by_css_selector('#form1 > div.container > div.col-sm-8.blog-main > div:nth-child(8)').text.split('\n')
                d['board_sanction']=board[1]
            except NoSuchElementException:
                pass
            
            save_info(driver,state,li_number)
            return d
        
        def single_scrape_nurse(wbd, li_number, f_name, l_name):
            pause_time = 2 + random.random() * 2
            d = feature_dict(state, li_number, f_name, l_name)
            driver=wbd
            
            select=Select(driver.find_element_by_id('t_web_lookup__profession_name'))

            select.select_by_value('Nursing')
            time.sleep(pause_time)

            license_num_box_link = 't_web_lookup__license_no'
            first_name_box_link = 't_web_lookup__first_name'
            last_name_box_link = 't_web_lookup__last_name'
            search_button_link = '//*[@id="sch_button"]'

            # select the license number search box
            license_num_search_box = driver.find_element_by_id(license_num_box_link)

            # fill search box with license number

            license_num_search_box.send_keys(li_number)

            # # select the lastname search box
            # last_name_search_box = driver.find_element_by_id(last_name_box_link)

            # # fill search box with last name

            # last_name_search_box.send_keys(l_name)

            # select the first name search box
            first_name_search_box = driver.find_element_by_id(first_name_box_link)

            # fill search box with first name

            first_name_search_box.send_keys(f_name)

            # click the search button
            driver.find_element_by_xpath(search_button_link).click()
            time.sleep(pause_time)
            try:
                searchname=l_name+', '+f_name
                link=driver.find_element_by_partial_link_text(searchname).get_attribute('href')
                driver.get(link)
                time.sleep(pause_time)
            except NoSuchElementException:
                try:
                    searchname=', '+f_name
                    link=driver.find_element_by_partial_link_text(searchname).get_attribute('href')
                    driver.get(link)
                    time.sleep(pause_time)
                except NoSuchElementException:
                    d['license_type']='None Found nurse'
                    return d
                pass

            try:
                full_name=driver.find_element_by_id('ctl23_ctl01_full_name_new').text

                d['last_name']=full_name.split()[1].strip()
                d['first_name']=full_name.split()[0].strip()
            except NoSuchElementException:
                pass
            
            d['license_number'] = driver.find_element_by_xpath('//*[@id="ctl33_ctl01_license_no"]').text
            d['license_type'] = driver.find_element_by_id('ctl33_ctl01_license_type').text     
            d['license_status'] = driver.find_element_by_id('ctl33_ctl01_status').text
            d['expire_date'] = driver.find_element_by_id('ctl33_ctl01_expiry').text
            d['effective_date'] = driver.find_element_by_id('ctl33_ctl01_label_issue_date').text
            
            save_info(driver,state,li_number)
            return d
        
        def single_scrape_other(wbd, li_number, f_name, l_name):
            pause_time = 2 + random.random() * 2
            d = feature_dict(state, li_number, f_name, l_name)
            driver=wbd
            
            if len(li_number)<5:
                while len(li_number)<5:
                    li_number='0'+li_number

            driver.find_element_by_css_selector('#bodyContentPlaceHolder_reset1').click()
            time.sleep(pause_time)

            license_num_box_link = '#bodyContentPlaceHolder_licenseNoSearch'
            search_button_link = '#bodyContentPlaceHolder_search2'

            # select the license number search box
            license_num_search_box = driver.find_element_by_css_selector(license_num_box_link)

            # fill search box with license number

            license_num_search_box.send_keys(li_number)

            # click the search button
            driver.find_element_by_css_selector(search_button_link).click()
            time.sleep(pause_time)

            try:
                results=driver.find_element_by_css_selector('#form > div:nth-child(4) > table > tbody > tr:nth-child(2) > td:nth-child(2) > span').text
                d['license_type']='None Found other'
                return d
            except NoSuchElementException:
                pass
            table=driver.find_element_by_css_selector('#bodyContentPlaceHolder_verificationLicGridView').text.split('\n')
            found=False
            searchf_name=f_name[0]+f_name[1:].lower()
            searchl_name=l_name[0]+l_name[1:].lower()
            for n,row in enumerate(table):
                if ((f_name in row) and (l_name in row)) or ((searchf_name in row) and (searchl_name in row)):
                    try:
                        css_selector='#bodyContentPlaceHolder_verificationLicGridView > tbody > tr:nth-child('+str(n+1)+') > td:nth-child(6) > a'
                        driver.find_element_by_css_selector(css_selector).click()
                    except:
                        try:
                            css_selector='#bodyContentPlaceHolder_verificationLicGridView > tbody > tr:nth-child('+str(n+1)+') > td:nth-child(5) > a'
                            driver.find_element_by_css_selector(css_selector).click()
                        except:
                            pass

                    time.sleep(pause_time)
                    found=True
            if found==False:
                d['license_type']='None Found other'
                return d

            content=driver.find_element_by_css_selector('#bodyContentPlaceHolder_DetailsView1 > tbody').text.split('\n')
            for c in content:
                if "License Type " in c:
                    d['license_type'] = c.split('Type')[-1].strip()
                if "Expiration Date " in c:
                    d['expire_date'] = c.split()[-1].strip()
                if "License Number" in c:
                    d['license_number'] = c.split()[-1].strip()
                if "Full Name" in c:
                    fullname=c.split('Full Name ')[-1].strip()
                    d['first_name'] = fullname.split()[0]
                    d['last_name'] = fullname.split()[-1]
                if "Original" in c:
                    d['effective_date']=c.split()[-1].strip()

                if "License Level " in c:
                    d['license_type'] = c.split('Level')[-1].strip()

                if "Status" in c:
                    d['license_status'] = c.split('Status')[-1].strip()

                if "Disciplinary Action" in c:
                    d['board_sanction'] = c.split()[-1].strip()


            if (d['expire_date']!='') and (d['license_status']==''):
                currentdate = datetime.datetime.strptime(d['License_As_Of_Date'], "%Y/%m/%d")
                expiredate=datetime.datetime.strptime(d['expire_date'], "%m/%d/%Y")
                if currentdate>expiredate:
                    d['license_status']='Expired'
                else:
                    d['license_status']='Active'
            
            save_info(driver,state,li_number)
            return d
        
        sdata = sdata.astype(str)
        length = int(sdata.shape[0])
        if stop == True:
            stop = length

        # Write a file for the output

        file=Results(state)
        file.w(headers)
        result=[]  
        number_files=int(stop-start)
        for i in range(start, stop):
            print('processing '+state+ ': '+str(i+1)+' of '+str(number_files),'\n')
            
            if '--proxy-server' in driver_params:
                if i%loop_break==int(loop_break-1):
                    driver.quit()
                    driver = get_driver(url,driver_params)
                    driver = try_get_url(driver,driver_params,url)
            
            li_number = str(sdata.iloc[i].loc['num'])
            f_name = sdata.iloc[i].loc['fn']
            l_name = sdata.iloc[i].loc['ln']
            
            if (li_number[0:2]=='LP') or (li_number[0]=='R'):
                driver = try_get_url(driver,driver_params,urlnur)
                time.sleep(pause_time)
                try:
                    output=single_scrape_nurse(driver, li_number, f_name, l_name)
                except Exception as inst:
                    result.append((i,inst))
                    driver = try_get_url(driver,driver_params,url)
                    time.sleep(pause_time)
                    continue
                    
            elif (len(li_number)<=5) or (len(li_number.strip().split()[-1])<=5):
                output=feature_dict(state, li_number, f_name, l_name)
                job=0
                while (output['license_status']=='') and (job<2):
                    urls=urlbase+urlother[job]
#                     print(urls)
                    driver = try_get_url(driver,driver_params,urls)
                    try:
                        output=single_scrape_other(driver, li_number, f_name, l_name)
                    except Exception as inst:
                        print(inst)
                        result.append((i,inst))
                        continue
                    job+=1
                
            else:
                #extract the input parameters for the scraper function
                driver = try_get_url(driver,driver_params,url)
                time.sleep(pause_time)
                try:
                    output=single_scrape(driver, li_number, f_name, l_name)
                except Exception as inst:

                    result.append((i,inst))
                    driver = try_get_url(driver,driver_params,url)
                    time.sleep(pause_time)
                    continue

            output['num']=li_number
            output['fn']=f_name
            output['ln']=l_name
            output['state']=state
            result.append((i,output))        
            file.w(output,method='add')
        driver.quit()
        return result

    except Exception as inst:
        print('Scraper Exception - Something bad happened in scraper_function not single_scrape')
        print(inst)
        if 'driver' in locals():
            try:
                driver.quit()
            except:
                pass
        return True,inst

###############################################################################
###############################################################################
###############################################################################
# State of Missouri

# with "Additional Public Information" did not
# li_number is 5 digit number string.
def scrape_MO(sdata, driver_params, start=0, stop=True):
    try:
        state='MO'
        url = "https://renew.pr.mo.gov/licensee-search.asp"
        url_na = "https://pr.mo.gov/licensee-search-nonactive.asp"
        url_sw = "https://pr.mo.gov/socialworkers-licensee-search.asp"

        driver = get_driver(url,driver_params)
        driver = try_get_url(driver,driver_params,url)
        time.sleep(pause_time)

        def single_scrape(wbd, li_number, f_name, l_name):
#             print('ss')
            pause_time = 2 + random.random() * 2
            d = feature_dict(state, li_number, f_name, l_name)
            driver=wbd
            
            wait = WebDriverWait(driver, 10)

            # search by license
            driver.find_elements_by_id('optionsRadios2')[1].click()
                
            license_num_box_link = '.radio .form-control'
            search_button_link = '.btn-primary'

            # select the license number search box
            license_num_search_box = driver.find_element_by_css_selector(license_num_box_link)

            # fill search box with license number
            license_num_search_box.clear()
            license_num_search_box.send_keys(li_number)

            # click the search button
            driver.find_element_by_css_selector(search_button_link).click()

            results=wait.until(EC.presence_of_element_located((By.CSS_SELECTOR,'#main-content > table:nth-child(4) > tbody > tr:nth-child(2) > td'))).text.upper()
                
            # Find name in table of results
            table_num='6'
            table_id='#main-content > table:nth-child('+table_num+')'
            table=driver.find_element_by_css_selector(table_id).text.split('\n')
            searchl_name=l_name.upper()
            searchf_name=f_name.upper()
            if '-' in searchf_name:
                searchf_name_split=searchf_name.split('-')
                searchf_name=searchf_name_split[0]

            searchname=searchl_name+', '+searchf_name
            linknum=''
            for row in table:
                row=row.upper()
                if (searchname in row) or ((searchf_name in row) and (searchl_name in row)):
                    linknum=int(row[0])-1

            if len(str(linknum))<1:
                d['license_type']='None Found'
                return d

            link_list=driver.find_elements_by_tag_name('a')
            searchlinks=[s.get_attribute('href') for s in link_list if 'passkey' in s.get_attribute('href')]
            link=searchlinks[linknum]
            driver.get(link)
            time.sleep(pause_time)

            tablerows=driver.find_elements_by_css_selector('td')
            content=[s.text for s in tablerows]
            if content==[]:
                d['license_type']='Not Found'
                return d
            
            for c in content:
                c1=c.upper()
                if "PROFESSION NAME:" in c1:
                    d['license_type'] = content[content.index(c)+1].strip().upper()
                if "EXPIRATION DATE:" in c1:
                    d['expire_date'] = content[content.index(c)+1].strip().upper()
                if "LICENSEE NUMBER:" in c1:
                    d['license_number'] = content[content.index(c)+1].strip().upper()
                if "LICENSEE NAME" in c1:
                    fullname=content[content.index(c)+1].strip().upper()
                    d['first_name'] = fullname.split(',')[1]
                    d['last_name'] = fullname.split(',')[0]
                if "ORIGINAL" in c1:
                    d['effective_date']=content[content.index(c)+1].strip().upper()

                if "CURRENT DISCIPLINE STATUS:" in c1:
                    d['board_sanction'] = content[content.index(c)+1].strip().upper()

                if "LICENSE STATUS:" in c1:
                    d['license_status']=content[content.index(c)+1].strip().upper()

            if (d['expire_date']!='') & (d['license_status']==''):
                currentdate = datetime.datetime.strptime(d['License_As_Of_Date'], "%Y/%m/%d")
                expiredate=datetime.datetime.strptime(d['expire_date'], "%m/%d/%Y")
                if currentdate>expiredate:
                    d['license_status']='EXPIRED'
                else:
                    d['license_status']='ACTIVE'

            if d['license_status']=='':
                return d

            save_info(driver,state,li_number)
            return d
        
        def single_scrape_na(wbd, li_number, f_name, l_name):
#             print('na')
            pause_time = 2 + random.random() * 2
            d = feature_dict(state, li_number, f_name, l_name)
            driver=wbd
            
            wait = WebDriverWait(driver, 10)

            # search by license
            driver.find_elements_by_id('optionsRadios2')[1].click()
            
            license_num_box_link = '.radio .form-control'
            search_button_link = '.btn-primary'

            # select the license number search box
            license_num_search_box = driver.find_element_by_css_selector(license_num_box_link)

            # fill search box with license number
            license_num_search_box.clear()
            license_num_search_box.send_keys(li_number)

            # click the search button
            driver.find_element_by_css_selector(search_button_link).click()

            results=wait.until(EC.presence_of_element_located((By.CSS_SELECTOR,'#main-content > table:nth-child(5) > tbody > tr:nth-child(2) > td'))).text.upper()

            # Find name in table of results
            table_num='7'
            table_id='#main-content > table:nth-child('+table_num+')'
            table=driver.find_element_by_css_selector(table_id).text.split('\n')
            searchl_name=l_name.upper()
            searchf_name=f_name.upper()
            if '-' in searchf_name:
                searchf_name_split=searchf_name.split('-')
                searchf_name=searchf_name_split[0]

            searchname=searchl_name+', '+searchf_name
            linknum=list(range(100))
            for row in table:
                row=row.upper()
                if (searchname in row) or ((searchf_name in row) and (searchl_name in row)):
                    linknum=int(row[0])-1

            if len(str(linknum))==390:
                d['license_type']='None Found'
                return d

            link_list=driver.find_elements_by_tag_name('a')
            searchlinks=[s.get_attribute('href') for s in link_list if 'passkey' in s.get_attribute('href')]
            link=searchlinks[linknum]
            driver.get(link)
            time.sleep(pause_time)

            tablerows=driver.find_elements_by_css_selector('td')
            content=[s.text for s in tablerows]
            if content==[]:
                d['license_type']='Not Found'
                return d

            for c in content:
                c1=c.upper()
                if "PROFESSION NAME:" in c1:
                    d['license_type'] = content[content.index(c)+1].strip().upper()
                if "EXPIRATION DATE:" in c1:
                    d['expire_date'] = content[content.index(c)+1].strip().upper()
                if "LICENSEE NUMBER:" in c1:
                    d['license_number'] = content[content.index(c)+1].strip().upper()
                if "LICENSEE NAME" in c1:
                    fullname=content[content.index(c)+1].strip().upper()
                    d['first_name'] = fullname.split(',')[1]
                    d['last_name'] = fullname.split(',')[0]
                if "ORIGINAL" in c1:
                    d['effective_date']=content[content.index(c)+1].strip().upper()

                if "CURRENT DISCIPLINE STATUS:" in c1:
                    d['board_sanction'] = content[content.index(c)+1].strip().upper()

                if "LICENSE STATUS:" in c1:
                    d['license_status']=content[content.index(c)+1].strip().upper()

            if (d['expire_date']!='') & (d['license_status']==''):
                currentdate = datetime.datetime.strptime(d['License_As_Of_Date'], "%Y/%m/%d")
                expiredate=datetime.datetime.strptime(d['expire_date'], "%m/%d/%Y")
                if currentdate>expiredate:
                    d['license_status']='EXPIRED'
                else:
                    d['license_status']='ACTIVE'

            if d['license_status']=='':
                return d

            save_info(driver,state,li_number)
            return d
        
        def single_scrape_sw(wbd, li_number, f_name, l_name):
#             print('sw')
            pause_time = 2 + random.random() * 2
            d = feature_dict(state, li_number, f_name, l_name)
            driver=wbd
#             time.sleep(300)
            wait = WebDriverWait(driver, 10)

            # search by license
            driver.find_element_by_css_selector('#main-content > form > fieldset > div:nth-child(3) > div > div:nth-child(3) > label > input[type=radio]').click()

            license_num_box_link = 'inputEmail'
            search_button_link = '#main-content > form > fieldset > div:nth-child(4) > div > button.btn.btn-primary'

            # select the license number search box
            license_num_search_box = driver.find_element_by_id(license_num_box_link)

            # fill search box with license number
            license_num_search_box.clear()
            license_num_search_box.send_keys(li_number)

            # click the search button
            driver.find_element_by_css_selector(search_button_link).click()

            results=wait.until(EC.presence_of_element_located((By.CSS_SELECTOR,'#main-content > table:nth-child(6) > tbody > tr:nth-child(2)'))).text.upper()
            table_num='8'

            # Find name in table of results
            table_id='#main-content > table:nth-child('+table_num+')'
            table=driver.find_element_by_css_selector(table_id).text.split('\n')
            searchl_name=l_name.upper()
            searchf_name=f_name.upper()
            if '-' in searchf_name:
                searchf_name_split=searchf_name.split('-')
                searchf_name=searchf_name_split[0]

            searchname=searchl_name+', '+searchf_name
            linknum=''
            for row in table:
                row=row.upper()
                if (searchname in row) or ((searchf_name in row) and (searchl_name in row)):
                    linknum=int(row[0])-1
#             print(li_number,linknum,len(str(linknum)))
            if len(str(linknum))<1:
                d['license_type']='None Found'
                return d

            link_list=driver.find_elements_by_tag_name('a')
            searchlinks=[s.get_attribute('href') for s in link_list if 'passkey' in s.get_attribute('href')]
            link=searchlinks[linknum]
            driver.get(link)
            time.sleep(pause_time)

            tablerows=driver.find_elements_by_css_selector('td')
            content=[s.text for s in tablerows]
            if content==[]:
                d['license_type']='Not Found'
                return d

            for c in content:
                c1=c.upper()
                if "PROFESSION:" in c1:
                    d['license_type'] = content[content.index(c)+1].strip().upper()
                if "EXPIRATION DATE:" in c1:
                    d['expire_date'] = content[content.index(c)+1].strip().upper()
                if "LICENSEE NUMBER:" in c1:
                    d['license_number'] = content[content.index(c)+1].strip().upper()
                if "LICENSEE NAME" in c1:
                    fullname=content[content.index(c)+1].strip().upper()
                    d['first_name'] = fullname.split(',')[1]
                    d['last_name'] = fullname.split(',')[0]
                if "ORIGINAL" in c1:
                    d['effective_date']=content[content.index(c)+1].strip().upper()

                if "LICENSE STATUS:" in c1:
                    d['license_status']=content[content.index(c)+1].strip().upper()

            if (d['expire_date']!='') & (d['license_status']==''):
                currentdate = datetime.datetime.strptime(d['License_As_Of_Date'], "%Y/%m/%d")
                expiredate=datetime.datetime.strptime(d['expire_date'], "%m/%d/%Y")
                if currentdate>expiredate:
                    d['license_status']='EXPIRED'
                else:
                    d['license_status']='ACTIVE'

            try:
                d['board_sanction']=driver.find_element_by_css_selector('#main-content > p:nth-child(6)').text.split(':')[-1].strip()
            except NoSuchElementException:
                pass

            if d['license_status']=='':
                return d

            save_info(driver,state,li_number)
            return d
        
        sdata = sdata.astype(str)
        length = int(sdata.shape[0])
        if stop == True:
            stop = length

        # Write a file for the output

        file=Results(state)
        file.w(headers)
        result=[]  
        number_files=int(stop-start)
        for i in range(start, stop):
            print('processing '+state+ ': '+str(i+1)+' of '+str(number_files),'\n')
            
            if '--proxy-server' in driver_params:
                if i%loop_break==int(loop_break-1):
                    driver.quit()
                    driver = get_driver(url,driver_params)
                    driver = try_get_url(driver,driver_params,url)
            
            li_number = str(sdata.iloc[i].loc['num'])
            f_name = sdata.iloc[i].loc['fn']
            l_name = sdata.iloc[i].loc['ln']
            #extract the input parameters for the scraper function
            try:
                output=single_scrape(driver, li_number, f_name, l_name)
#                 print(i, output)
            except Exception as inst:
                print(i, inst)
                result.append((i,inst))
                driver = try_get_url(driver,driver_params,url)
                time.sleep(pause_time)
                continue
                
            if output['license_status']=='':
                driver = try_get_url(driver,driver_params,url_na)
                time.sleep(pause_time)
                try:
                    output=single_scrape_na(driver, li_number, f_name, l_name)
#                 print(i, output)
                except Exception as inst:
                    print(i, inst)
                    result.append((i,inst))
                    driver = try_get_url(driver,driver_params,url)
                    time.sleep(pause_time)
                    continue
               
            if output['license_status']=='':
                driver = try_get_url(driver,driver_params,url_sw)
                time.sleep(pause_time)
                try:
                    output=single_scrape_sw(driver, li_number, f_name, l_name)
#                 print(i, output)
                except Exception as inst:
                    print(i, inst)
                    result.append((i,inst))
                    driver = try_get_url(driver,driver_params,url)
                    time.sleep(pause_time)
                    continue
            
            output['num']=li_number
            output['fn']=f_name
            output['ln']=l_name
            output['state']=state
            result.append((i,output))        
            file.w(output,method='add')
            driver = try_get_url(driver,driver_params,url)
            time.sleep(pause_time)
        driver.quit()
        return result

    except Exception as inst:
        print('Scraper Exception - Something bad happened in scraper_function not single_scrape')
        print(inst)
        if 'driver' in locals():
            try:
                print('driver quit')
#                 driver.quit()
            except:
                pass
        return True,inst
    
######################################
######################################
# State of Pennsylvania

def scrape_PA(sdata, driver_params, start=0, stop=True):
    try:
        state='PA'
        url = "https://www.pals.pa.gov/#/page/search"
        driver = get_driver(url,driver_params)

        def single_scrape(wbd, li_number, f_name, l_name):
            pause_time = 2 + random.random() * 2
            d = feature_dict(state, li_number, f_name, l_name)
            driver=wbd
            
            wait = WebDriverWait(driver, 10)

            license_num_box_link = 'LicenseNo'
            first_name_box_link = 'FirstName'
            last_name_box_link = 'LastName'
            search_button_link = '//*[@id="SearchFilter"]/div[6]/div/div/button[1]'

            # select the license number search box
            license_num_search_box = wait.until(EC.presence_of_element_located((By.NAME,license_num_box_link)),message='Lic Num')

            # fill search box with license number
            license_num_search_box.clear()
            license_num_search_box.send_keys(li_number)

            # # select the lastname search box
            last_name_search_box = driver.find_element_by_name(last_name_box_link)

            # # fill search box with last name
            last_name_search_box.clear()
            last_name_search_box.send_keys(l_name)

            # select the first name search box
            first_name_search_box = driver.find_element_by_name(first_name_box_link)

            # fill search box with first name
            first_name_search_box.clear()
            first_name_search_box.send_keys(f_name)

            # click the search button
            driver.find_element_by_xpath(search_button_link).click()

            searchnames=[' '+l_name,f_name+' ']
            found=False
            for name in searchnames[::-1]:
                try:
                    link=wait.until(EC.element_to_be_clickable((By.PARTIAL_LINK_TEXT,name)),message='name')
                    found=True
                    break
                except TimeoutException:
                    continue
            if found ==False:
                print('No link found')
                d['license_type']='None found'
                return d
            try:
                link.click()
            except ElementClickInterceptedException:
                driver.execute_script("window.scrollTo(0, 500)")
                driver.execute_script("arguments[0].click();", link)
            time.sleep(pause_time)

            handles=driver.window_handles
            driver.close()
            try:
                driver.switch_to.window(handles[1])
            except IndexError:
                d['license_type']='Found wrong link'
                return d
            content=wait.until(EC.presence_of_element_located((By.ID,'panelDemo2')),message='last').text.split('\n')

            for c in content:
                if 'License Information' in c:
                    if ':'in content[content.index(c)+1]:
                        d['license_type']='Page Messed Up'
                        return d
                    fullname=content[content.index(c)+1]
                    d['last_name']=fullname.split()[-1].strip().upper()
                    d['first_name']=fullname.split()[0].strip().upper()

                # Extract License type
                if "LicenseType:" in c:
                    d['license_type'] = content[content.index(c)+1].strip().upper()

                if "License Number:" in c:
                    d['license_number'] = content[content.index(c)+1].strip().upper()

                if "Status:" in c:
                    d['license_status'] = content[content.index(c)+1].strip().upper()

                if "Effective Date:" in c:
                    d['effective_date'] = content[content.index(c)+1].strip().upper()

                # Extract expire date
                if "Expiration Date:" in c:
                    d['expire_date'] = content[content.index(c)+1].strip().upper()

                # Extract license status
                if "License Status" in c:
                    d['license_status'] = content[content.index(c)+1].strip().upper()

            # extract the "Additional Public Information" for sanction
            try:
                d['board_sanction'] = driver.find_element_by_id('panelDemo3').text.split('\n')[-1].strip().upper()
            except NoSuchElementException:
                pass
            
            save_info(driver,state,li_number)
            return d
        
        sdata = sdata.astype(str)
        length = int(sdata.shape[0])
        if stop == True:
            stop = length

        # Write a file for the output

        file=Results(state)
        file.w(headers)
        result=[]  
        number_files=int(stop-start)
        for i in range(start, stop):
            print('processing '+state+ ': '+str(i+1)+' of '+str(number_files),'\n')
            
            if '--proxy-server' in driver_params:
                if i%loop_break==int(loop_break-1):
                    driver.quit()
                    driver = get_driver(url,driver_params)
                    driver = try_get_url(driver,driver_params,url)
            
            li_number = str(sdata.iloc[i].loc['num'])
            f_name = sdata.iloc[i].loc['fn']
            l_name = sdata.iloc[i].loc['ln']
            #extract the input parameters for the scraper function
            try:
                driver = try_get_url(driver,driver_params,url)
                time.sleep(pause_time)
                output=single_scrape(driver, li_number, f_name, l_name)
#                 print(i, output)
            except Exception as inst:
                print(i,li_number,f_name,l_name, inst)
                
                result.append((i,inst))
                driver.quit()
                time.sleep(pause_time)
                driver = get_driver(url,driver_params)
                driver = try_get_url(driver,driver_params,url)
                time.sleep(pause_time)
                continue

            output['num']=li_number
            output['fn']=f_name
            output['ln']=l_name
            output['state']=state
            result.append((i,output))        
            file.w(output,method='add')

        driver.quit()
        return result

    except Exception as inst:
        print('Scraper Exception - Something bad happened in scraper_function not single_scrape')
        print(inst)
        if 'driver' in locals():
            try:
                driver.quit()
            except:
                pass
        return True,inst
    
# state of Indiana
# we can add names attribute directly from variable 'names'
def scrape_IN(sdata, driver_params, start=0, stop=True):
    try:
        state='IN'
        url = "https://mylicense.in.gov/everification/"
        driver = get_driver(url,driver_params)
        
        def single_scrape(wbd, li_number, f_name, l_name):
            pause_time = 2 + random.random() * 2
            d = feature_dict(state, li_number, f_name, l_name)
            driver=wbd

            license_num_box_link = 't_web_lookup__license_no'
            first_name_box_link = 't_web_lookup__first_name'
            last_name_box_link = 't_web_lookup__last_name'
            search_button_link = 'sch_button'

            # select the license number search box
            license_num_search_box = driver.find_element_by_id(license_num_box_link)

            # fill search box with license number

            license_num_search_box.send_keys(li_number)

            # # select the lastname search box
            last_name_search_box = driver.find_element_by_id(last_name_box_link)

            # # fill search box with last name

            last_name_search_box.send_keys(l_name)

            # select the first name search box
#             first_name_search_box = driver.find_element_by_id(first_name_box_link)

            # fill search box with first name

#             first_name_search_box.send_keys(f_name)

            # click the search button
            driver.find_element_by_id(search_button_link).click()
            time.sleep(pause_time)
            searchnames=[l_name+', '+f_name,l_name[0]+l_name[1:].lower()+', '+f_name[0]+f_name[1:].lower(),', '+f_name,', '+f_name[0]+f_name[1:].lower(),l_name+', ',l_name[0]+l_name[1:].lower()+', ']
            found =False
            for name in searchnames[::-1]:
#                 print(name)
                try:
                    namefound=driver.find_element_by_partial_link_text(name)
                    link = namefound.get_attribute('href')
                    driver.get(link)
                    time.sleep(pause_time)
                    found=True
                    break
                except (NoSuchElementException, WebDriverException) as e:
#                     print(e)
                    pass
            if not found:
                d['license_type']='None Found'
                return d

            try:
                full_name=driver.find_element_by_id('_ctl27__ctl1_full_name').text

                d['last_name']=full_name.split(',')[0].split()[-1].strip()
                d['first_name']=full_name.split(',')[0].split()[0].strip()
            except NoSuchElementException:
                pass

            d['license_number'] = driver.find_element_by_id('_ctl37__ctl1_license_no').text
            d['license_type'] = driver.find_element_by_id('_ctl37__ctl1_license_type').text     
            d['license_status'] = driver.find_element_by_id('_ctl37__ctl1_status').text
            d['expire_date'] = driver.find_element_by_id('_ctl37__ctl1_expiry').text
            d['effective_date'] = driver.find_element_by_id('_ctl37__ctl1_issue_date').text
            
            save_info(driver,state,li_number)
            return d
                                    

        sdata = sdata.astype(str)
        length = int(sdata.shape[0])
        if stop == True:
            stop = length

    # Write a file for the output
        # Write a file for the output

        file=Results(state)
        file.w(headers)
        result=[]  
        number_files=int(stop-start)
        for i in range(start, stop):
            print('processing '+state+ ': '+str(i+1)+' of '+str(number_files),'\n')
            
            if '--proxy-server' in driver_params:
                if i%loop_break==int(loop_break-1):
                    driver.quit()
                    driver = get_driver(url,driver_params)
                    driver = try_get_url(driver,driver_params,url)
            
            li_number = str(sdata.iloc[i].loc['num'])
            f_name = sdata.iloc[i].loc['fn']
            l_name = sdata.iloc[i].loc['ln']
            driver = try_get_url(driver,driver_params,url)
            time.sleep(pause_time)
            
            try:
                output=single_scrape(driver, li_number, f_name, l_name)
            except Exception as inst:
                print(inst)
                result.append((i,inst))
                driver = try_get_url(driver,driver_params,url)
                time.sleep(pause_time)
                continue

            output['num']=li_number
            output['fn']=f_name
            output['ln']=l_name
            output['state']=state
            result.append((i,output))        
            file.w(output,method='add')

        driver.quit()
        return result

    except Exception as inst:
        print('Scraper Exception - Something bad happened in scraper_function not single_scrape')
        print(inst)
        if 'driver' in locals():
            try:
                driver.quit()
            except:
                pass
        return True,inst
    
# state of LA
# 
def scrape_LA(sdata, driver_params, start=0, stop=True):
    try:
        state='LA'
        url = "https://online.lasbme.org/#/verifylicense"
        url_soc = "https://www.labswe.org/index.cfm/licensee-search"
        url_pt = "https://www.laptboard.org/index.cfm/pt-search#tab-3-search"
        driver = get_driver(url,driver_params)
        
        def single_scrape(wbd, li_number, f_name, l_name):
            pause_time = 2 + random.random() * 2
            short_pause=random.randint(1,5) / 10 + random.random()
            d = feature_dict(state, li_number, f_name, l_name)
            driver=wbd

            wait = WebDriverWait(driver, 10)

            license_num_box_link = 'licenseNumber'
            first_name_box_link = 'firstName'
            last_name_box_link = 'lastName'
            search_button_link = 'body > div.ng-scope > div.middle-content.ng-scope > div > div > div > form > div:nth-child(12) > div > input.btn.btn-success'

            # select the license number search box
            license_num_search_box = driver.find_element_by_name(license_num_box_link)

            # fill search box with license number
            license_num_search_box.clear()
            license_num_search_box.send_keys(li_number)

            # # select the lastname search box
            #             last_name_search_box = driver.find_element_by_id(last_name_box_link)

            # # fill search box with last name
            # last_name_search_box.clear()
            #             last_name_search_box.send_keys(l_name)

            # select the first name search box
            first_name_search_box = driver.find_element_by_name(first_name_box_link)

            # fill search box with first name
            first_name_search_box.clear()
            first_name_search_box.send_keys(f_name)

            # click the search button
            driver.find_element_by_css_selector(search_button_link).click()
            wait.until(EC.visibility_of_element_located((By.CSS_SELECTOR,'body > div.ng-scope > div.middle-content.ng-scope > div > div > div > div.searchoption.mt30.ng-scope > div:nth-child(2) > table > tbody')),message=f"LA 1, {li_number}")
            time.sleep(short_pause)
            results=driver.find_element_by_css_selector('body > div.ng-scope > div.middle-content.ng-scope > div > div > div > div.searchoption.mt30.ng-scope > div.gridUpperDiv.mt30.print-no-display').text
            if '0 OF 0' in results.upper():
                d['license_type']='None Found 1'
                return d

            wait.until(EC.element_to_be_clickable((By.CSS_SELECTOR,'body > div.ng-scope > div.middle-content.ng-scope > div > div > div > div.searchoption.mt30.ng-scope > div:nth-child(2) > table > tbody > tr > td:nth-child(8) > input')),message=f"LA 2, {li_number}")
            table1=driver.find_element_by_css_selector('body > div.ng-scope > div.middle-content.ng-scope > div > div > div > div.searchoption.mt30.ng-scope > div:nth-child(2) > table').text.split('\n')
            table2='a'
            while table1 != table2:
                table2=driver.find_element_by_css_selector('body > div.ng-scope > div.middle-content.ng-scope > div > div > div > div.searchoption.mt30.ng-scope > div:nth-child(2) > table').text.split('\n')
                time.sleep(short_pause)
                table1=driver.find_element_by_css_selector('body > div.ng-scope > div.middle-content.ng-scope > div > div > div > div.searchoption.mt30.ng-scope > div:nth-child(2) > table').text.split('\n')

            table=table2
            num=''
            for i,c in enumerate(table):
                if (li_number.upper() in c.upper()) or (f_name.upper() and l_name.upper() in c.upper()):
                    num=str(i)
                    break
            if len(num)==0:
                d['license_type']='None Found 2'
                return d

            start='body > div.ng-scope > div.middle-content.ng-scope > div > div > div > div.searchoption.mt30.ng-scope > div:nth-child(2) > table > tbody > tr:nth-child('
            end=') > td:nth-child(8) > input'
            driver.find_element_by_css_selector(start+num+end).click()
            wait.until(EC.element_to_be_clickable((By.CSS_SELECTOR,'body > div.ng-scope > div.middle-content.ng-scope > div > div > div > div.searchoption.mt30.ng-scope > div:nth-child(2) > table > tbody > tr > td > div > div.text-center.print-no-display > a.btn.btn-danger.btn-info')),message=f"LA 3, {li_number}")

            path_start='body > div.ng-scope > div.middle-content.ng-scope > div > div > div > div.searchoption.mt30.ng-scope > div:nth-child(2) > table > tbody > tr > td > div > div.detail-container > div:nth-child(2) > table > tbody > tr:nth-child('
            time.sleep(short_pause)
            namecheck=driver.find_element_by_css_selector('body > div.ng-scope > div.middle-content.ng-scope > div > div > div > div.searchoption.mt30.ng-scope > div:nth-child(2) > table > tbody > tr > td > div > div.detail-container > div:nth-child(1) > table > tbody > tr > td:nth-child(1)').text
            if (l_name.upper() not in namecheck.upper()):
                d['license_type']='None Found 3'
                return d

            d['first_name']=namecheck.split()[0].strip().upper()
            d['last_name']=namecheck.split()[-1].strip().upper()

            for i in range(1,5):
                try:
                    row_num=str(i)
                    path_row=path_start+row_num+') > td:nth-child('
                    numcheck=driver.find_element_by_css_selector(path_row+'1)').text.strip().upper()
                    if (li_number.strip().upper() in numcheck) or (numcheck in li_number.strip().upper()):
                        break
                except (NoSuchElementException,ValueError) as e:
                    print(e)
                    d['license_type']='None Found 4'
                    return d

            useful_columns=[1,2,3,5,6]
            col_labs=['license_number','license_type','license_status','effective_date','expire_date']
            for z,j in enumerate(useful_columns):

                path=path_row+str(j)+')'
                d[col_labs[z]]=driver.find_element_by_css_selector(path).text.strip().upper()
            
            save_info(driver,state,li_number)
            return d
        
        def single_scrape_sw(wbd, li_number, f_name, l_name):
            pause_time = 2 + random.random() * 2
            short_pause=random.randint(1,5) / 10 + random.random()
            d = feature_dict(state, li_number, f_name, l_name)
            driver=wbd

            wait = WebDriverWait(driver, 10)

            license_num_box_link = 'licensee'
            first_name_box_link = 'firstname'
            last_name_box_link = 'lastname'
            search_button_link = 'search_button'

            # select the license number search box
            license_num_search_box = driver.find_element_by_id(license_num_box_link)

            # fill search box with license number
            license_num_search_box.clear()
            license_num_search_box.send_keys(li_number)

            # # select the lastname search box
            #             last_name_search_box = driver.find_element_by_id(last_name_box_link)

            # # fill search box with last name
            # last_name_search_box.clear()
            #             last_name_search_box.send_keys(l_name)

            # # select the first name search box
            # first_name_search_box = driver.find_element_by_name(first_name_box_link)

            # # fill search box with first name
            # first_name_search_box.clear()
            # first_name_search_box.send_keys(f_name)

            # click the search button
            driver.find_element_by_id(search_button_link).click()

            wait.until(EC.element_to_be_clickable((By.ID,'search-parameters')),message=f"LA sw 1, {li_number}")

            results =  driver.find_element_by_css_selector('#interior > div.search.content > div.container > div:nth-child(1)').text
            if '0 RESULT' in results.upper():
                d['license_type']='None Found 5'
                return d

            wait.until(EC.element_to_be_clickable((By.ID,'accordion-search-groups')),message=f"LA sw 2, {li_number}").click()

            content=driver.find_element_by_id('accordion-search-groups').text.split('\n')

            if len(content)==1:
                time.sleep(.1)
            content=driver.find_element_by_id('accordion-search-groups').text.split('\n')    

            for c in content:
                if c==content[0] and '1 record' in c:
                    fullname=content[content.index(c)+1]
                    d['last_name']=fullname.split()[-1].strip().upper()
                    d['first_name']=fullname.split()[0].strip().upper()

                # Extract License type
                if "LICENSE #" in c.upper():
                    d['license_number'] = c.split(':')[-1].strip().upper()

                if "PROFESSION" in c.upper():
                    d['license_type'] = c.split(':')[-1].strip().upper()

                if "STATUS:" in c.upper():
                    d['license_status'] = c.split(':')[-1].strip().upper()

                if "EFFECTIVE DATE:" in c.upper():
                    d['effective_date'] = c.split(':')[-1].strip().upper()

                if "EXPIRATION DATE:" in c.upper():  
                    d['expire_date'] = c.split(':')[-1].strip().upper()

            if l_name not in fullname:
                d = feature_dict(state, li_number, f_name, l_name)
                d['license_type'] ='Name mismatch'
                return d
            
            save_info(driver,state,li_number)
            return d    
        
        def single_scrape_pt(wbd, li_number, f_name, l_name):
            pause_time = 2 + random.random() * 2
            short_pause=random.randint(1,5) / 10 + random.random()
            d = feature_dict(state, li_number, f_name, l_name)
            driver=wbd

            wait = WebDriverWait(driver, 10)

            license_num_box_link = '#frm-search-pt-by-license > div.search-input > input[type=text]'
            # first_name_box_link = 'firstName'
            # last_name_box_link = 'lastName'
            search_button_link = 'btn-search-pt-by-license'

            # select the license number search box
            license_num_search_box = driver.find_element_by_css_selector(license_num_box_link)

            # fill search box with license number
            license_num_search_box.clear()
            license_num_search_box.send_keys(li_number)

            # # select the lastname search box
            #             last_name_search_box = driver.find_element_by_id(last_name_box_link)

            # # fill search box with last name
            # last_name_search_box.clear()
            #             last_name_search_box.send_keys(l_name)

            # select the first name search box
            # first_name_search_box = driver.find_element_by_name(first_name_box_link)

            # fill search box with first name
            # first_name_search_box.clear()
            # first_name_search_box.send_keys(f_name)

            # click the search button
            driver.find_element_by_id(search_button_link).click()
            wait.until(EC.visibility_of_element_located((By.ID,'search-results')),message=f"LA phys 1, {li_number}")

            try:
                results = driver.find_element_by_css_selector('#search-results > div.row > p').text
                if 'NO RESULTS' in results.upper():
                    d['license_type'] = 'None Found'
                    return d
            except NoSuchElementException as e:
                pass

            content = driver.find_element_by_css_selector('#search-results > div.row').text.split('\n')

            d['license_number'] = li_number

            for c in content:
                if c==content[0]:
                    fullname=c.upper()
                    d['last_name']=fullname.split()[-1].strip()
                    d['first_name']=fullname.split()[0].strip()

                if "TYPE:" in c.upper():
                    d['license_type'] = c.split(':')[-1].strip().upper()

                if "STATUS:" in c.upper():
                    d['license_status'] = c.split(':')[-1].strip().upper()

                if "ISSUE DATE:" in c.upper():
                    d['effective_date'] = c.split(':')[-1].strip().upper()

                if "EXPIRATION DATE:" in c.upper():  
                    d['expire_date'] = c.split(':')[-1].strip().upper()

                if "BOARD ACTION" in c.upper():  
                    d['board_sanction'] = content[content.index(c)+1].strip().upper()

            if l_name not in fullname:
                d = feature_dict(state, li_number, f_name, l_name)
                d['license_type'] ='Name mismatch'
                return d
            
            save_info(driver,state,li_number)
            return d

        sdata = sdata.astype(str)
        length = int(sdata.shape[0])
        if stop == True:
            stop = length

    # Write a file for the output
        # Write a file for the output

        file=Results(state)
        file.w(headers)
        result=[]  
        number_files=int(stop-start)
        for i in range(start, stop):
            print('processing '+state+ ': '+str(i+1)+' of '+str(number_files),'\n')
            
            if '--proxy-server' in driver_params:
                if i%loop_break==int(loop_break-1):
                    driver.quit()
                    driver = get_driver(url,driver_params)
                    driver = try_get_url(driver,driver_params,url)
            
            li_number = str(sdata.iloc[i].loc['num'])
            f_name = sdata.iloc[i].loc['fn']
            l_name = sdata.iloc[i].loc['ln']
            driver = try_get_url(driver,driver_params,url)
            time.sleep(pause_time)
            if li_number[:2]!='AP':
                try:
                    output=single_scrape(driver, li_number, f_name, l_name)
                except Exception as inst:
                    print(inst)
                    result.append((i,inst))
                    driver = try_get_url(driver,driver_params,url)
                    time.sleep(pause_time)
                    continue
                    
                if output['license_status']=='':
                    driver = try_get_url(driver,driver_params,url_soc)
                    time.sleep(pause_time)
                    try:
                        output=single_scrape_sw(driver, li_number, f_name, l_name)
                    except Exception as inst:
                        print(inst)
                        result.append((i,inst))
                        driver = try_get_url(driver,driver_params,url)
                        time.sleep(pause_time)
                        continue
                        
                if output['license_status']=='':
                    driver = try_get_url(driver,driver_params,url_phys)
                    time.sleep(pause_time)
                    try:
                        output=single_scrape_pt(driver, li_number, f_name, l_name)
                    except Exception as inst:
                        print(inst)
                        result.append((i,inst))
                        driver = try_get_url(driver,driver_params,url)
                        time.sleep(pause_time)
                        continue
                        
            else:
                output= feature_dict(state, li_number, f_name, l_name)
                output['license_type']='AP Nurse'
                
            output['num']=li_number
            output['fn']=f_name
            output['ln']=l_name
            output['state']=state
            result.append((i,output))        
            file.w(output,method='add')

        driver.quit()
        return result

    except Exception as inst:
        print('Scraper Exception - Something bad happened in scraper_function not single_scrape')
        print(inst)
        if 'driver' in locals():
            try:
                driver.quit()
            except:
                pass
        return True,inst
    
# state of Utah
# we can add names attribute directly from variable 'names'
def scrape_UT(sdata, driver_params, start=0, stop=True):
    try:
        state='UT'
        url = "https://secure.utah.gov/llv/search/index.html#"
        driver = get_driver(url,driver_params)
        
        def single_scrape(wbd, li_number, f_name, l_name):
            pause_time = 2 + random.random() * 2
            short_pause=random.randint(1,5) / 10 + random.random()
            d = feature_dict(state, li_number, f_name, l_name)
            driver=wbd

            driver.find_element_by_id('searchNumberTab').click()
            time.sleep(short_pause)

            license_num_box_link = 'licenseNumberCore'
            license_num_box_link1 = 'licenseNumberFourDigit'
            #             first_name_box_link = 'firstName'
            #             last_name_box_link = 'lastName'
            search_button_link = '#command > fieldset > p.buttons > input[type=submit]:nth-child(1)'

            li_number_core,li_number_4=li_number.split('-')

            # select the license number search box
            license_num_search_box = driver.find_element_by_name(license_num_box_link)

            # fill search box with license number
            license_num_search_box.clear()
            license_num_search_box.send_keys(li_number_core)

            # select the license number search box
            license_num_search_box1 = driver.find_element_by_name(license_num_box_link1)

            # fill search box with license number
            license_num_search_box1.clear()
            license_num_search_box1.send_keys(li_number_4,'\uE007')

            # # select the lastname search box
            #             last_name_search_box.clear()
            #             last_name_search_box = driver.find_element_by_id(last_name_box_link)

            # # fill search box with last name

            #             last_name_search_box.send_keys(l_name)

            # select the first name search box
            #             first_name_search_box = driver.find_element_by_name(first_name_box_link)

            #             # fill search box with first name
            #             first_name_search_box.clear()
            #             first_name_search_box.send_keys(f_name)

            # click the search button
            # driver.find_element_by_css_selector(search_button_link).click()
            time.sleep(pause_time)

            searches=[li_number,f_name+' '+l_name,f_name[0]+f_name[1:].lower()+' '+l_name[0]+l_name[1:].lower(),f_name,f_name[0]+f_name[1:].lower()]
            found =False
            for name in searches:
            #                 print(name)
                try:
                    linkfound=driver.find_element_by_partial_link_text(name)
                    link = linkfound.get_attribute('href')
                    driver.get(link)
                    time.sleep(pause_time)
                    found=True
                    break
                except (NoSuchElementException, WebDriverException) as e:
            #                     print(e)
                    pass
            if not found:
                d['license_type']='None Found'
                return d

            content=driver.find_element_by_css_selector('#main > table').text.split('\n')
            for c in content:
                if 'Name:' in c:
                    fullname=c.split(':')[-1].strip().upper()
                    d['last_name']=fullname.split()[-1].strip()
                    d['first_name']=fullname.split()[0].strip()

                # Extract License type
                if "License Type:" in c:
                    d['license_type'] = c.split(':')[-1].strip().upper()

                if "License Number:" in c:
                    d['license_number'] = c.split(':')[-1].strip().upper()

                if "License Status:" in c:
                    d['license_status'] = c.split(':')[-1].strip().upper()

                if "Original Issue Date:" in c:
                    d['effective_date'] = c.split(':')[-1].strip().upper()

                # Extract expire date
                if "Expiration Date:" in c:
                    d['expire_date'] = c.split(':')[-1].strip().upper()

                # board actions
                if "Agency and Disciplinary Action" in c:
                    d['board_sanction'] = c.split(':')[-1].strip().upper()
            
            if (f_name not in fullname) and (l_name not in fullname):
                d=feature_dict(state, li_number, f_name, l_name)
                d['license_type']='None found'
                return d
            
            save_info(driver,state,li_number)
            return d
                                    

        sdata = sdata.astype(str)
        length = int(sdata.shape[0])
        if stop == True:
            stop = length

    # Write a file for the output
        # Write a file for the output

        file=Results(state)
        file.w(headers)
        result=[]  
        number_files=int(stop-start)
        for i in range(start, stop):
            print('processing '+state+ ': '+str(i+1)+' of '+str(number_files),'\n')
            
            if '--proxy-server' in driver_params:
                if i%loop_break==int(loop_break-1):
                    driver.quit()
                    driver = get_driver(url,driver_params)
                    driver = try_get_url(driver,driver_params,url)
            
            li_number = str(sdata.iloc[i].loc['num'])
            f_name = sdata.iloc[i].loc['fn']
            l_name = sdata.iloc[i].loc['ln']
            driver = try_get_url(driver,driver_params,url)
            time.sleep(pause_time)
            try:
                output=single_scrape(driver, li_number, f_name, l_name)
            except Exception as inst:
                print(inst)
                result.append((i,inst))
                driver = try_get_url(driver,driver_params,url)
                time.sleep(pause_time)
                continue
                
            output['num']=li_number
            output['fn']=f_name
            output['ln']=l_name
            output['state']=state
            result.append((i,output))        
            file.w(output,method='add')

        driver.quit()
        return result

    except Exception as inst:
        print('Scraper Exception - Something bad happened in scraper_function not single_scrape')
        print(inst)
        if 'driver' in locals():
            try:
                driver.quit()
            except:
                pass
        return True,inst
    
 # state of North Carolina
# we can add names attribute directly from variable 'names'
def scrape_NC(sdata, driver_params, start=0, stop=True):
    try:
        state='NC'
        url = "https://wwwapps.ncmedboard.org/Clients/NCBOM/Public/LicenseeInformationSearch.aspx"
        url_soc = "https://www.ncswboard.org/page/license-lookup.html"
        url_nur = "https://portal.ncbon.com/LicenseVerification/search.aspx" 
        driver = get_driver(url,driver_params)
        
        def single_scrape(wbd, li_number, f_name, l_name):
            pause_time = 2 + random.random() * 2
            short_pause=random.randint(1,5) / 10 + random.random()
            d = feature_dict(state, li_number, f_name, l_name)
            driver=wbd

            wait = WebDriverWait(driver, 10)

            license_num_box_link = 'waLicenseNumber'
            # first_name_box_link = 'firstName'
            last_name_box_link = 'waLastName'
            search_button_link = 'btnNext'

            li_numbers=[li_number,li_number.split('-')[-1],li_number.replace('-','')
                        ,re.sub('0+','',li_number),re.sub('^0+','',li_number.replace('-',''))]

            for num in li_numbers:
                wait.until(EC.element_to_be_clickable((By.ID,search_button_link)))

                # select the license number search box
                try:
                    license_num_search_box = driver.find_element_by_id(license_num_box_link)
                except NoSuchElementException:
                    driver.execute_script("window.scrollTo(0, 500)")
                    time.sleep(pause_time)
                    license_num_search_box = driver.find_element_by_id(license_num_box_link)

                # fill search box with license number
                license_num_search_box.clear()
                license_num_search_box.send_keys(num)


                # click the search button
                driver.find_element_by_id(search_button_link).click()
                wait.until(EC.element_to_be_clickable((By.ID,'btnBack')))
                # time.sleep(.1)

                searches=[l_name+', '+f_name,l_name[0]+l_name[1:].lower()+', '+f_name[0]+f_name[1:].lower(),f_name,f_name[0]+f_name[1:].lower(),l_name[0]+l_name[1:].lower()]
                found =False
                for name in searches:
                #                 print(name)
                    try:
                        linkfound=driver.find_element_by_partial_link_text(name)
                        link = linkfound.get_attribute('href')
                        driver.get(link)
                        time.sleep(pause_time)
                        found=True
                        break
                    except (NoSuchElementException, WebDriverException) as e:
                        time.sleep(.1)

                if found==True:
                    break
                else:
                    driver.find_element_by_id('btnBack').click()

            #                     print(e)
                    pass
            if not found:
                d['license_type']='None Found'
                return d

            worked=False
            try:
                content=driver.find_element_by_id('div_licensee_info').text.split('\n')
                for c in content:
                    if c==content[0]:
                        fullname=c.split('-')[0].split(',')[0].strip()
                        d['last_name']=fullname.split()[-1].strip().upper()
                        d['first_name']=fullname.split()[0].strip().upper()

                    # Extract License type
                    if "LICENSE#:" in c:
                        items=c.split()
                        d['license_number'] = items[items.index('LICENSE#:')+1].strip().upper()

                    if "STATUS:" in c:
                        items=c.split()
                        d['license_status'] = items[items.index('STATUS:')+1].strip().upper()

                    if "DATE:" in c:
                        items=c.split()
                        try:
                            d['effective_date'] = items[items.index('ISSUE')+2].strip().upper()
                            d['expire_date'] = items[items.index('RENEWAL')+2].strip().upper()
                        except ValueError:
                            pass
                try:
                    d['board_sanction'] = driver.find_element_by_id('divActionsInsert2').text.strip().upper()
                except NoSuchElementException:
                    pass
                if len(d['board_sanction'])<1:
                    try:
                        driver.find_element_by_id('__tab_TabContainer1_TabActions').click()
                        d['board_sanction']=driver.find_element_by_id('TabContainer1_TabActions_tblNCBOMBoardAction').text.strip().upper()
                    except NoSuchElementException:
                        pass
                worked=True
            except NoSuchElementException as e:
                pass

            if worked==False:
                content=driver.find_element_by_id('ctl00_ContentPlaceHolder1_Div4').text.split('\n')
                for c in content:
                    if c==content[0]:
                        fullname=c.split('-')[0].split(',')[0].strip()
                        d['last_name']=fullname.split()[-1].strip().upper()
                        d['first_name']=fullname.split()[0].strip().upper()

                    # Extract License type
                    if "ISSUE DATE:" in c.upper():
                        d['license_number'] = c.split(':')[-1].strip().upper()
                        d['effective_date'] = content[content.index(c)+1].strip().upper()

                    if "RENEWAL DATE:" in c.upper():
                        d['license_status'] = c.split(':')[-1].strip().upper()
                        d['expire_date'] = content[content.index(c)+1].strip().upper()

                    if "EXPIRE DATE:" in c.upper():
                        d['license_status'] = c.split(':')[-1].strip().upper()
                        d['expire_date'] = content[content.index(c)+1].strip().upper()

                    if "PUBLIC ACTION:" in c.upper():
                        d['board_sanction'] = c.split(':')[-1].strip().upper()

            save_info(driver,state,li_number)
            return d
        
        def single_scrape_sw(wbd, li_number, f_name, l_name):
            pause_time = 2 + random.random() * 2
            short_pause=random.randint(1,5) / 10 + random.random()
            d = feature_dict(state, li_number, f_name, l_name)
            driver=wbd
            
            iframe=driver.find_element_by_css_selector('#content > iframe').get_attribute('src')
            driver.get(iframe)
            time.sleep(short_pause)
            license_num_box_link = 'License'
            # first_name_box_link = 'FirstName'
            last_name_box_link = 'LastName'
            search_button_link = '#search-form > div:nth-child(5) > input[type=submit]'

            # select the license number search box
            license_num_search_box = driver.find_element_by_id(license_num_box_link)

            # fill search box with license number
            license_num_search_box.clear()
            license_num_search_box.send_keys(li_number)

            # select the lastname search box
            # last_name_search_box = driver.find_element_by_id(last_name_box_link)

            # fill search box with last name
            # last_name_search_box.clear()
            # last_name_search_box.send_keys(l_name)

            # select the first name search box
            #             first_name_search_box = driver.find_element_by_name(first_name_box_link)

            #             # fill search box with first name
            #             first_name_search_box.clear()
            #             first_name_search_box.send_keys(f_name)

            # click the search button
            driver.find_element_by_css_selector(search_button_link).click()
            time.sleep(pause_time)

            searches=[l_name+', '+f_name,l_name[0]+l_name[1:].lower()+', '+f_name[0]+f_name[1:].lower(),f_name,f_name[0]+f_name[1:].lower()]
            found =False
            for name in searches:
            #                 print(name)
                try:
                    linkfound=driver.find_element_by_partial_link_text(name)
                    link = linkfound.get_attribute('href')
                    driver.get(link)
                    time.sleep(pause_time)
                    found=True
                    break
                except (NoSuchElementException, WebDriverException) as e:
            #                     print(e)
                    pass
            if not found:
                d['license_type']='None Found'
                return d
            
            content=driver.find_element_by_css_selector('body > div').text.split('\n')
            for c in content:
                if c==content[0]:
                    fullname=c.split(',')
                    d['last_name']=fullname[0].strip().upper()
                    d['first_name']=fullname[1].split()[0].strip().upper()

                # Extract License type
                if "Licensed/Certified" in c:
                    d['license_type'] = content[content.index(c)+1].strip().upper()

                if "Number" in c:
                    d['license_number'] = content[content.index(c)+1].strip().upper()

                if "Status" in c:
                    d['license_status'] = content[content.index(c)+1].strip().upper()

                if "Issue Date" in c:
                    d['effective_date'] = content[content.index(c)+1].strip().upper()

                if "Expiration Date" in c:
                    d['expire_date'] = content[content.index(c)+1].strip().upper()

                if "Public Record" in c:
                    d['board_sanction'] = content[content.index(c)+1].strip().upper()


            save_info(driver,state,li_number)
            return d
        
        def single_scrape_nur(wbd, li_number, f_name, l_name):
            pause_time = 2 + random.random() * 2
            short_pause=random.randint(1,5) / 10 + random.random()
            d = feature_dict(state, li_number, f_name, l_name)
            driver=wbd

            wait = WebDriverWait(driver, 10)

            license_num_box_link = 'txtLicNum'
            license_type_box_link = "ddLicType"
            # first_name_box_link = 'FirstName'
            # last_name_box_link = 'LastName'
            search_button_link = 'btnSearch'

            # select the license number search box
            license_num_search_box = driver.find_element_by_id(license_num_box_link)

            # fill search box with license number
            license_num_search_box.clear()
            license_num_search_box.send_keys(li_number)

            l_type=driver.find_element_by_id(license_type_box_link)
            options=l_type.find_elements_by_tag_name('option')
            opts=[x.text for x in options]
            opts_order=[9,8,4,1,2,6,7,3,0,5]
            opts_sorting = sorted(zip(opts, opts_order), key=lambda x: x[1])

            opts_useful=[x[0] for x in opts_sorting][0:4]
            result='something'
            for opt in opts_useful:
                if len(opt)>1:
                    l_type=driver.find_element_by_id(license_type_box_link)
                    l_type.send_keys(opt)
                    driver.find_element_by_id(search_button_link).click()
                    time.sleep(short_pause)
                    result=driver.find_element_by_id('ErrorList').text
                    time_start=time.time()
                    timer=0
                    while ('PROCESS' in result.upper()) & (timer<10):
                        if len(result)==0:
                            break

                        timer=time.time()-time_start
                        result=driver.find_element_by_id('ErrorList').text
                        if len(result)==0:
                            break

                        driver.find_element_by_id(search_button_link).click()
                        time.sleep(pause_time)
                        driver.refresh()

                    if len(result)==0:
                        break

                if len(result)==0:
                        break
            try:
                fullname=wait.until(EC.visibility_of_element_located((By.CSS_SELECTOR,'#Form1 > div.primaryContent > div.pageTitle'))).text
            except WebDriverException:
                time.sleep(pause_time)
                fullname=wait.until(EC.visibility_of_element_located((By.CSS_SELECTOR,'#Form1 > div.primaryContent > div.pageTitle'))).text

            d['first_name']=fullname.split()[0].strip().upper()
            d['last_name']=fullname.split(' ',1)[-1].split(',')[0].strip().upper()
            if (l_name.upper() not in fullname.upper()) & (f_name.upper() not in fullname.upper()):
                d['license_type']='None Found'
                return d

            content = driver.find_element_by_css_selector('#Form1 > div.primaryContent > div:nth-child(4)').text.split('\n')

            for i,c in enumerate(content):

                if 'LICENSE #' in c.upper():
                    d['license_type']=c.split('#')[0].strip().upper()
                    d['license_number']=c.split('#')[-1].strip().upper()

                if 'APPROVAL DATE' in c.upper():
                    d['effective_date']=content[content.index(c)+1].strip().upper()

                if 'LICENSE STATUS' in c.upper():
                    d['license_status']=content[content.index(c)+1].strip().upper()

                if 'EXPIRATION DATE' in c.upper():
                    d['expire_date']=content[content.index(c)+1].strip().upper()

                if 'DISCIPLINE' in c.upper():
                    d['board_sanction']=content[content.index(c)+1].strip().upper()

            if (li_number not in d['license_number']) and (d['license_number'] not in li_number):
                content = driver.find_element_by_css_selector('#Form1 > div.primaryContent > div:nth-child(6)').text.split('\n')

                for i,c in enumerate(content):

                    if '#' in c.upper():
                        d['license_type']=c.split('#')[0].strip().upper()
                        d['license_number']=c.split('#')[-1].strip().upper()

                    if 'APPROVAL DATE' in c.upper():
                        d['effective_date']=content[content.index(c)+1].strip().upper()

                    if 'LICENSE STATUS' in c.upper():
                        d['license_status']=content[content.index(c)+1].strip().upper()

                    if 'EXPIRATION DATE' in c.upper():
                        d['expire_date']=content[content.index(c)+1].strip().upper()

                    if 'DISCIPLINE' in c.upper():
                        d['board_sanction']=content[content.index(c)+1].strip().upper()

            if (li_number not in d['license_number']) and (d['license_number'] not in li_number):
                d = feature_dict(state, li_number, f_name, l_name)
                d['license_type'] - 'None Found'
                return d

            save_info(driver,state,li_number)
            return d
                                    

        sdata = sdata.astype(str)
        length = int(sdata.shape[0])
        if stop == True:
            stop = length

    # Write a file for the output
        # Write a file for the output

        file=Results(state)
        file.w(headers)
        result=[]  
        number_files=int(stop-start)
        for i in range(start, stop):
            print('processing '+state+ ': '+str(i+1)+' of '+str(number_files),'\n')
            
            if '--proxy-server' in driver_params:
                if i%loop_break==int(loop_break-1):
                    driver.quit()
                    driver = get_driver(url,driver_params)
                    driver = try_get_url(driver,driver_params,url)
            
            li_number = str(sdata.iloc[i].loc['num'])
            f_name = sdata.iloc[i].loc['fn']
            l_name = sdata.iloc[i].loc['ln']
            driver = try_get_url(driver,driver_params,url)
            time.sleep(pause_time)
            try:
                output=single_scrape(driver, li_number, f_name, l_name)
            except Exception as inst:
                print(inst)
                result.append((i,inst))
                driver = try_get_url(driver,driver_params,url)
                time.sleep(pause_time)
                continue
                
            if output['license_status']=='':
                driver = try_get_url(driver,driver_params,url_nur)
                time.sleep(pause_time)
                try:
                    output=single_scrape_nur(driver, li_number, f_name, l_name)
                except Exception as inst:
                    print(inst)
                    result.append((i,inst))
                    driver = try_get_url(driver,driver_params,url)
                    time.sleep(pause_time)
                    continue
            
            if output['license_status']=='':
                driver = try_get_url(driver,driver_params,url_soc)
                time.sleep(pause_time)
                try:
                    output=single_scrape_sw(driver, li_number, f_name, l_name)
                except Exception as inst:
                    print(inst)
                    result.append((i,inst))
                    driver = try_get_url(driver,driver_params,url)
                    time.sleep(pause_time)
                    continue

            output['num']=li_number
            output['fn']=f_name
            output['ln']=l_name
            output['state']=state
            result.append((i,output))        
            file.w(output,method='add')

        driver.quit()
        return result

    except Exception as inst:
        print('Scraper Exception - Something bad happened in scraper_function not single_scrape')
        print(inst)
        if 'driver' in locals():
            try:
                driver.quit()
            except:
                pass
        return True,inst
    
    
 # state of Massachussetts
# we can add names attribute directly from variable 'names'
def scrape_MA(sdata, driver_params, start=0, stop=True):
    try:
        state='MA'
        url = "http://profiles.ehs.state.ma.us/Profiles/Pages/FindAPhysician.aspx"
        url_prof = "https://elicensing.mass.gov/CitizenAccess/GeneralProperty/PropertyLookUp.aspx"
        url_oth = "https://checkalicense.hhs.state.ma.us/MyLicenseVerification/"
        driver = get_driver(url,driver_params)
        
        def single_scrape(wbd, li_number, f_name, l_name):
            pause_time = 2 + random.random() * 2
            short_pause=random.randint(1,5) / 10 + random.random()
            d = feature_dict(state, li_number, f_name, l_name)
            driver=wbd

            li_num=re.sub('\D', '',li_number)

            license_num_box_link = 'ctl00_ContentPlaceHolder1_txtLicenseNumber'
            # first_name_box_link = '#ctl00_ContentPlaceHolder1_txtFirstName'
            last_name_box_link = 'ctl00_ContentPlaceHolder1_txtLastName'
            search_button_link = 'ctl00_ContentPlaceHolder1_btnSearch'

            # select the license number search box

            license_num_search_box = driver.find_element_by_id(license_num_box_link)

            # fill search box with license number
            license_num_search_box.clear()
            license_num_search_box.send_keys(li_number)

            # select the lastname search box
            last_name_search_box = driver.find_element_by_id(last_name_box_link)

            # fill search box with last name
            last_name_search_box.clear()
            last_name_search_box.send_keys(l_name)

            # select the first name search box
            #             first_name_search_box = driver.find_element_by_name(first_name_box_link)

            #             # fill search box with first name
            #             first_name_search_box.clear()
            #             first_name_search_box.send_keys(f_name)

            # click the search button
            driver.find_element_by_id(search_button_link).click()
            time.sleep(pause_time)

            searches=[l_name,l_name[0]+l_name[1:].lower()]
            found =False
            for name in searches:
            #                 print(name)
                try:
                    linkfound=driver.find_element_by_partial_link_text(name)
                    link = linkfound.get_attribute('href')
                    driver.get(link)
                    time.sleep(pause_time)
                    found=True
                    break
                except (NoSuchElementException, WebDriverException) as e:
            #                     print(e)
                    pass
            if not found:
                d['license_type']='None Found'
                return d

            try:
                fullname=driver.find_element_by_css_selector('#content > center > p:nth-child(2)').text.split(',')[0]
                d['last_name']=fullname.split()[-1].strip().upper()
                d['first_name']=fullname.split()[0].strip().upper()
            except NoSuchElementException:
                pass

            content=driver.find_element_by_css_selector('#content > center > table:nth-child(4)').text.split('\n')
            for c in content:
                c=c.upper()

                # Extract License type
                if "LICENSE NUMBER" in c:
                    d['license_number'] = c.split()[-1].strip()

                if "LICENSE STATUS" in c:
                    item=c.split('(')[0]
                    d['license_status'] = ''.join(item.split()[2:]).strip()

                if "ISSUE DATE" in c:
                    d['effective_date'] = c.split()[-1].strip()

                if "EXPIRATION DATE" in c:
                    d['expire_date'] = c.split()[-1].strip()

            try:
                specialty_content=driver.find_element_by_css_selector('#content > center > table:nth-child(7)').text.split('\n)')
                for c in specialty_content:
                    c=c.upper()
                    if 'AREA OF SPECIALITY' in c:
                        d['license_type']=c.split()[-1].strip()
            except NoSuchElementException:
                pass

            try:
                board_content=driver.find_element_by_css_selector('#content > center > table:nth-child(12)').text.split('\n')
                bc=[]
                for c in board_content:
                    c=c.upper()
                    if 'DATE' in c:
                        bc.append(c.split()[-1].strip())
                    if 'CASE' in c:
                        bc.append('CASE #: '+c.split()[-1].strip())   
                    if 'ACTION ' in c:
                        bc.append(c.split()[-1].strip())
                if bc!=[]:
                    d['board_sanction']=' '.join(bc)
            except NoSuchElementException:
                pass

            save_info(driver,state,li_number)
            return d
        
        def single_scrape_prof(wbd, li_number, f_name, l_name):
            pause_time = 2 + random.random() * 2
            short_pause=random.randint(1,5) / 10 + random.random()
            d = feature_dict(state, li_number, f_name, l_name)
            driver=wbd

            license_num_box_link = 'ctl00_PlaceHolderMain_refLicenseeSearchForm_txtLicenseNumber'
            # first_name_box_link = 'ctl00_PlaceHolderMain_refLicenseeSearchForm_txtFirstName'
            last_name_box_link = 'ctl00_PlaceHolderMain_refLicenseeSearchForm_txtLastName'
            search_button_link = 'ctl00_PlaceHolderMain_btnNewSearch'

            # select the license number search box

            license_num_search_box = driver.find_element_by_id(license_num_box_link)

            # fill search box with license number
            license_num_search_box.clear()
            license_num_search_box.send_keys(li_number)

            # select the lastname search box
            last_name_search_box = driver.find_element_by_id(last_name_box_link)

            # fill search box with last name
            last_name_search_box.clear()
            last_name_search_box.send_keys(l_name)

            # select the first name search box
            #             first_name_search_box = driver.find_element_by_name(first_name_box_link)

            #             # fill search box with first name
            #             first_name_search_box.clear()
            #             first_name_search_box.send_keys(f_name)

            # click the search button
            driver.find_element_by_id(search_button_link).click()
            time.sleep(pause_time)

            try:
                if any(['no' in x for x in driver.find_element_by_id('ctl00_PlaceHolderMain_noResultMessage_messageBar').text.split('\n')]):
                    d['license_type']='None Found'
                    return d
            except NoSuchElementException:
                pass

            content=driver.find_element_by_id('MainContent').text.split('\n')
            for c in content:
                if '  NAME:' in c.upper():
                    fullname=content[content.index(c)+1]
                    d['last_name']=fullname.split()[-1].strip().upper()
                    d['first_name']=fullname.split()[0].strip().upper()

                # Extract License type
                if "LICENSE TYPE:" in c.upper():
                    d['license_type'] = content[content.index(c)+1].strip().upper()

                if "LICENSE NUMBER:" in c.upper():
                    d['license_number'] = content[content.index(c)+1].strip().upper()

                if "STATUS:" in c.upper():
                    d['license_status'] = content[content.index(c)+1].strip().upper()

                if "ISSUE DATE:" in c.upper():
                    d['effective_date'] = content[content.index(c)+1].strip().upper()

                if "EXPIRATION DATE" in c.upper():
                    d['expire_date'] = content[content.index(c)+1].strip().upper()

            try:
                iframe=driver.find_element_by_id('iframeAttachmentList').get_attribute('src')
                driver.get(iframe)
                time.sleep(short_pause)

                d['board_sanction'] = driver.find_element_by_id('form1').text.split('\n')[-1].strip().upper()

            except NoSuchElementException:
                pass

            save_info(driver,state,li_number)
            return d
        
        def single_scrape_oth(wbd, li_number, f_name, l_name):
            pause_time = 2 + random.random() * 2
            short_pause=random.randint(1,5) / 10 + random.random()
            d = feature_dict(state, li_number, f_name, l_name)
            driver=wbd

            license_num_box_link = 't_web_lookup__license_no'
            # first_name_box_link = 't_web_lookup__first_name'
            last_name_box_link = 't_web_lookup__last_name'
            search_button_link = 'sch_button'

            # select the license number search box

            license_num_search_box = driver.find_element_by_id(license_num_box_link)

            # fill search box with license number
            license_num_search_box.clear()
            license_num_search_box.send_keys(li_number)

            # select the lastname search box
            last_name_search_box = driver.find_element_by_id(last_name_box_link)

            # fill search box with last name
            last_name_search_box.clear()
            last_name_search_box.send_keys(l_name)

            # select the first name search box
            #             first_name_search_box = driver.find_element_by_name(first_name_box_link)

            #             # fill search box with first name
            #             first_name_search_box.clear()
            #             first_name_search_box.send_keys(f_name)

            # click the search button
            driver.find_element_by_name(search_button_link).click()
            time.sleep(pause_time)

            searches=[l_name+', '+f_name,l_name[0]+l_name[1:].lower()+', '+f_name[0]+f_name[1:].lower(),f_name,f_name[0]+f_name[1:].lower()]
            found =False
            for name in searches:
            #                 print(name)
                try:
                    linkfound=driver.find_element_by_partial_link_text(name)
                    link = linkfound.get_attribute('href')
                    driver.get(link)
                    time.sleep(pause_time)
                    found=True
                    break
                except (NoSuchElementException, WebDriverException) as e:
            #                     print(e)
                    pass
            if not found:
                d['license_type']='None Found'
                return d


            content = driver.find_element_by_css_selector('#TheForm > table > tbody > tr:nth-child(2) > td.center').text.split('\n')
            for c in content:
                c1=c
                c=c.upper()
                if 'FULL NAME:' in c:
                    fullname=c.split(':')[-1].strip()
                    d['last_name']=fullname.split()[-1].strip().upper()
                    d['first_name']=fullname.split()[0].strip().upper()

                # Extract 
                if "LICENSE NUMBER:" in c:
                    d['license_number'] = c.split(':')[-1].strip()

                if "PROFESSION:" in c:
                    d['license_type'] = c.split(':')[-1].strip()

                if "LICENSE STATUS" in c:
                    data=c.split(':')[1]
                    d['license_status'] = data.split()[0].strip()

                if "ISSUE DATE" in c:
                    d['effective_date'] = c.split()[-1].strip()

                if "EXPIRATION DATE" in c:
                    d['expire_date'] = c.split(':')[-1].strip()

                if "CASE #" in c:
                    d['board_sanction'] = content[content.index(c1)+1].strip().upper()

            save_info(driver,state,li_number)
            return d
                                    

        sdata = sdata.astype(str)
        length = int(sdata.shape[0])
        if stop == True:
            stop = length

    # Write a file for the output
        # Write a file for the output

        file=Results(state)
        file.w(headers)
        result=[]  
        number_files=int(stop-start)
        for i in range(start, stop):
            print('processing '+state+ ': '+str(i+1)+' of '+str(number_files),'\n')
            
            if '--proxy-server' in driver_params:
                if i%loop_break==int(loop_break-1):
                    driver.quit()
                    driver = get_driver(url,driver_params)
                    driver = try_get_url(driver,driver_params,url)
            
            li_number = str(sdata.iloc[i].loc['num'])
            f_name = sdata.iloc[i].loc['fn']
            l_name = sdata.iloc[i].loc['ln']
            driver = try_get_url(driver,driver_params,url)
            time.sleep(pause_time)
            try:
                output=single_scrape(driver, li_number, f_name, l_name)
            except Exception as inst:
                print(inst)
                result.append((i,inst))
                driver = try_get_url(driver,driver_params,url)
                time.sleep(pause_time)
                continue
            
            if output['license_status']=='':
                driver = try_get_url(driver,driver_params,url_prof)
                time.sleep(pause_time)
                try:
                    output=single_scrape_prof(driver, li_number, f_name, l_name)
                except Exception as inst:
                    print(inst)
                    result.append((i,inst))
                    driver = try_get_url(driver,driver_params,url)
                    time.sleep(pause_time)
                    continue
            
            if output['license_status']=='':
                driver = try_get_url(driver,driver_params,url_oth)
                time.sleep(pause_time)
                try:
                    output=single_scrape_oth(driver, li_number, f_name, l_name)
                except Exception as inst:
                    print(inst)
                    result.append((i,inst))
                    driver = try_get_url(driver,driver_params,url)
                    time.sleep(pause_time)
                    continue
            
            output['num']=li_number
            output['fn']=f_name
            output['ln']=l_name
            output['state']=state
            result.append((i,output))        
            file.w(output,method='add')

        driver.quit()
        return result

    except Exception as inst:
        print('Scraper Exception - Something bad happened in scraper_function not single_scrape')
        print(inst)
        if 'driver' in locals():
            try:
                driver.quit()
            except:
                pass
        return True,inst
    
 # state of Iowa
# we can add names attribute directly from variable 'names'
def scrape_IA(sdata, driver_params, start=0, stop=True):
    try:
        state='IA'
        url = "https://eservices.iowa.gov/PublicPortal/Iowa/IBM/licenseQuery/LicenseQuery.jsp?Profession=Physician"
        url_nurse="https://eservices.iowa.gov/PublicPortal/Iowa/IBON/public/license_verification.jsp"
        url_prof = "https://ibplicense.iowa.gov/PublicPortal/Iowa/IBPL/publicsearch/publicsearch.jsp"
        url_dent="https://eservices.iowa.gov/PublicPortal/Iowa/IDB/licenseQuery/LicenseQuery.jsp" # Not made yet
        driver = get_driver(url,driver_params)
        
        def single_scrape(wbd, li_number, f_name, l_name):
            pause_time = 2 + random.random() * 2
            short_pause=random.randint(1,5) / 10 + random.random()
            d = feature_dict(state, li_number, f_name, l_name)
            driver=wbd

            license_num_box_link = 'licenseNo'
            # first_name_box_link = 'NameFirst'
            last_name_box_link = 'NameLast'
            search_button_link = '[value=Search]'

            # select the license number search box

            license_num_search_box = driver.find_element_by_id(license_num_box_link)

            # fill search box with license number
            license_num_search_box.clear()
            license_num_search_box.send_keys(li_number)

            # select the lastname search box
            # last_name_search_box = driver.find_element_by_id(last_name_box_link)

            # fill search box with last name
            # last_name_search_box.clear()
            # last_name_search_box.send_keys(l_name)

            # select the first name search box
            #             first_name_search_box = driver.find_element_by_name(first_name_box_link)

            #             # fill search box with first name
            #             first_name_search_box.clear()
            #             first_name_search_box.send_keys(f_name)

            # click the search button
            driver.find_element_by_css_selector(search_button_link).click()
            time.sleep(pause_time)
            try:
                nores=driver.find_element_by_id('errorMessageContainer').text
                if 'No' in nores:
                    d['license_type']='None Found'
                    return d
            except NoSuchElementException:
                pass
            try:
                driver.find_element_by_id('data1').click()
                time.sleep(.1)
                driver.find_element_by_id('Result').click()
                time.sleep(pause_time)
            except NoSuchElementException:
                d['license_type']='None Found'
                return d

            content=driver.find_element_by_id('contentbox').text.split('\n')
            for c in content:
                c=c.upper()

                if "FIRST NAME" in c:
                    d['first_name'] = c.split('NAME')[-1].strip()

                if "LAST NAME" in c:
                    d['last_name'] = c.split('NAME')[-1].strip()      

                # Extract License type
                if "LICENSE NUMBER" in c:
                    d['license_number'] = c.split('NUMBER')[-1].strip()

                if "STATUS" in c:
                    if "STATUS AT TIME" in c:
                        continue
                    else:
                        d['license_status'] = c.split('STATUS')[-1].strip()

                if "LICENSE TYPE" in c:
                    d['license_type'] = c.split('TYPE')[-1].strip()

                if "RENEWAL DATE" in c:
                    d['effective_date'] = c.split('DATE')[-1].strip()

                if "EXPIRATION DATE" in c:
                    d['expire_date'] = c.split('DATE')[-1].strip()

            try:
                public_content=driver.find_element_by_css_selector('#contentbox > tbody > tr > td > table > tbody > tr:nth-child(6)').text.split('\n)')
                board=''.join(public_content).strip()
                if len(board)>0:
                    d['board_sanction']=board
            except NoSuchElementException as e:
                pass

            fullname=d['first_name']+' '+d['last_name']
            if (l_name.upper() not in fullname) & (f_name.upper() not in fullname):
                d = feature_dict(state, li_number, f_name, l_name)
                d['license_type']='None Found'
                return d
            save_info(driver,state,li_number)
            return d
        
        def single_scrape_prof(wbd, li_number, f_name, l_name):
            pause_time = 2 + random.random() * 2
            short_pause=random.randint(1,5) / 10 + random.random()
            d = feature_dict(state, li_number, f_name, l_name)
            driver=wbd

            wait = WebDriverWait(driver, 10)

            license_num_box_link = 'licenseNumber'
            # first_name_box_link = 'firstName'
            last_name_box_link = 'lastName'
            search_button_link = 'cb_Search'

            # select the license number search box

            license_num_search_box = driver.find_element_by_id(license_num_box_link)

            # fill search box with license number
            license_num_search_box.clear()
            license_num_search_box.send_keys(li_number)

            # select the lastname search box
            last_name_search_box = driver.find_element_by_id(last_name_box_link)

            # fill search box with last name
            last_name_search_box.clear()
            last_name_search_box.send_keys(l_name)

            # select the first name search box
            #             first_name_search_box = driver.find_element_by_name(first_name_box_link)

            #             # fill search box with first name
            #             first_name_search_box.clear()
            #             first_name_search_box.send_keys(f_name)

            # click the search button
            driver.find_element_by_id(search_button_link).click()

            noelm=wait.until(EC.presence_of_element_located((By.ID,'searchResultsTable')),message='noelm')

            if 'NO RECORD' in noelm.text.split('\n')[-1].upper():
                d['license_type']='None Found'
                return d
            try:
                wait.until(EC.element_to_be_clickable((By.PARTIAL_LINK_TEXT,li_number)),message='num').click()
            except TimeoutException:
                d['license_type']='None Found'
                return d
            lic_num=''
            lic_type=''
            lic_stat=''
            eff_date=''
            exp_date=''
            disp_act=''
            try:
                lic_num=driver.find_element_by_css_selector('#licenseDetailTable > tbody > tr > td:nth-child(2)').text.strip().upper()
            except NoSuchElementException:
                pass
            try:
                lic_type=driver.find_element_by_css_selector('#licenseDetailTable > tbody > tr > td:nth-child(4)').text.strip().upper()
            except NoSuchElementException:
                pass
            try:
                lic_stat=driver.find_element_by_css_selector('#licenseDetailTable > tbody > tr > td:nth-child(6)').text.strip().upper()
            except NoSuchElementException:
                pass
            try:
                eff_date=driver.find_element_by_css_selector('#licenseDetailTable > tbody > tr > td:nth-child(7)').text.strip().upper()
            except NoSuchElementException:
                pass
            try:
                exp_date=driver.find_element_by_css_selector('#licenseDetailTable > tbody > tr > td:nth-child(8)').text.strip().upper()
            except NoSuchElementException:
                pass
            try:
                disp_act=driver.find_element_by_css_selector('#infoTable > tbody > tr.FormtableData > td:nth-child(2)').text.strip().upper()
            except NoSuchElementException:
                pass

            d['license_number']=lic_num
            d['license_type']=lic_type
            d['license_status']=lic_stat
            d['effective_date']=eff_date
            d['expire_date']=exp_date
            d['board_sanction']=disp_act
            save_info(driver,state,li_number)
            return d    
        def single_scrape_nurse(wbd, li_number, f_name, l_name):
            pause_time = 2 + random.random() * 2
            short_pause=random.randint(1,5) / 10 + random.random()
            d = feature_dict(state, li_number, f_name, l_name)
            driver=wbd

            wait = WebDriverWait(driver, 10)

            li_number = re.sub('[^a-zA-Z0-9]', '', li_number)

            license_num_box_link = 'licenseNumber'
            # first_name_box_link = 'firstName'
            # last_name_box_link = 'lastName'
            search_button_link = '#licenseQuery > tbody > tr:nth-child(5) > td:nth-child(2) > input'

            # select the license number search box

            license_num_search_box = driver.find_element_by_id(license_num_box_link)

            # fill search box with license number
            license_num_search_box.clear()
            license_num_search_box.send_keys(li_number)

            # select the lastname search box
            # last_name_search_box = driver.find_element_by_id(last_name_box_link)

            # fill search box with last name
            # last_name_search_box.clear()
            # last_name_search_box.send_keys(l_name)

            # select the first name search box
            #             first_name_search_box = driver.find_element_by_name(first_name_box_link)

            #             # fill search box with first name
            #             first_name_search_box.clear()
            #             first_name_search_box.send_keys(f_name)

            # click the search button
            driver.find_element_by_css_selector(search_button_link).click()

            content=wait.until(EC.presence_of_element_located((By.ID,'displaybody')),message='noelm').text.split('\n')
            start_num=0
            lic_nums=[]
            for i,c in enumerate(content):
                if "LICENSE NO:" in c.upper():
                    if (c.split(':')[1].split()[0].strip().upper() in li_number) | (li_number in c.split(':')[1].split()[0].strip().upper()):
                        start_num=i
                    lic_nums.append(i)

            end_num=-1
            if (len(lic_nums)>0) and (max(lic_nums)>start_num):
                end_num=[x for x in lic_nums if x>start_num][0]-1

            content1=content[start_num:end_num]

            if "NO LICENSE(S)" in content[-1].upper():
                d['license_type']='None Found'
                return d
            for c in content:
                if "NAME:" in c.upper():
                    fullname=c.upper().split(':')[-1]
                    if (l_name.upper() not in fullname) & (f_name.upper() not in fullname):
                        d['license_type']='None Found'
                        return d
                    d['first_name'] = fullname.split()[0].strip()
                    d['last_name'] = fullname.split()[-1].strip()      

            for c in content1:

                # Extract License type
                if "LICENSE NO:" in c.upper():
                    d['license_number'] = c.split(':')[1].split()[0].strip().upper()

                if "STATUS:" in c.upper():
                    if "COMPACT" in c.upper():
                        pass
                    else:
                        d['license_status'] = c.split(':')[1].split()[0].strip().upper()

                if "LICENSE TYPE:" in c.upper():
                    d['license_type'] = c.split(':')[-1].strip().upper()

                if "ISSUE DATE:" in c.upper():
                    d['effective_date'] = c.split(':')[-1].strip().upper()

                if 'ISSUE DATE FOR CURRENT LICENSE:' in c.upper():
                    d['effective_date'] = c.split(':')[-1].strip().upper()

                if "INACTIVE DATE:" in c.upper():
                    d['expire_date'] = c.split(':')[-1].strip().upper()

                if "EXPIRATION DATE:" in c.upper():
                    d['expire_date'] = c.split(':')[-1].strip().upper()

                if "DISCIPLINE INFORMATION" in c.upper():
                    if "NO DISCIPLINE INFORMATION" in c.upper():
                        continue
                    else:
                        d['board_sanction']=content[content.index(c)+1].strip().upper()

            save_info(driver,state,li_number)
            return d

        sdata = sdata.astype(str)
        length = int(sdata.shape[0])
        if stop == True:
            stop = length

    # Write a file for the output
        # Write a file for the output

        file=Results(state)
        file.w(headers)
        result=[]  
        number_files=int(stop-start)
        for i in range(start, stop):
            print('processing '+state+ ': '+str(i+1)+' of '+str(number_files),'\n')
            
            if '--proxy-server' in driver_params:
                if i%loop_break==int(loop_break-1):
                    driver.quit()
                    driver = get_driver(url,driver_params)
                    driver = try_get_url(driver,driver_params,url)
            
            li_number = str(sdata.iloc[i].loc['num'])
            f_name = sdata.iloc[i].loc['fn']
            l_name = sdata.iloc[i].loc['ln']
            driver = try_get_url(driver,driver_params,url)
            time.sleep(pause_time)
            try:
                output=single_scrape(driver, li_number, f_name, l_name)
            except Exception as inst:
                print(inst)
                result.append((i,inst))
                driver = try_get_url(driver,driver_params,url)
                time.sleep(pause_time)
                continue
                
            if output['license_status']=='':
                driver = try_get_url(driver,driver_params,url_nurse)
                time.sleep(pause_time)
                try:
                    output=single_scrape_nurse(driver, li_number, f_name, l_name)
                except Exception as inst:
                    print(inst)
                    result.append((i,inst))
                    driver = try_get_url(driver,driver_params,url)
                    time.sleep(pause_time)
                    continue
                    
            if output['license_status']=='':
                driver = try_get_url(driver,driver_params,url_prof)
                time.sleep(pause_time)
                try:
                    output=single_scrape_prof(driver, li_number, f_name, l_name)
                except Exception as inst:
                    print(i,inst)
                    result.append((i,inst))
                    driver = try_get_url(driver,driver_params,url)
                    time.sleep(pause_time)
                    continue
            
            output['num']=li_number
            output['fn']=f_name
            output['ln']=l_name
            output['state']=state
            result.append((i,output))        
            file.w(output,method='add')

        driver.quit()
        return result

    except Exception as inst:
        print('Scraper Exception - Something bad happened in scraper_function not single_scrape')
        print(inst)
        if 'driver' in locals():
            try:
                driver.quit()
            except:
                pass
        return True,inst
    
#################################################################################
#################################################################################
#################################################################################
### State of Oregon

def scrape_OR(sdata, driver_params, start=0, stop=True):
    try:
        state='OR'
        url = "https://techmedweb.omb.state.or.us/Clients/ORMB/Public/VerificationRequest.aspx"
        url_nur = "https://osbn.oregon.gov/OSBNVerification/Default.aspx" 
        url_psy = "https://obpe.alcsoftware.com/liclookup.php"
        url_chiro = "https://obce.alcsoftware.com/liclookup.php"
        driver = get_driver(url,driver_params)
        
        def single_scrape(wbd, li_number, f_name, l_name):
            pause_time = 2 + random.random() * 2
            short_pause=random.randint(1,5) / 10 + random.random()
            d = feature_dict(state, li_number, f_name, l_name)
            driver=wbd

            wait = WebDriverWait(driver, 10)

            # click the button

            driver.find_element_by_id('optionsRadios2').click()


            license_num_box_link = 'tbLicense'
            search_button_link = 'btnLicense'

            # select the license number search box
            license_num_search_box = driver.find_element_by_id(license_num_box_link)

            # fill search box with license number
            license_num_search_box.clear()
            license_num_search_box.send_keys(li_number)

            # click the search button
            driver.find_element_by_id(search_button_link).click()
            time.sleep(pause_time)
            # wait.until(EC.element_to_be_clickable((By.CSS_SELECTOR,'#Form1 > div.wrapper1 > div.navbar > div > ul > li:nth-child(2) > a')))

            try:
                result = driver.find_element_by_id('divErrLicSearch').text
                if 'NO ' in result.upper():
                    d['license_type']='None Found'
                    return d

            except NoSuchElementException:
                pass

            fullname=driver.find_element_by_id('ctl00_ContentPlaceHolder1_lblLicensee').text.upper()

            if (l_name.upper() not in fullname) and (f_name.upper() not in fullname):
                d['license_type'] ='Name Mismatch'
                return d

            d['first_name'] = fullname.split(',')[1].strip()
            d['last_name'] = fullname.split(',')[0].strip()

            content = driver.find_element_by_id('ctl00_ContentPlaceHolder1_divLicense').text.split('\n')
            top=''
            for c in content:
                if (li_number in c) and (abs(len(li_number.strip())-len(c.strip()))<3):
                    top='1'
                    break
                elif (li_number in c) and (abs(len(li_number.strip())-len(c.strip()))>=3):
                    top='22'
                    break

            if len(top)==0:
                d['license_type']='None Found'
                return d

            if len(top)==1:  
                 for c in content: 
                    if 'NUMBER:' in c.upper():
                        d['license_number']=content[content.index(c)+1].strip().upper()

                    if "EFFECTIVE: " in c.upper():
                        d['effective_date'] = content[content.index(c)+1].strip().upper()

                    # Extract expire date
                    if "EXPIRES" in c.upper():
                        d['expire_date'] = content[content.index(c)+1].strip().upper()                  

                    # Extract license status
                    if "STATUS:" in c:
                        d['license_status'] = content[content.index(c)+1].strip().upper()

                    if "TYPE:" in c:
                        d['license_type'] = content[content.index(c)+1].strip().upper()

            start_num=0
            lic_nums=[]
            for i,c in enumerate(content):
                if "LICENSE:" in c.upper():
                    if (c.split(':')[1].split()[0].strip().upper() in li_number) | (li_number in c.split(':')[1].split()[0].strip().upper()):
                        start_num=i

                    lic_nums.append(i)

            end_num=-1
            if (len(lic_nums)>0) and (max(lic_nums)>start_num):
                end_num=[x for x in lic_nums if x>start_num][0]

            content1=content[start_num:end_num]

            if len(top)==2:  
                 for c in content1:
                    if 'LICENSE:' in c.upper():
                        d['license_type']=c.split('License:')[0].strip().upper()
                        d['license_number']=c.split(':')[-1].strip().upper()

                    if "EFFECTIVE: " in c.upper():
                        d['effective_date'] = c.split(':')[-1].strip().upper()

                    # Extract expire date
                    if "EXPIRES" in c.upper():
                        d['expire_date'] = c.split(':')[-1].strip().upper()                    

                    # Extract license status
                    if "STATUS:" in c.upper():
                        d['license_status'] = c.split(':')[-1].strip().upper()

                    if ((li_number in c) and (abs(len(li_number.strip())-len(c.strip()))>=3) and d['license_status']!=''):
                        try:
                            d['license_number']=c.split()[0].strip()
                            d['effective_date']=c.split()[1].strip()
                            d['expire_date']=c.split()[2].strip()
                            d['license_type']=' '.join(c.split()[2:]).strip()
                            if d['expire_date']!='':
                                currentdate = datetime.datetime.strptime(d['License_As_Of_Date'], "%Y/%m/%d")
                                try:
                                    expiredate=datetime.datetime.strptime(d['expire_date'], "%m/%d/%Y")
                                except ValueError:
                                    d = feature_dict(state, li_number, f_name, l_name)
                                    d['license_type']='Scraper Broken'
                                    return d

                                if currentdate>expiredate:
                                    d['license_status']='Expired'
                                else:
                                    d['license_status']='Active'
                            break
                        except IndexError:
                            break

            ## Post Graduate License
            if d['license_status']=='':
                content = driver.find_element_by_id('ctl00_ContentPlaceHolder1_dtgOtherLic').text.split('\n')

                for c in content:
                    if (li_number in c):
                        d['license_number']=c.split()[0].strip()
                        d['effective_date']=c.split()[1].strip()
                        d['expire_date']=c.split()[2].strip()
                        d['license_type']=' '.join(c.split()[3:]).strip()

                if d['expire_date']!='': 
                        currentdate = datetime.datetime.strptime(d['License_As_Of_Date'], "%Y/%m/%d")
                        expiredate=datetime.datetime.strptime(d['expire_date'], "%m/%d/%Y")
                        if currentdate>expiredate:
                            d['license_status']='Expired'
                        else:
                            d['license_status']='Active'

            try:
                d['board_sanction']=driver.find_element_by_id('ctl00_ContentPlaceHolder1_lblOrderStand').text   
            except NoSuchElementException:
                pass

            save_info(driver,state,li_number)
            return d

        def single_scrape_nur(wbd, li_number, f_name, l_name):
            pause_time = 2 + random.random() * 2
            short_pause=random.randint(1,5) / 10 + random.random()
            d = feature_dict(state, li_number, f_name, l_name)
            driver=wbd

            wait = WebDriverWait(driver, 10)

            license_num_box_link = 'ctl00_MainContent_txtLicense'
            search_button_link = 'ctl00_MainContent_btnLicense'

            # select the license number search box
            license_num_search_box = driver.find_element_by_id(license_num_box_link)

            # fill search box with license number
            license_num_search_box.clear()
            license_num_search_box.send_keys(li_number)

            # click the search button
            driver.find_element_by_id(search_button_link).click()
            results = wait.until(EC.element_to_be_clickable((By.ID,'ctl00_MainContent_pnlResultsLanguage'))).text
            if 'NO ' in results.upper():
                d['license_type']='None found'
                return d

            try:
                link = driver.find_element_by_partial_link_text(li_number).get_attribute('href')
                driver.get(link)
            except NoSuchElementException:
                time.sleep(short_pause)
                try:
                    link = driver.find_element_by_partial_link_text(li_number).get_attribute('href')
                    driver.get(link)
                except NoSuchElementException:
                    d['license_type']='None found'
                    return d

            wait.until(EC.element_to_be_clickable((By.ID,'btnPrint')))

            fullname=driver.find_element_by_id('lblLicenseeName').text.upper()

            if (l_name.upper() not in fullname) and (f_name.upper() not in fullname):
                d['license_type'] ='Name Mismatch'
                return d

            d['first_name'] = fullname.split(',')[1].strip()
            d['last_name'] = fullname.split(',')[0].strip()


            tables = driver.find_element_by_id('gvLicenses').text.split('\n')
            found = False

            for i,c in enumerate(tables): 
                if li_number.upper() in c.upper(): ## FIX THIS
                    num=i+1

            if found ==False:
                d['license_type']='None found'
                return d

            row_string='#gvLicenses > tbody > tr:nth-child('+str(num)+') > td'
            rows=driver.find_elements_by_css_selector(row_string)
            d['license_number']=rows[0].text.strip()
            d['license_type'] = rows[1].text.strip()
            d['effective_date'] = rows[4].text.strip()
            d['license_status'] = rows[3].text.strip()
            d['expire_date'] = rows[5].text.strip() 


            save_info(driver,state,li_number)
            return d
        
        def single_scrape_chiro(wbd, li_number, f_name, l_name):
            pause_time = 2 + random.random() * 2
            short_pause=random.randint(1,5) / 10 + random.random()
            d = feature_dict(state, li_number, f_name, l_name)
            driver=wbd

            search_type = driver.find_element_by_name('searchby')

            search_type.send_keys('Lic')

            wait = WebDriverWait(driver, 10)

            license_num_box_link = 'searchfor'
            # last_name_box_link = 'lastName'
            search_button_link = 'Submit'

            # select the license number search box
            license_num_search_box = driver.find_element_by_name(license_num_box_link)

            # fill search box with license number
            license_num_search_box.clear()
            license_num_search_box.send_keys(li_number)

            # click the search button
            driver.find_element_by_name(search_button_link).click()
            wait.until(EC.element_to_be_clickable((By.CSS_SELECTOR,'#main > div > div:nth-child(2) > div.col-md-9 > div:nth-child(2) > div:nth-child(3) > div:nth-child(2) > form > input[type=submit]')))
            results = driver.find_element_by_id('top').text

            if 'FOUND 0 RECORDS' in results.upper():
                d['license_type'] = 'None Found'
                return d

            found = False
            lastnames=[l_name.upper()[0]+l_name.lower()[1:],l_name.upper()]
            for name in lastnames:
                try:
                    link = driver.find_element_by_partial_link_text(name).get_attribute('href')
                    driver.get(link)
                    found=True
                except NoSuchElementException:
                    pass

            if found==False:
                d['license_type'] = 'None Found'
                return d

            wait.until(EC.element_to_be_clickable((By.CSS_SELECTOR,'#main > div > div:nth-child(2) > div.col-md-9 > div:nth-child(1) > div > a')))

            content = driver.find_element_by_css_selector('#main > div > div:nth-child(2) > div.col-md-9 > div:nth-child(2) > div > div.table-responsive').text.split('\n')

            for c in content: 
                if 'LICENSE NUMBER' in c.upper():
                    d['license_number']=c.split()[-1].strip().upper()

                if 'FIRST NAME' in c.upper():
                    d['first_name']=c.upper().split('NAME')[-1].strip()

                if 'LAST NAME' in c.upper():
                    d['last_name']=c.upper().split('NAME')[-1].strip()

                if 'ORIGINAL LICENSE DATE' in c.upper():
                    d['effective_date']=c.split()[-1].strip().upper()

                if 'STATUS' in c.upper():
                    d['license_status']=c.upper().split('STATUS')[-1].strip()

                if 'RENEWAL DATE' in c.upper():
                    d['expire_date']=c.split()[-1].strip().upper()

                if 'DISCIPLINARY ACTION' in c.upper():
                    d['board_sanction']=c.upper().split('ACTION')[-1].strip()         

                if d['expire_date']!='':
                    month=d['expire_date'].split('/',1)[0].strip()
                    date_year=d['expire_date'].split('/')[-1].strip()
                    day=d['expire_date'].split('/')[-2].strip()
                    date_month= int(month)+1

                    if int(date_month)>12:
                        date_month='01'
                        date_year=str(int(date_year)+1)

                    if len(date_month)==1:
                        date_month='0'+date_month


                    date_updated=str(date_month)+'/'+day+'/'+str(date_year)
                    d['expire_date'] = date_updated

            d['license_type']='CHIROPRACTOR'

            save_info(driver,state,li_number)
            return d
        
        def single_scrape_psy(wbd, li_number, f_name, l_name):
            pause_time = 2 + random.random() * 2
            short_pause=random.randint(1,5) / 10 + random.random()
            d = feature_dict(state, li_number, f_name, l_name)
            driver=wbd

            wait = WebDriverWait(driver, 10)

            license_num_box_link = 'licenseNumber'
            last_name_box_link = 'lastName'
            search_button_link = '#main-table > tbody > tr:nth-child(4) > td.bodyContentGutter > table > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(4) > td > form > table > tbody > tr:nth-child(5) > td > input[type=Submit]:nth-child(1)'

            # select the lastname search box
            last_name_search_box = driver.find_element_by_name(last_name_box_link)

            # fill search box with last name
            last_name_search_box.clear()
            last_name_search_box.send_keys(l_name)

            # select the license number search box
            license_num_search_box = driver.find_element_by_name(license_num_box_link)

            # fill search box with license number
            license_num_search_box.clear()
            license_num_search_box.send_keys(li_number)

            # click the search button
            driver.find_element_by_css_selector(search_button_link).click()
            wait.until(EC.element_to_be_clickable((By.NAME,'NewSearch')))
            results= driver.find_element_by_css_selector('#main-table > tbody > tr:nth-child(4) > td.bodyContentGutter > table > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(4)').text
            if '(0) ' in results:
                d['license_type']='None found'
                return d

            lastname=l_name.upper()[0]+l_name.lower()[1:]
            try:
                link = driver.find_element_by_partial_link_text(lastname).get_attribute('href')
                driver.get(link)
            except NoSuchElementException:
                d['license_type']='None found'
                return d

            wait.until(EC.element_to_be_clickable((By.CSS_SELECTOR,'#main-table > tbody > tr:nth-child(4) > td.bodyContentGutter > table > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(4) > td > a:nth-child(20)')))


            content = driver.find_element_by_css_selector('#main-table > tbody > tr:nth-child(4) > td.bodyContentGutter > table > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(4) > td > table').text.split('\n')

            for c in content: 
                if 'APPLICATION NUMBER' in c.upper():
                    d['license_number']=c.split()[-1].strip().upper()

                if 'FIRST NAME' in c.upper():
                    d['first_name']=c.upper().split('NAME')[-1].strip()

                if 'LAST NAME' in c.upper():
                    d['last_name']=c.upper().split('NAME')[-1].strip()

                if 'DATE LICENSED' in c.upper():
                    d['effective_date']=c.split()[-1].strip().upper()

                if 'STATUS' in c.upper():
                    d['license_status']=c.upper().split('STATUS')[-1].strip()

                if 'EXPIRATION DATE' in c.upper():
                    d['expire_date']=c.split()[-1].strip().upper()

                if 'BOARD ACTION' in c.upper():
                    d['board_sanction']=c.upper().split('ACTION')[-1].strip()         

            d['license_type'] = 'Psychologist'

            save_info(driver,state,li_number)
            return d

        sdata = sdata.astype(str)
        length = int(sdata.shape[0])
        if stop == True:
            stop = length
                
        # Write a file for the output

        file=Results(state)
        file.w(headers)
        result=[]  
        number_files=int(stop-start)
        for i in range(start, stop):
            print('processing '+state+ ': '+str(i+1)+' of '+str(number_files),'\n')
            
            if '--proxy-server' in driver_params:
                if i%loop_break==int(loop_break-1):
                    driver.quit()
                    driver = get_driver(url,driver_params)
                    driver = try_get_url(driver,driver_params,url)
            
            li_number = str(sdata.iloc[i].loc['num'])
            f_name = sdata.iloc[i].loc['fn']
            l_name = sdata.iloc[i].loc['ln']

            driver = try_get_url(driver,driver_params,url)
            time.sleep(pause_time)
            try:
                output=single_scrape(driver, li_number, f_name, l_name)
            except Exception as inst:

                result.append((i,inst))
                driver = try_get_url(driver,driver_params,url)
                time.sleep(pause_time)
                continue

            if output['license_status']=="":
                driver = try_get_url(driver,driver_params,url_nur)
                time.sleep(pause_time)
                try:
                    output=single_scrape_nur(driver, li_number, f_name, l_name)
                except Exception as inst:
                        
                    result.append((i,inst))
                    driver = try_get_url(driver,driver_params,url)
                    time.sleep(pause_time)
                    continue
                        
            if output['license_status']=="":
                driver = try_get_url(driver,driver_params,url_psy)
                time.sleep(pause_time)
                try:
                    output=single_scrape_psy(driver, li_number, f_name, l_name)
                except Exception as inst:

                    result.append((i,inst))
                    driver = try_get_url(driver,driver_params,url)
                    time.sleep(pause_time)
                    continue
                    
            if output['license_status']=="":
                driver = try_get_url(driver,driver_params,url_chiro)
                time.sleep(pause_time)
                try:
                    output=single_scrape_chiro(driver, li_number, f_name, l_name)
                except Exception as inst:

                    result.append((i,inst))
                    driver = try_get_url(driver,driver_params,url)
                    time.sleep(pause_time)
                    continue
                        
            output['num']=li_number
            output['fn']=f_name
            output['ln']=l_name
            output['state']=state
            result.append((i,output))        
            file.w(output,method='add')
        driver.quit()
        return result

    except Exception as inst:
        print('Scraper Exception - Something bad happened in scraper_function not single_scrape')
        print(inst)
        if 'driver' in locals():
            try:
                driver.quit()
            except:
                pass
        return True,inst

    
#################################################################################
#################################################################################
#################################################################################
### State of Arizona

def scrape_AZ(sdata, driver_params, start=0, stop=True):
    try:
        state='AZ'
        url = "https://gls.azmd.gov/glsuiteweb/clients/azbom/public/WebVerificationSearch.aspx"
        url_sw = "https://www.azbbhe.us/node/3"
        url_pt = "https://elicense.az.gov/ARDC_LicenseSearch"# not done yet
        url_chiro = "https://chiroboard.az.gov/find-chiropractor"# not done yet
        url_aud = "https://azdhs.gov/licensing/index.php?#azcarecheck"# not done yet
        driver = get_driver(url,driver_params)
        
        def single_scrape(wbd, li_number, f_name, l_name):
            pause_time = 2 + random.random() * 2
            short_pause=random.randint(1,5) / 10 + random.random()
            d = feature_dict(state, li_number, f_name, l_name)
            driver=wbd

            wait = WebDriverWait(driver, 10)

            license_num_box_link = 'tbFileNumber'
            search_button_link = 'btnLicense'
            found=False
            buttons=['rbLicense1','rbLicense2','rbLicense3']
            lic_types=['Medical Doctor','Osteopathic Physician', 'Physician Assistant']

            for ty,button in enumerate(buttons):
                header=''
                short_pause=random.randint(1,5) / 10 + random.random()
                driver.execute_script("window.scrollTo(0, 1000)")
                driver.find_element_by_id(button).click()

                # select the license number search box
                license_num_search_box = driver.find_element_by_id(license_num_box_link)

                # fill search box with license number
                license_num_search_box.clear()
                license_num_search_box.send_keys(li_number)
                # click the search button
                driver.find_element_by_id(search_button_link).click()
                time.sleep(short_pause)
                header = wait.until(EC.visibility_of_element_located((By.ID,'header'))).text

                if 'RESULTS' in header.upper(): 
                    results=driver.find_element_by_id('dtgList').text.upper()
                    if (l_name.upper() in results):
                        found=True
                        break
                if found==True:
                    break 

            if found==False:
                d['license_type'] ='None Found'
                return d

            d['license_type']=lic_types[ty]

            link=wait.until(EC.element_to_be_clickable((By.PARTIAL_LINK_TEXT,'Show Profile'))).get_attribute('href')
            driver.get(link)

            wait.until(EC.element_to_be_clickable((By.ID,'lnkBtnPrint')))

            content = driver.find_element_by_id('dtgGeneral').text.split('\n')
            fullname=content[0].upper()

            if (l_name.upper() not in fullname) and (f_name.upper() not in fullname):
                d['license_type'] ='Name Mismatch'
                return d

            d['first_name'] = f_name
            d['last_name'] = l_name

            for c in content: 

                if 'NUMBER:' in c.upper():
                    d['license_number']=c.split(':')[-1].strip()

                if "LICENSED DATE:" in c.upper():
                    d['effective_date'] = c.split(':')[-1].strip()

                if "RENEWED:" in c.upper():
                    if c.split(':')[-1]!='':
                        d['effective_date'] = c.split(':')[-1].strip()

                # Extract expire date
                if "EXPIRES:" in c.upper():

                    d['expire_date'] = c.split(':')[-1].strip()                 

                # Extract license status
                if "STATUS:" in c.upper():
                    d['license_status'] = c.split(':')[-1].strip().upper()

            try:
                d['board_sanction']=driver.find_element_by_css_selector('#pnlBoard > div > table').text.split('\n')[-1] 
            except NoSuchElementException:
                pass

            save_info(driver,state,li_number)
            return d

        def single_scrape_sw(wbd, li_number, f_name, l_name):
            pause_time = 2 + random.random() * 2
            short_pause=random.randint(1,5) / 10 + random.random()
            d = feature_dict(state, li_number, f_name, l_name)
            driver=wbd

            search_type=driver.find_element_by_name('TypeSearch')
            search_type.send_keys('Lic')

            driver.find_element_by_name('ckActive').click()

            wait = WebDriverWait(driver, 10)

            license_num_box_link = 'DaInBox'
            search_button_link = 'B1'

            options=driver.find_element_by_name('LicSearch').text.split('\n')

            # select the license number search box
            license_num_search_box = wait.until(EC.element_to_be_clickable((By.NAME, license_num_box_link)))

            # fill search box with license number
            license_num_search_box.clear()
            license_num_search_box.send_keys(li_number)
            # click the search button
            driver.find_element_by_name(search_button_link).click()

            time.sleep(short_pause)
            new_search=wait.until(EC.element_to_be_clickable((By.CSS_SELECTOR,'body > p:nth-child(4) > font > a')))

            header = driver.find_element_by_css_selector('body').text

            if 'LAST UPDATED' not in header.upper():
                d['license_type'] ='None Found'
                return d

            searchnames=[l_name+', '+f_name,l_name[0]+l_name[1:].lower()+', '+f_name[0]+f_name[1:].lower(),l_name+', ',l_name[0]+l_name[1:].lower()+', ',', '+f_name,', '+f_name[0]+f_name[1:].lower()]
            found=False
            for name in searchnames:
            #     print(name)
                try:
                    namelink=driver.find_element_by_partial_link_text(name)
                    newlink=namelink.get_attribute('href')
                    driver.get(newlink)
                    found=True
                    break
                except NoSuchElementException:
                    pass

            if found==False:
                d['license_type'] ='None Found'
                return d

            wait.until(EC.element_to_be_clickable((By.CSS_SELECTOR,'body > p:nth-child(11) > a')))

            content = driver.find_element_by_css_selector('body').text.split('\n')

            for c in content: 

                if 'NAME:' in c.upper():
                    fullname =c.split(':')[-1].strip().upper()
                    d['first_name']=fullname.split()[0].strip()
                    d['last_name']=fullname.split(' ',1)[-1].strip()

                if li_number.upper() in c.upper():
                    check=c
                    d['license_number']=li_number
                    d['expire_date'] = c.split()[-1]
                    d['effective_date'] = c.split()[-2]
                    d['license_type'] = c.split(li_number)[0].strip().upper()
                    d['license_status'] = check.split(li_number)[-1].split('/')[0][:-2].strip().upper()

                if len(c)==0:
                    d['board_sanction']= content[content.index(c)+1].strip().upper()

            save_info(driver,state,li_number)
            return d
        
        def single_scrape_psy(wbd, li_number, f_name, l_name):
            pause_time = 2 + random.random() * 2
            short_pause=random.randint(1,5) / 10 + random.random()
            d = feature_dict(state, li_number, f_name, l_name)
            driver=wbd

            wait = WebDriverWait(driver, 10)

            license_num_box_link = 'licenseNumber'
            last_name_box_link = 'lastName'
            search_button_link = '#main-table > tbody > tr:nth-child(4) > td.bodyContentGutter > table > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(4) > td > form > table > tbody > tr:nth-child(5) > td > input[type=Submit]:nth-child(1)'

            # select the lastname search box
            last_name_search_box = driver.find_element_by_name(last_name_box_link)

            # fill search box with last name
            last_name_search_box.clear()
            last_name_search_box.send_keys(l_name)

            # select the license number search box
            license_num_search_box = driver.find_element_by_name(license_num_box_link)

            # fill search box with license number
            license_num_search_box.clear()
            license_num_search_box.send_keys(li_number)

            # click the search button
            driver.find_element_by_css_selector(search_button_link).click()
            wait.until(EC.element_to_be_clickable((By.NAME,'NewSearch')))
            results= driver.find_element_by_css_selector('#main-table > tbody > tr:nth-child(4) > td.bodyContentGutter > table > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(4)').text
            if '(0) ' in results:
                d['license_type']='None found'
                return d

            lastname=l_name.upper()[0]+l_name.lower()[1:]
            try:
                link = driver.find_element_by_partial_link_text(lastname).get_attribute('href')
                driver.get(link)
            except NoSuchElementException:
                d['license_type']='None found'
                return d

            wait.until(EC.element_to_be_clickable((By.CSS_SELECTOR,'#main-table > tbody > tr:nth-child(4) > td.bodyContentGutter > table > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(4) > td > a:nth-child(20)')))

            content = driver.find_element_by_css_selector('#main-table > tbody > tr:nth-child(4) > td.bodyContentGutter > table > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(4) > td > table').text.split('\n')

            for c in content: 
                if 'APPLICATION NUMBER' in c.upper():
                    d['license_number']=c.split()[-1].strip().upper()

                if 'FIRST NAME' in c.upper():
                    d['first_name']=c.upper().split('NAME')[-1].strip()

                if 'LAST NAME' in c.upper():
                    d['last_name']=c.upper().split('NAME')[-1].strip()

                if 'DATE LICENSED' in c.upper():
                    d['effective_date']=c.split()[-1].strip().upper()

                if 'STATUS' in c.upper():
                    d['license_status']=c.upper().split('STATUS')[-1].strip()

                if 'EXPIRATION DATE' in c.upper():
                    d['expire_date']=c.split()[-1].strip().upper()

                if 'BOARD ACTION' in c.upper():
                    d['board_sanction']=c.upper().split('ACTION')[-1].strip()         

            save_info(driver,state,li_number)
            return d

        sdata = sdata.astype(str)
        length = int(sdata.shape[0])
        if stop == True:
            stop = length
                
        # Write a file for the output

        file=Results(state)
        file.w(headers)
        result=[]  
        number_files=int(stop-start)
        for i in range(start, stop):
            print('processing '+state+ ': '+str(i+1)+' of '+str(number_files),'\n')
            
            if '--proxy-server' in driver_params:
                if i%loop_break==int(loop_break-1):
                    driver.quit()
                    driver = get_driver(url,driver_params)
                    driver = try_get_url(driver,driver_params,url)
            
            li_number = str(sdata.iloc[i].loc['num'])
            f_name = sdata.iloc[i].loc['fn']
            l_name = sdata.iloc[i].loc['ln']

            driver = try_get_url(driver,driver_params,url)
            time.sleep(pause_time)
            try:
                output=single_scrape(driver, li_number, f_name, l_name)
            except Exception as inst:

                result.append((i,inst))
                driver = try_get_url(driver,driver_params,url)
                time.sleep(pause_time)
                continue

                if output['license_status']=="":
                    driver = try_get_url(driver,driver_params,url_sw)
                    time.sleep(pause_time)
                    try:
                        output=single_scrape_sw(driver, li_number, f_name, l_name)
                    except Exception as inst:
                        
                        result.append((i,inst))
                        driver = try_get_url(driver,driver_params,url)
                        time.sleep(pause_time)
                        continue
                        
#                 if output['license_status']=="":
#                     driver = try_get_url(driver,driver_params,url_psy)
#                     time.sleep(pause_time)
#                     try:
#                         output=single_scrape_psy(driver, li_number, f_name, l_name)
#                     except Exception as inst:

#                         result.append((i,inst))
#                         driver = try_get_url(driver,driver_params,url)
#                         time.sleep(pause_time)
#                         continue
                        
            output['num']=li_number
            output['fn']=f_name
            output['ln']=l_name
            output['state']=state
            result.append((i,output))        
            file.w(output,method='add')
        driver.quit()
        return result

    except Exception as inst:
        print('Scraper Exception - Something bad happened in scraper_function not single_scrape')
        print(inst)
        if 'driver' in locals():
            try:
                driver.quit()
            except:
                pass
        return True,inst    

#################################################################################
#################################################################################
#################################################################################
### State of Illinois

def scrape_IL(sdata, driver_params, start=0, stop=True):
    try:
        state='IL'
        url = "https://ilesonline.idfpr.illinois.gov/DFPR/Lookup/LicenseLookup.aspx"
        driver = get_driver(url,driver_params)
        
        def single_scrape(wbd, li_number, f_name, l_name):
            pause_time = 2 + random.random() * 2
            short_pause=random.randint(1,5) / 10 + random.random()
            d = feature_dict(state, li_number, f_name, l_name)
            driver=wbd

            wait = WebDriverWait(driver, 10)

            license_num_box_link = 'ctl00_MainContentPlaceHolder_ucLicenseLookup_ctl03_tbCredentialNumber_Credential'
            # first_name_box_link = 'firstName'
            # last_name_box_link = 'lastName'
            search_button_link = 'btnLookup'

            # select the license number search box

            license_num_search_box = driver.find_element_by_id(license_num_box_link)

            # fill search box with license number
            license_num_search_box.clear()
            license_num_search_box.send_keys(li_number)

            driver.find_element_by_id(search_button_link).click()

            results = wait.until(EC.presence_of_element_located((By.ID,'ctl00_MainContentPlaceHolder_ucLicenseLookup_gvSearchResults'))).text.split('\n') 

            num=0
            found=False
            for t in results:
                if len(t)>3:
                    num+=1
                    if ((l_name.upper() in t.upper()) or (f_name.upper() in t.upper())) and (li_number[0:2] in t) and (li_number[-2:] in t):
                        found=True
                        break
                if found==True:
                    break

            if found==False:
                d['license_type']='None Found'
                return d
            
            if num<10:
                num='0'+str(num)
            else:
                num=str(num)
            result_id='ctl00_MainContentPlaceHolder_ucLicenseLookup_gvSearchResults_ctl'+num+'_HyperLinkDetail'
            driver.find_element_by_id(result_id).click()

            wait.until(EC.element_to_be_clickable((By.ID,'btnPrint')))

            fullname=driver.find_element_by_css_selector('#divLicDetailInner > div > div.Content > div:nth-child(3)').text.split('\n')[1]

            if (l_name.upper() not in fullname) & (f_name.upper() not in fullname):
                d['license_type']='None Found'
            #     return d
            else:
                d['first_name']=f_name
                d['last_name']=l_name

            d['license_number']=li_number
            content=driver.find_elements_by_css_selector('#Grid1 > tbody > tr > td')
            c=[]
            for item in content:
                c.append(item.text)
                
            d['board_sanction']=c[-1].strip()
            d['expire_date']=c[5].strip().upper()
            d['effective_date']=c[4].strip().upper()
            d['license_status']=c[2].strip().upper()
            d['license_type']=c[1].strip().upper().replace('\n',' ')

            save_info(driver,state,li_number)
            return d

        sdata = sdata.astype(str)
        length = int(sdata.shape[0])
        if stop == True:
            stop = length
                
        # Write a file for the output

        file=Results(state)
        file.w(headers)
        result=[]  
        number_files=int(stop-start)
        for i in range(start, stop):
            print('processing '+state+ ': '+str(i+1)+' of '+str(number_files),'\n')
            
            if '--proxy-server' in driver_params:
                if i%loop_break==int(loop_break-1):
                    driver.quit()
                    driver = get_driver(url,driver_params)
                    driver = try_get_url(driver,driver_params,url)
            
            li_number = str(sdata.iloc[i].loc['num'])
            f_name = sdata.iloc[i].loc['fn']
            l_name = sdata.iloc[i].loc['ln']

            driver = try_get_url(driver,driver_params,url)
            time.sleep(pause_time)
            try:
                output=single_scrape(driver, li_number, f_name, l_name)
            except Exception as inst:

                result.append((i,inst))
                driver = try_get_url(driver,driver_params,url)
                time.sleep(pause_time)
                continue
                        
            output['num']=li_number
            output['fn']=f_name
            output['ln']=l_name
            output['state']=state
            result.append((i,output))        
            file.w(output,method='add')
        driver.quit()
        return result

    except Exception as inst:
        print('Scraper Exception - Something bad happened in scraper_function not single_scrape')
        print(inst)
        if 'driver' in locals():
            try:
                driver.quit()
            except:
                pass
        return True,inst  
    
    
#################################################################################
#################################################################################
#################################################################################
### State of Colorado

def scrape_CO(sdata, driver_params, start=0, stop=True):
    try:
        state='CO'
        url = "https://apps.colorado.gov/dora/licensing/lookup/licenselookup.aspx"
        driver = get_driver(url,driver_params)
        
        def single_scrape(wbd, li_number, f_name, l_name):
            pause_time = 2 + random.random() * 2
            short_pause=random.randint(1,5) / 10 + random.random()
            d = feature_dict(state, li_number, f_name, l_name)
            driver=wbd

            wait = WebDriverWait(driver, 10)

            pref=li_number.split('.')[0]
            num=li_number.split('.')[-1].split('-')[0]

            license_num_box_link = 'ctl00_MainContentPlaceHolder_ucLicenseLookup_ctl03_tbLicenseNumber'
            license_num_box_link1 = 'ctl00_MainContentPlaceHolder_ucLicenseLookup_ctl03_ddCredPrefix'
            search_button_link = 'btnLookup'

            # select the license number search box
            license_num_search_box = driver.find_element_by_id(license_num_box_link)

            # select the license number search box
            license_num_search_box1 = driver.find_element_by_id(license_num_box_link1)

            # fill search box with license number
            license_num_search_box.clear()
            license_num_search_box.send_keys(num)

            # fill search box with license number
            license_num_search_box1.send_keys(pref)

            # click the search button
            driver.find_element_by_id(search_button_link).click()
            results = wait.until(EC.element_to_be_clickable((By.ID,'ctl00_MainContentPlaceHolder_ucLicenseLookup_gvSearchResults'))).text
            # if 'NO ' in results:
            #     d['license_type']='None found'
            #     print('1')
            # #     return d
            num=1
            found=False
            for t in results.split('\n'):
                if len(t)>3:
                    num+=1
                    if ((l_name.upper() in t.upper()) or (f_name.upper() in t.upper())) and (li_number[0:2] in t) and (li_number[-2:] in t):
                        found=True
                        break
                if found==True:
                    break

            if found==False:
                d['license_type']='None Found'
                return d

            if num<10:
                num='0'+str(num)
            else:
                num=str(num)
            result_id='ctl00_MainContentPlaceHolder_ucLicenseLookup_gvSearchResults_ctl'+num+'_HyperLinkDetail'
            driver.find_element_by_id(result_id).click()

            wait.until(EC.element_to_be_clickable((By.ID,'btnPrint')))

            fullname=driver.find_element_by_css_selector('#divLicDetailInner > div > div.Content > div:nth-child(3)').text.split('\n')[1].upper()

            if (l_name.upper() not in fullname) & (f_name.upper() not in fullname):
                d['license_type']='None Found'
                return d
            else:
                d['first_name']=f_name
                d['last_name']=l_name

            d['license_number']=li_number
            content=driver.find_elements_by_css_selector('#Grid1 > tbody > tr > td')
            headers=driver.find_elements_by_css_selector('#Grid1 > thead > tr > th')
            c=[]
            for item in content:
                c.append(item.text)

            h=[]
            for item in headers:
                h.append(item.text+':')

            for (j,i) in zip(c,h):
                if 'LICENSE NUMBER' in i.upper():
                    d['license_number']=j.split(':')[-1].strip().upper()

                if 'EFFECTIVE DATE' in i.upper():
                    d['effective_date']=j.split(':')[-1].strip().upper()

                if 'STATUS' in i.upper():
                    d['license_status']=j.split(':')[-1].strip().upper()

                if 'EXPIRATION DATE' in i.upper():
                    d['expire_date']=j.split(':')[-1].strip().upper()

                if 'LICENSE TYPE' in i.upper():
                    d['license_type']=j.split(':')[-1].strip().upper()  

            try:
                d['board_sanction']=driver.find_element_by_id('Grid3').text.split('\n')[1]
            except NoSuchElementException:
                try:
                    d['board_sanction']=driver.find_element_by_id('Grid2').text.split('\n')[1]
                except NoSuchElementException:
                    pass
                pass

            save_info(driver,state,li_number)
            return d

        sdata = sdata.astype(str)
        length = int(sdata.shape[0])
        if stop == True:
            stop = length
                
        # Write a file for the output

        file=Results(state)
        file.w(headers)
        result=[]  
        number_files=int(stop-start)
        for i in range(start, stop):
            print('processing '+state+ ': '+str(i+1)+' of '+str(number_files),'\n')
            
            if '--proxy-server' in driver_params:
                if i%loop_break==int(loop_break-1):
                    driver.quit()
                    driver = get_driver(url,driver_params)
                    driver = try_get_url(driver,driver_params,url)
            
            li_number = str(sdata.iloc[i].loc['num'])
            f_name = sdata.iloc[i].loc['fn']
            l_name = sdata.iloc[i].loc['ln']

            driver = try_get_url(driver,driver_params,url)
            time.sleep(pause_time)
            try:
                output=single_scrape(driver, li_number, f_name, l_name)
            except Exception as inst:

                result.append((i,inst))
                driver = try_get_url(driver,driver_params,url)
                time.sleep(pause_time)
                continue
                        
            output['num']=li_number
            output['fn']=f_name
            output['ln']=l_name
            output['state']=state
            result.append((i,output))        
            file.w(output,method='add')
        driver.quit()
        return result

    except Exception as inst:
        print('Scraper Exception - Something bad happened in scraper_function not single_scrape')
        print(inst)
        if 'driver' in locals():
            try:
                driver.quit()
            except:
                pass
        return True,inst     

#################################################################################
#################################################################################
#################################################################################
# # # # Georgia
def scrape_GA(sdata, driver_params, start=0, stop=True):
    try:
        state='GA'
        url = "https://gcmb.mylicense.com/verification/Search.aspx"
        url_prof="http://verify.sos.ga.gov/verification/"
        driver = get_driver(url,driver_params)

        def single_scrape(wbd, li_number, f_name, l_name):
            pause_time = 2 + random.random() * 2
            short_pause=random.randint(1,5) / 10 + random.random()
            d = feature_dict(state, li_number, f_name, l_name)
            driver=wbd

            wait = WebDriverWait(driver, 10)
            
            li_number=re.sub('0+','',li_number)
            
            license_num_search_box_link = '#t_web_lookup__license_no'
            last_name_search_box_link = '#t_web_lookup__last_name'
            search_button_link = '#sch_button'

            # select the license number search box
            license_num_search_box = driver.find_element_by_css_selector(license_num_search_box_link )

            # fill search box with license number
            license_num_search_box.clear()
            license_num_search_box.send_keys(li_number)

            # select the last name search box
            last_name_search_box = driver.find_element_by_css_selector(last_name_search_box_link)

            # fill search box with last name
            last_name_search_box.clear()
            last_name_search_box.send_keys(l_name)

            # # select the first name search box
            # first_name_search_box = driver.find_element_by_css_selector('#t_web_lookup__first_name')

            # # fill search box with first name
            # first_name_search_box.clear()
            # first_name_search_box.send_keys(f_name)

            # click the search button
            driver.find_element_by_css_selector(search_button_link).click()
            wait.until(EC.visibility_of_element_located((By.ID,'datagrid_results')))
            searchnames=[
                l_name[0]+l_name[1:].lower()+' ',
                l_name[0]+l_name[1:].lower()+', ',
                l_name+' '+f_name,
                l_name+', '+f_name,
                l_name[0]+l_name[1:].lower()+' '+f_name[0]+f_name[1:].lower(),
                l_name[0]+l_name[1:].lower()+', '+f_name[0]+f_name[1:].lower()]
            found=False
            for name in searchnames[::-1]:
            #     print(name)
                try:
                    link=driver.find_element_by_partial_link_text(name).get_attribute('href')
                    driver.get(link)
                    found=True
                    break
                except NoSuchElementException:
                    pass

            if found==False:
                d['license_type'] ='None Found'
                return d


            content = wait.until(EC.visibility_of_element_located(
                (By.CSS_SELECTOR,
                '#TheForm > table > tbody > tr:nth-child(2) > td.center > table.center > tbody > tr:nth-child(10) > td')
                )).text.split('\n')
            for c in content:

                if " TYPE:" in c.upper():
                    d['license_type'] = c.upper().split(' TYPE: ')[-1].split()[0]

                # Extract License Type
                if "PROFESSION:" in c.upper():
                    d['license_type'] = c.upper().split('PROFESSION: ')[-1].split()[0]

                # Extract effective date
                if "ISSUED:" in c.upper():
                    d['effective_date'] = c.upper().split('ISSUED:')[-1].strip().split()[0]   

                # Extract expire date
                if "EXPIRES:" in c.upper():
                    d['expire_date'] = c.upper().split('EXPIRES: ')[-1].strip()

                # Extract license status
                if "STATUS:" in c.upper():
                    d['license_status'] = c.upper().split('STATUS: ')[-1].strip().split()[0]

                # extract the "Additional Public Information" for sanction
                if 'DISCIPLINARY ACTION' in c.upper():
                    idx = content.index(c)
                    d['board_sanction'] = content[idx + 2].strip()
                    if 'HOSPITAL' in d['board_sanction'].upper():
                        d['board_sanction']='None'
            
            save_info(driver,state,li_number)
            return d
        
        def single_scrape_prof(wbd, li_number, f_name, l_name):
            pause_time = 2 + random.random() * 2
            short_pause=random.randint(1,5) / 10 + random.random()
            d = feature_dict(state, li_number, f_name, l_name)
            driver=wbd

            wait = WebDriverWait(driver, 10)

            # select the license number search box
            license_num_search_box = driver.find_element_by_css_selector('#t_web_lookup__license_no')

            # fill search box with license number
            license_num_search_box.clear()
            license_num_search_box.send_keys(li_number)

            # select the last name search box
            last_name_search_box = driver.find_element_by_css_selector('#t_web_lookup__last_name')

            # fill search box with last name
            last_name_search_box.clear()
            last_name_search_box.send_keys(l_name)

            # click the search button
            driver.find_element_by_css_selector('#sch_button').click()
            wait.until(EC.visibility_of_element_located((By.ID,'datagrid_results')))
            searchnames=[
                l_name[0]+l_name[1:].lower()+' ',
                l_name[0]+l_name[1:].lower()+', ',
                l_name+' '+f_name,
                l_name+', '+f_name,
                l_name[0]+l_name[1:].lower()+' '+f_name[0]+f_name[1:].lower(),
                l_name[0]+l_name[1:].lower()+', '+f_name[0]+f_name[1:].lower()]
            found=False
            for name in searchnames[::-1]:
            #     print(name)
                try:
                    link=driver.find_element_by_partial_link_text(name).get_attribute('href')
                    driver.get(link)
                    found=True
                    break
                except NoSuchElementException:
                    pass

            if found==False:
                d['license_type'] ='None Found'
                return d


            content = wait.until(EC.visibility_of_element_located(
                (By.CSS_SELECTOR,
                '#TheForm > table > tbody > tr:nth-child(2) > td.center > table.center > tbody')
                )).text.split('\n')
            for c in content:

                if " TYPE:" in c.upper():
                    d['license_type'] = c.upper().split(' TYPE: ')[-1].split()[0]

                # Extract License Type
                if "PROFESSION:" in c.upper():
                    d['license_type'] = c.upper().split('PROFESSION: ')[-1].split()[0]

                # Extract effective date
                if "ISSUED:" in c.upper():
                    d['effective_date'] = c.upper().split('ISSUED:')[-1].strip().split()[0]   

                # Extract expire date
                if "EXPIRES:" in c.upper():
                    d['expire_date'] = c.upper().split('EXPIRES: ')[-1].strip().split()[0]

                # Extract license status
                if "STATUS:" in c.upper():
                    d['license_status'] = c.upper().split('STATUS: ')[-1].strip().split()[0]

                # extract the "Additional Public Information" for sanction
                if 'DISCIPLINARY ACTION' in c.upper():
                    idx = content.index(c)
                    d['board_sanction'] = content[idx + 2].strip()
                    if 'HOSPITAL' in d['board_sanction'].upper():
                        d['board_sanction']='None'
            
            save_info(driver,state,li_number)
            return d

        sdata = sdata.astype(str)
        length = int(sdata.shape[0])
#         print('Number files = ',length)
        if stop == True:
            stop = length

        # Write a file for the output

        file=Results(state)
        file.w(headers)
        result=[]  
        number_files=int(stop-start)
        for i in range(start, stop):
            print('processing '+state+ ': '+str(i+1)+' of '+str(number_files),'\n')
            
            if '--proxy-server' in driver_params:
                if i%loop_break==int(loop_break-1):
                    driver.quit()
                    driver = get_driver(url,driver_params)
                    driver = try_get_url(driver,driver_params,url)
            
            li_number = str(sdata.iloc[i].loc['num'])
            f_name = sdata.iloc[i].loc['fn']
            l_name = sdata.iloc[i].loc['ln']
            driver = try_get_url(driver,driver_params,url)
            time.sleep(pause_time)

            #extract the input parameters for the scraper function
            try:
                
                output=single_scrape(driver, li_number, f_name, l_name)
            except Exception as inst:

                result.append((i,inst))
                driver = try_get_url(driver,driver_params,url)
                time.sleep(pause_time)
                continue
                
            if output['license_status']=='':
                driver= try_get_url(driver,driver_params,url_prof)
                time.sleep(pause_time)
                try:
                    output=single_scrape_prof(driver, li_number, f_name, l_name)
                except Exception as inst:

                    result.append((i,inst))
                    driver = try_get_url(driver,driver_params,url)
                    time.sleep(pause_time)
                    continue
                

            output['num']=li_number
            output['fn']=f_name
            output['ln']=l_name
            output['state']=state
            result.append((i,output))        
            file.w(output,method='add')

        driver.quit()
        return result

    except Exception as inst:
        print('Scraper Exception - Something bad happened in scraper_function not single_scrape')
        print(inst)
        if 'driver' in locals():
            try:
                driver.quit()
            except:
                pass
        return True,inst
    

    #################################################################################
#################################################################################
#################################################################################
# needs Tesseract and Magick Package ? Maybe?
# # # # Tennessee
def scrape_TN(sdata, driver_params, start=0, stop=True):
    try:
        state='TN'
        url = "https://apps.health.tn.gov/Licensure/default.aspx"
        driver = get_driver(url,driver_params)
        driver = try_get_url(driver,driver_params,url)
        time.sleep(pause_time)

        def single_scrape(wbd, li_number, f_name, l_name):
            pause_time = 2 + random.random() * 2
            short_pause=random.randint(1,5) / 10 + random.random()
            d = feature_dict(state, li_number, f_name, l_name)
            driver=wbd
            
            file_int=str(random.randint(0,20))

            wait = WebDriverWait(driver, 10)

            driver.find_element_by_id('ctl00_PageContent_btnReset').click()

            found_code='DOES NOT MATCH'
            a=0
            while 'DOES NOT MATCH' in found_code.upper():
                if a>5:
                    break

                # select the license number search box
                license_num_search_box = driver.find_element_by_id('ctl00_PageContent_txtLicense')

                # fill search box with license number
                license_num_search_box.clear()
                license_num_search_box.send_keys(li_number)

                # select the last name search box
                last_name_search_box = driver.find_element_by_id('ctl00_PageContent_txtLastName')

                # fill search box with last name
                last_name_search_box.clear()
                last_name_search_box.send_keys(l_name)

                # # select the first name search box
                # first_name_search_box = driver.find_element_by_css_selector('#t_web_lookup__first_name')

                # # fill search box with first name
                # first_name_search_box.clear()
                # first_name_search_box.send_keys(f_name)

                urllib.request.urlcleanup()
                src=driver.find_element_by_tag_name('img').get_attribute('src')
                urllib.request.urlretrieve(src, "alert_data\\filename"+file_int+".jpg")

                im = Image.open("alert_data\\filename"+file_int+".jpg")
                image=im.convert('1').resize((720,200)).filter(ImageFilter.ModeFilter(size=15)).filter(ImageFilter.MaxFilter(size=1))
                w,h=image.size
                t=[]
                for i in range(5):
                    (left, upper, right, lower) = (i*w/5, 0, (i+1)*w/5, 200)
                    t.append(tesserocr.image_to_text(image.crop((left, upper, right, lower)),psm=PSM.SINGLE_CHAR).replace('\n','')[0])

                text=''.join(t)
                text=re.sub('\s','',text.translate(str.maketrans('', '', string.punctuation)))

                captcha_box=driver.find_element_by_name('ctl00$PageContent$CaptchaControl1')
                captcha_box.send_keys(text)
                time.sleep(3)
                # click the search button
                driver.find_element_by_id('ctl00_PageContent_btnSubmit').click()
                time.sleep(3)
                result=''
                try:
                    result=driver.find_element_by_id('ctl00_PageContent_lblSearchResults').text.upper()
                except NoSuchElementException:
                    pass

                if 'SEARCH RESULT' in result:
                    found_code=''
                    break
                found_code=driver.find_element_by_id('ctl00_PageContent_ctl00').text.upper()
                driver.find_element_by_id('ctl00_PageContent_btnReset').click()
                a+=1

            nores=''
            try:
                nores=driver.find_element_by_id('ctl00_PageContent_lblNoRecords').text
            except NoSuchElementException:
                pass

            if 'NO ' in nores.upper():
                d['license_type']='None Found'
                return d

            content = driver.find_element_by_id('ctl00_PageContent_dlstLicensure').text.split('\n')

            fullname = content[0].split('.')[-1].strip().upper()
            if (f_name.upper() not in fullname) and (l_name.upper() not in fullname):
                d=feature_dict(state, li_number, f_name, l_name)
                d['license_type']='None found'
                return d

            d['first_name']=fullname.split(',')[-1].strip()
            d['last_name']=fullname.split(',')[0].strip()

            for c in content:

                if "PROFESSION:" in c.upper():
                    d['license_type'] = c.upper().split(':')[-1].strip()

                # Extract License Type
                if "LICENSE NUMBER:" in c.upper():
                    d['license_number'] = c.upper().split(':')[-1].strip()

                # Extract effective date
                if "ORIGINAL DATE:" in c.upper():
                    d['effective_date'] = c.upper().split(':')[-1].strip()

                # Extract expire date
                if "EXPIRATION DATE:" in c.upper():
                    d['expire_date'] = c.upper().split(':')[-1].strip()

                # Extract license status
                if "STATUS:" in c.upper():
                    d['license_status'] = c.upper().split(':')[-1].strip()

            
            save_info(driver,state,li_number)
            return d

        sdata = sdata.astype(str)
        length = int(sdata.shape[0])
#         print('Number files = ',length)
        if stop == True:
            stop = length

        # Write a file for the output

        file=Results(state)
        file.w(headers)
        result=[]  
        number_files=int(stop-start)
        for i in range(start, stop):
            print('processing '+state+ ': '+str(i+1)+' of '+str(number_files),'\n')
            
            if '--proxy-server' in driver_params:
                if i%loop_break==int(loop_break-1):
                    driver.quit()
                    driver = get_driver(url,driver_params)
                    driver = try_get_url(driver,driver_params,url)
            
            li_number = str(sdata.iloc[i].loc['num'])
            f_name = sdata.iloc[i].loc['fn']
            l_name = sdata.iloc[i].loc['ln']
            driver = try_get_url(driver,driver_params,url)
            time.sleep(pause_time)

            #extract the input parameters for the scraper function
            try:
                
                output=single_scrape(driver, li_number, f_name, l_name)
            except Exception as inst:
                print(li_number, inst, traceback.format_exc())
                result.append((i,inst))
                driver = try_get_url(driver,driver_params,url)
                time.sleep(pause_time)
                continue                

            output['num']=li_number
            output['fn']=f_name
            output['ln']=l_name
            output['state']=state
            result.append((i,output))        
            file.w(output,method='add')

        driver.quit()
        return result

    except Exception as inst:
        print('Scraper Exception - Something bad happened in scraper_function not single_scrape')
        print(inst)
        if 'driver' in locals():
            try:
                driver.quit()
            except:
                pass
        return True,inst

    
#################################################################################
#################################################################################
#################################################################################
### State of Kansas

def scrape_KS(sdata, driver_params, start=0, stop=True):
    try:
        state='KS'
        url = "https://www.accesskansas.org/ssrv-ksbhada/search.html"
        url_nur = "https://www.kansas.gov/ksbn-verifications/search/records"
        url_opt = "https://www.accesskansas.org/ssrv-optometry/search/search.html" # Need to create
        url_psy = "https://www.kansas.gov/bsrb-verification/" # Need to create

        driver = get_driver(url,driver_params)
        
        def single_scrape(wbd, li_number, f_name, l_name):
            pause_time = 2 + random.random() * 2
            short_pause=random.randint(1,5) / 10 + random.random()
            d = feature_dict(state, li_number, f_name, l_name)
            driver=wbd

            wait = WebDriverWait(driver, 10)

            license_num_box_link = 'licenseNumber'
            # first_name_box_link = 'firstName'
            # last_name_box_link = 'lastName'
            search_button_link = 'id_submit'

            if len(l_name.split())>1:
                l_name=l_name.split()[-1]

            # select the license number search box
            license_num_search_box = driver.find_element_by_id(license_num_box_link)

            # fill search box with license number
            license_num_search_box.clear()
            license_num_search_box.send_keys(li_number)

            # click the search button
            driver.find_element_by_id(search_button_link).click()
            wait.until(EC.element_to_be_clickable((By.ID,'id1_back')))

            results=driver.find_element_by_id('agency-content').text

            if 'NO LICENSE' in results.upper():
                d['license_type']='None Found'
                return d

            searchnames=[
                l_name[0]+l_name[1:].lower()+' ',
                l_name[0]+l_name[1:].lower()+', ',
                l_name+' '+f_name,
                l_name+', '+f_name,
                l_name[0]+l_name[1:].lower()+' '+f_name[0]+f_name[1:].lower(),
                l_name[0]+l_name[1:].lower()+', '+f_name[0]+f_name[1:].lower()]
            found=False
            for name in searchnames[::-1]:
            #     print(name)
                try:
                    link=driver.find_element_by_partial_link_text(name).get_attribute('href')
                    driver.get(link)
                    found=True
                    break
                except NoSuchElementException:
                    pass

            if found==False:
                d['license_type'] ='None Found'
                return d

            wait.until(EC.element_to_be_clickable((By.ID,'id_new')))

            content = driver.find_element_by_id('agency-content').text.split('\n')
            for c in content:

                if "LICENSE NUMBER:" in c.upper():
                    d['license_number'] = c.upper().split(':')[-1].strip()

                if " TYPE:" in c.upper():
                    d['license_type'] = c.upper().split(':')[-1].strip()

                # Extract effective date
                if "LAST RENEWAL DATE:" in c.upper():
                    d['effective_date'] = c.upper().split(':')[-1].strip()   

                # Extract expire date
                if "LICENSE CANCELLATION DATE:" in c.upper():
                    d['expire_date'] = c.upper().split(':')[-1].strip()

                # Extract license status
                if "LICENSE STATUS:" in c.upper():
                    if 'DATE' in c:
                        continue
                    else:
                        d['license_status'] = c.upper().split(':')[-1].strip()
                        if 'PREVIOUS' in c.upper():
                            d['license_status']=d['license_type']

                # extract the "Additional Public Information" for sanction
                if 'ACTION' in c.upper():
                    idx = content.index(c)
                    d['board_sanction'] = content[idx + 1].upper().strip()

            save_info(driver,state,li_number)
            return d

        def single_scrape_nurse(wbd, li_number, f_name, l_name):
            pause_time = 2 + random.random() * 2
            short_pause=random.randint(1,5) / 10 + random.random()
            d = feature_dict(state, li_number, f_name, l_name)
            driver=wbd

            wait = WebDriverWait(driver, 10)

            license_num_box_link = 'licenseNumber'
            # first_name_box_link = 'firstName'
            # last_name_box_link = 'lastName'
            search_button_link = '_eventId_submit'

            if len(l_name.split())>1:
                l_name=l_name.split()[-1]

            # select the license number search box
            license_num_search_box = driver.find_element_by_id(license_num_box_link)

            # fill search box with license number
            license_num_search_box.clear()
            license_num_search_box.send_keys(li_number)

            # click the search button
            driver.find_element_by_name(search_button_link).click()

            error=''
            try:
                error=driver.find_element_by_id('commandErrorContent').text
            except NoSuchElementException:
                pass
            if len(error)>0:
                d['license_type']='None Found'
                return d

            wait.until(EC.text_to_be_present_in_element((By.NAME,search_button_link),'Perform'))

            results=driver.find_element_by_id('agency-content').text

            if 'NO RECORD FOUND' in results.upper():
                d['license_type']='None Found'
                return d

            found=False
            contents = driver.find_elements_by_id('search-results')
            for content in contents:
                for c in content.text.split('\n'):
                    if 'LAST NAME' in c.upper():
                        d['last_name'] = c.split(':')[-1].strip().upper()

                    if 'FIRST NAME' in c.upper():
                        d['first_name'] = c.split(':')[-1].strip().upper()

                    fullname = d['first_name']+' '+d['last_name']
                if (l_name.upper() not in fullname.upper()) & (f_name.upper() not in fullname.upper()):
                    continue 

                found=True
                for c in content.text.split('\n'):

                    if "LICENSE :" in c.upper():
                        d['license_number'] = c.upper().split(':')[-1].strip()

                    if "PROFESSIONAL" in c.upper():
                        d['license_type'] = c.upper().split(':')[-1].strip()

                    # Extract effective date
                    if "ISSUE DATE" in c.upper():
                        d['effective_date'] = c.upper().split(':')[-1].strip()   

                    # Extract expire date
                    if "EXPIRATION" in c.upper():
                        d['expire_date'] = c.upper().split(':')[-1].strip()

                    # Extract license status
                    if "STATUS" in c.upper():
                        d['license_status'] = c.upper().split(':')[-1].strip()

            if found==False:
                d = feature_dict(state, li_number, f_name, l_name)
                d['license_type'] = 'None Found'
                return d

            save_info(driver,state,li_number)
            return d

        sdata = sdata.astype(str)
        length = int(sdata.shape[0])
        if stop == True:
            stop = length
                
        # Write a file for the output

        file=Results(state)
        file.w(headers)
        result=[]  
        number_files=int(stop-start)
        for i in range(start, stop):
            print('processing '+state+ ': '+str(i+1)+' of '+str(number_files),'\n')
            
            if '--proxy-server' in driver_params:
                if i%loop_break==int(loop_break-1):
                    driver.quit()
                    driver = get_driver(url,driver_params)
                    driver = try_get_url(driver,driver_params,url)
            
            li_number = str(sdata.iloc[i].loc['num'])
            f_name = sdata.iloc[i].loc['fn']
            l_name = sdata.iloc[i].loc['ln']

            driver = try_get_url(driver,driver_params,url)
            time.sleep(pause_time)
            try:
                output=single_scrape(driver, li_number, f_name, l_name)
            except Exception as inst:

                result.append((i,inst))
                driver = try_get_url(driver,driver_params,url)
                time.sleep(pause_time)
                continue

            if output['license_status']=="":
                driver = try_get_url(driver,driver_params,url_nur)
                time.sleep(pause_time)
                try:
                    output=single_scrape_nurse(driver, li_number, f_name, l_name)
                except Exception as inst:
                        
                    result.append((i,inst))
                    driver = try_get_url(driver,driver_params,url)
                    time.sleep(pause_time)
                    continue
                        
# #                 if output['license_status']=="":
# #                     driver = try_get_url(driver,driver_params,url_psy)
# #                     time.sleep(pause_time)
# #                     try:
# #                         output=single_scrape_psy(driver, li_number, f_name, l_name)
# #                     except Exception as inst:

# #                         result.append((i,inst))
# #                         driver = try_get_url(driver,driver_params,url)
# #                         time.sleep(pause_time)
# #                         continue
                        
            output['num']=li_number
            output['fn']=f_name
            output['ln']=l_name
            output['state']=state
            result.append((i,output))        
            file.w(output,method='add')
        driver.quit()
        return result

    except Exception as inst:
        print('Scraper Exception - Something bad happened in scraper_function not single_scrape')
        print(inst)
        if 'driver' in locals():
            try:
                driver.quit()
            except:
                pass
        return True,inst    


    
#################################################################################
#################################################################################
#################################################################################
### State of Kentucky

def scrape_KY(sdata, driver_params, start=0, stop=True):
    try:
        state='KY'
        url = "http://web1.ky.gov/GenSearch/LicenseSearch.aspx?AGY=5"
        url_pa = "http://web1.ky.gov/GenSearch/LicenseSearch.aspx?AGY=20"
        url_opt = "http://web1.ky.gov/gensearch/LicenseSearch.aspx?AGY=8" # Need to create
        url_chir = "http://web1.ky.gov/gensearch/LicenseSearch.aspx?AGY=22" 
        url_pod = "http://web1.ky.gov/gensearch/LicenseSearch.aspx?AGY=24" 

        driver = get_driver(url,driver_params)
        
        def single_scrape(wbd, li_number, f_name, l_name):
            pause_time = 2 + random.random() * 2
            short_pause=random.randint(1,5) / 10 + random.random()
            d = feature_dict(state, li_number, f_name, l_name)
            driver=wbd

            wait = WebDriverWait(driver, 10)

            li_number=li_number.strip()

            license_num_box_link = 'usLicenseSearch_txtField2'
            # first_name_box_link = 'firstName'
            # last_name_box_link = 'lastName'
            search_button_link = 'usLicenseSearch_btnSearch'

            if len(l_name.split())>1:
                l_name=l_name.split()[-1]

            # select the license number search box
            license_num_search_box = driver.find_element_by_id(license_num_box_link)

            # fill search box with license number
            license_num_search_box.clear()
            license_num_search_box.send_keys(li_number)

            # click the search button
            driver.find_element_by_id(search_button_link).click()

            wait.until(EC.element_to_be_clickable((By.ID,'usLicenseList_btnPrint')))

            content=driver.find_element_by_css_selector('#Form1 > div.ky-content > div').text.split('\n')
            if len(content)<8:
                d['license_type']='None Found'
                return d

            start_num=0
            lic_nums=[]
            name_nums=[]
            for i,c in enumerate(content):
                if "LICENSE:" in c.upper():
                    if (content[i+1] in li_number) | (li_number in content[i+1]):
                        start_num=i
                    lic_nums.append(i)
                if "NAME" in c.upper():
                    name_nums.append(i)

            start=0
            if (len(name_nums)>0):
                start=[x for x in name_nums if x<start_num][-1]

            end_num=-1
            if (len(lic_nums)>0) and (max(lic_nums)>start_num):
                end_num=[x for x in lic_nums if x>start_num][0]

            content1=content[start:end_num]
            stop_num=-1
            for i,c in enumerate(content1):
                if 'BOARD ACTION' in c.upper():
                    stop_num=i
            content2=content1[:stop_num+2]
            content=content2

            for c in content:
                if 'NAME:' in c.upper():
                    fullname= content[content.index(c)+1].upper()
                    if (l_name.upper() not in fullname) & (f_name.upper() not in fullname):
                        d['license_type']='None Found'
                        return d
                    allname=content[content.index(c)+1].split()[1:]

                    d['last_name'] = ' '.join(allname).strip().upper()
                    d['first_name'] = content[content.index(c)+1].split()[0].strip().upper()

                if "LICENSE NUMBER:" in c.upper():
                    d['license_number'] = content[content.index(c)+1].strip().upper()

                # Extract effective date
                if "YEAR LICENSED" in c.upper():
                    d['effective_date'] = content[content.index(c)+1].split()[0].strip().upper()  

                # Extract expire date
                if "EXPIRATION" in c.upper():
                    d['expire_date'] = content[content.index(c)+1].split()[0].strip().upper()

                # Extract expire date
                if "STATUS" in c.upper():
                    d['license_status'] = content[content.index(c)+1].strip().upper()

                # Extract license status
                if "BOARD ACTION" in c.upper():
                    d['board_sanction'] = content[content.index(c)+1].strip().upper()

            if (d['expire_date']!='') and (d['license_status']==''):
                currentdate = datetime.datetime.strptime(d['License_As_Of_Date'], "%Y/%m/%d")
                try:
                    expiredate=datetime.datetime.strptime(d['expire_date'], "%m/%d/%Y")
                except ValueError:
                    d = feature_dict(state, li_number, f_name, l_name)
                    d['license_type']='Scraper Broken'
                    return d

                if currentdate>expiredate:
                    d['license_status']='Expired'
                else:
                    d['license_status']='Active'

            d['license_type']='Physician'

            save_info(driver,state,li_number)
            return d

        def single_scrape_pa(wbd, li_number, f_name, l_name):
            pause_time = 2 + random.random() * 2
            short_pause=random.randint(1,5) / 10 + random.random()
            d = feature_dict(state, li_number, f_name, l_name)
            driver=wbd

            wait = WebDriverWait(driver, 10)

            li_number=li_number.strip()

            if 'PA'!=li_number[:2].upper():
                li_number='PA'+li_number

            license_num_box_link = 'usLicenseSearch_txtField2'
            # first_name_box_link = 'firstName'
            # last_name_box_link = 'lastName'
            search_button_link = 'usLicenseSearch_btnSearch'

            if len(l_name.split())>1:
                l_name=l_name.split()[-1]

            # select the license number search box
            license_num_search_box = driver.find_element_by_id(license_num_box_link)

            # fill search box with license number
            license_num_search_box.clear()
            license_num_search_box.send_keys(li_number)

            # click the search button
            driver.find_element_by_id(search_button_link).click()

            wait.until(EC.element_to_be_clickable((By.ID,'usLicenseList_btnPrint')))

            content=driver.find_element_by_css_selector('#Form1 > div.ky-content > div').text.split('\n')
            
            if len(content)<8:
                d['license_type']='None Found'
                return d
            
            start_num=0
            lic_nums=[]
            name_nums=[]
            for i,c in enumerate(content):
                if "LICENSE NUMBER:" in c.upper():
                    if (content[i+1] in li_number) | (li_number in content[i+1]):
                        start_num=i
                    lic_nums.append(i)
                if "NAME" in c.upper():
                    name_nums.append(i)

            start=0
            if (len(name_nums)>0):
                start=[x for x in name_nums if x<start_num][-1]

            end_num=-1
            if (len(lic_nums)>0) and (max(lic_nums)>start_num):
                end_num=[x for x in lic_nums if x>start_num][0]

            content1=content[start:end_num]
            stop_num=-1
            for i,c in enumerate(content1):
                if '*' in c:
                    stop_num=i
            content2=content1[:stop_num]
            content=content2

            for c in content:
                if ' NAME:' in c.upper():
                    fullname= content[content.index(c)+1].upper()
                    if (l_name.upper() not in fullname) & (f_name.upper() not in fullname):
                        d['license_type']='None Found'
                        return d

                    d['last_name'] = content[content.index(c)+1].split()[1].strip()
                    d['first_name'] = content[content.index(c)+1].split()[1].strip()

                if "LICENSE NUMBER:" in c.upper():
                    d['license_number'] = content[content.index(c)+1].strip().upper()

                # Extract effective date
                if "YEAR LICENSED" in c.upper():
                    d['effective_date'] = content[content.index(c)+1].split()[0].strip().upper()  

                # Extract expire date
                if "EXPIRATION" in c.upper():
                    d['expire_date'] = content[content.index(c)+1].split()[0].strip().upper()

                # Extract license status
                if "BOARD ACTION" in c.upper():
                    d['board_sanction'] = content[content.index(c)+1].strip().upper()

            if d['expire_date']!='':
                currentdate = datetime.datetime.strptime(d['License_As_Of_Date'], "%Y/%m/%d")
                try:
                    expiredate=datetime.datetime.strptime(d['expire_date'], "%m/%d/%Y")
                except ValueError:
                    d = feature_dict(state, li_number, f_name, l_name)
                    d['license_type']='Scraper Broken'
                    return d

                if currentdate>expiredate:
                    d['license_status']='Expired'
                else:
                    d['license_status']='Active'

            d['license_type']='Physician Assistant'

            save_info(driver,state,li_number)
            return d
        
        def single_scrape_chiro(wbd, li_number, f_name, l_name):
            pause_time = 2 + random.random() * 2
            short_pause=random.randint(1,5) / 10 + random.random()
            d = feature_dict(state, li_number, f_name, l_name)
            driver=wbd

            wait = WebDriverWait(driver, 10)

            li_number=li_number.strip()

            license_num_box_link = 'usLicenseSearch_txtField2'
            # first_name_box_link = 'firstName'
            # last_name_box_link = 'lastName'
            search_button_link = 'usLicenseSearch_btnSearch'

            if len(l_name.split())>1:
                l_name=l_name.split()[-1]

            # select the license number search box
            license_num_search_box = driver.find_element_by_id(license_num_box_link)

            # fill search box with license number
            license_num_search_box.clear()
            license_num_search_box.send_keys(li_number)

            # click the search button
            driver.find_element_by_id(search_button_link).click()

            wait.until(EC.element_to_be_clickable((By.ID,'usLicenseList_btnPrint')))

            content=driver.find_element_by_css_selector('#Form1 > div.ky-content > div').text.split('\n')
            if len(content)<8:
                d['license_type']='None Found'
                return d

            start_num=0
            lic_nums=[]
            name_nums=[]
            for i,c in enumerate(content):
                if "LICENSE:" in c.upper():
                    if (content[i+1] in li_number) | (li_number in content[i+1]):
                        start_num=i
                    lic_nums.append(i)
                if "NAME" in c.upper():
                    name_nums.append(i)

            start=0
            if (len(name_nums)>0):
                start=[x for x in name_nums if x<start_num][-1]

            end_num=-1
            if (len(lic_nums)>0) and (max(lic_nums)>start_num):
                end_num=[x for x in lic_nums if x>start_num][0]

            content1=content[start:end_num]
            stop_num=-1
            for i,c in enumerate(content1):
                if 'FACILITY' in c.upper():
                    stop_num=i
            content2=content1[:stop_num+2]
            content=content2

            for c in content:
                if 'NAME:' in c.upper():
                    fullname= content[content.index(c)+1].upper()
                    if (l_name.upper() not in fullname) & (f_name.upper() not in fullname):
                        d['license_type']='None Found'
                        print(d)
                    allname=content[content.index(c)+1].split()[1:]

                    d['last_name'] = ' '.join(allname).strip().upper()
                    d['first_name'] = content[content.index(c)+1].split()[0].strip().upper()

                if "LICENSE NUMBER:" in c.upper():
                    d['license_number'] = content[content.index(c)+1].strip().upper()

                # Extract effective date
                if "ISSUE DATE" in c.upper():
                    d['effective_date'] = content[content.index(c)+1].split()[0].strip().upper()  

                # Extract expire date
                if "EXPIRATION DATE" in c.upper():
                    d['expire_date'] = content[content.index(c)+1].split()[0].strip().upper()

                # Extract expire date
                if "STATUS" in c.upper():
                    d['license_status'] = content[content.index(c)+1].strip().upper()

                # Extract license status
                if "DISCIPLINARY" in c.upper():
                    d['board_sanction'] = content[content.index(c)+1].strip().upper()

            d['license_type']='Chiropractor'

            date_repl={'JAN':'01','FEB':'02','MAR':'03','APR':'04',
                      'MAY':'05','JUN':'06','JUL':'07','AUG':'08',
                      'SEP':'09','OCT':'10','NOV':'11','DEC':'12'}

            if d['expire_date']!='':
                for key,value in zip(date_repl.keys(),date_repl.values()):
                    if key in d['expire_date'].upper():
                        d['expire_date']=d['expire_date'].replace(key,value)
            day=d['expire_date'].split('-')[0]
            month=d['expire_date'].split('-')[1]
            year=d['expire_date'].split('-')[2]
            if len(year)==2:
                year='20'+year
            d['expire_date']=month+'/'+day+'/'+year

            if d['effective_date']!='':
                for key,value in zip(date_repl.keys(),date_repl.values()):
                    if key in d['effective_date'].upper():
                        d['effective_date']=d['effective_date'].replace(key,value)
            day=d['effective_date'].split('-')[0]
            month=d['effective_date'].split('-')[1]
            year=d['effective_date'].split('-')[2]
            if (len(year)==2):
                if (int(year)>20):
                    year='19'+year
                else:
                    year='20'+year

            d['effective_date']=month+'/'+day+'/'+year        

            save_info(driver,state,li_number)
            return d

        def single_scrape_pod(wbd, li_number, f_name, l_name):
            pause_time = 2 + random.random() * 2
            short_pause=random.randint(1,5) / 10 + random.random()
            d = feature_dict(state, li_number, f_name, l_name)
            driver=wbd

            wait = WebDriverWait(driver, 10)

            li_number=li_number.strip()

            license_num_box_link = 'usLicenseSearch_txtField2'
            # first_name_box_link = 'firstName'
            # last_name_box_link = 'lastName'
            search_button_link = 'usLicenseSearch_btnSearch'

            if len(l_name.split())>1:
                l_name=l_name.split()[-1]

            # select the license number search box
            license_num_search_box = driver.find_element_by_id(license_num_box_link)

            # fill search box with license number
            license_num_search_box.clear()
            license_num_search_box.send_keys(li_number)

            # click the search button
            driver.find_element_by_id(search_button_link).click()

            wait.until(EC.element_to_be_clickable((By.ID,'usLicenseList_btnPrint')))

            content=driver.find_element_by_css_selector('#Form1 > div.ky-content > div').text.split('\n')
            if len(content)<8:
                d['license_type']='None Found'
                return d

            start_num=0
            lic_nums=[]
            name_nums=[]
            for i,c in enumerate(content):
                if "LICENSE:" in c.upper():
                    if (content[i+1] in li_number) | (li_number in content[i+1]):
                        start_num=i
                    lic_nums.append(i)
                if "NAME" in c.upper():
                    name_nums.append(i)

            start=0
            if (len(name_nums)>0):
                start=[x for x in name_nums if x<start_num][-1]

            end_num=-1
            if (len(lic_nums)>0) and (max(lic_nums)>start_num):
                end_num=[x for x in lic_nums if x>start_num][0]

            content1=content[start:end_num]
            stop_num=-1
            for i,c in enumerate(content1):
                if 'STATUS' in c.upper():
                    stop_num=i
            content2=content1[:stop_num+2]
            content=content2

            for c in content:
                if 'NAME:' in c.upper():
                    fullname= content[content.index(c)+1].upper()
                    if (l_name.upper() not in fullname) & (f_name.upper() not in fullname):
                        d['license_type']='None Found'
                        return d
                    
                    allname=content[content.index(c)+1].split()[1:]

                    d['last_name'] = ' '.join(allname).strip().upper()
                    d['first_name'] = content[content.index(c)+1].split()[0].strip().upper()

                if "LICENSE:" in c.upper():
                    d['license_number'] = content[content.index(c)+1].strip().upper()

                # Extract effective date
                if "ISSUE DATE" in c.upper():
                    d['effective_date'] = content[content.index(c)+1].split()[0].strip().upper()  

                # Extract expire date
                if "EXPIRATION" in c.upper():
                    d['expire_date'] = content[content.index(c)+1].split()[0].strip().upper()

                # Extract expire date
                if "STATUS" in c.upper():
                    d['license_status'] = content[content.index(c)+1].strip().upper()

                # Extract license status
                if "DISCIPLINARY" in c.upper():
                    d['board_sanction'] = content[content.index(c)+1].strip().upper()

            if (d['expire_date']!='') and (d['license_status']==''):
                currentdate = datetime.datetime.strptime(d['License_As_Of_Date'], "%Y/%m/%d")
                try:
                    expiredate=datetime.datetime.strptime(d['expire_date'], "%m/%d/%Y")
                except ValueError:
                    d = feature_dict(state, li_number, f_name, l_name)
                    d['license_type']='Scraper Broken'
                    return d

                if currentdate>expiredate:
                    d['license_status']='Expired'
                else:
                    d['license_status']='Active'

            d['license_type']='Podiatrist'

            save_info(driver,state,li_number)
            return d

        sdata = sdata.astype(str)
        length = int(sdata.shape[0])
        if stop == True:
            stop = length
                
        # Write a file for the output

        file=Results(state)
        file.w(headers)
        result=[]  
        number_files=int(stop-start)
        for i in range(start, stop):
            print('processing '+state+ ': '+str(i+1)+' of '+str(number_files),'\n')
            
            if '--proxy-server' in driver_params:
                if i%loop_break==int(loop_break-1):
                    driver.quit()
                    driver = get_driver(url,driver_params)
                    driver = try_get_url(driver,driver_params,url)
            
            li_number = str(sdata.iloc[i].loc['num'])
            f_name = sdata.iloc[i].loc['fn']
            l_name = sdata.iloc[i].loc['ln']
            
            PA_searched=False
            output={'license_status':''}
            
            if 'PA'==li_number.strip()[:2]:
                PA_searched=True
                driver = try_get_url(driver,driver_params,url_pa)
                time.sleep(pause_time)
                try:
                    output=single_scrape_pa(driver, li_number, f_name, l_name)
                except Exception as inst:
                        
                    result.append((i,inst))
                    driver = try_get_url(driver,driver_params,url)
                    time.sleep(pause_time)
                    continue
                
            if output['license_status']=="":
                driver = try_get_url(driver,driver_params,url)
                time.sleep(pause_time)
                try:
                    output=single_scrape(driver, li_number, f_name, l_name)
                except Exception as inst:

                    result.append((i,inst))
                    driver = try_get_url(driver,driver_params,url)
                    time.sleep(pause_time)
                    continue

            if (output['license_status']=="") and (PA_searched==False):
                driver = try_get_url(driver,driver_params,url_pa)
                time.sleep(pause_time)
                try:
                    output=single_scrape_pa(driver, li_number, f_name, l_name)
                except Exception as inst:
                        
                    result.append((i,inst))
                    driver = try_get_url(driver,driver_params,url)
                    time.sleep(pause_time)
                    continue
                        
            if output['license_status']=="":
                driver = try_get_url(driver,driver_params,url_chir)
                time.sleep(pause_time)
                try:
                    output=single_scrape_chiro(driver, li_number, f_name, l_name)
                except Exception as inst:

                    result.append((i,inst))
                    driver = try_get_url(driver,driver_params,url)
                    time.sleep(pause_time)
                    continue
                    
            if output['license_status']=="":
                driver = try_get_url(driver,driver_params,url_pod)
                time.sleep(pause_time)
                try:
                    output=single_scrape_pod(driver, li_number, f_name, l_name)
                except Exception as inst:

                    result.append((i,inst))
                    driver = try_get_url(driver,driver_params,url)
                    time.sleep(pause_time)
                    continue
                        
            output['num']=li_number
            output['fn']=f_name
            output['ln']=l_name
            output['state']=state
            result.append((i,output))        
            file.w(output,method='add')
        driver.quit()
        return result

    except Exception as inst:
        print('Scraper Exception - Something bad happened in scraper_function not single_scrape')
        print(inst)
        if 'driver' in locals():
            try:
                driver.quit()
            except:
                pass
        return True,inst    
    
# state of Washington
# we can add names attribute directly from variable 'names'
def scrape_WA(sdata, driver_params, start=0, stop=True):
    try:
        state='WA'
        url = "https://data.wa.gov/resource/qxh8-f4bd.json?credentialnumber="
        
        def single_scrape(url, li_number, f_name, l_name):
            pause_time = 2 + random.random() * 2
            short_pause=random.randint(1,5) / 10 + random.random()
            d = feature_dict(state, li_number, f_name, l_name)

            url_li=url+li_number.strip()
            data=''
            with urllib.request.urlopen(url_li) as info:
                data = json.loads(info.read().decode('utf-8'))[0]

            if len(data)==0:
                d['license_type']='None Found'
                return d

            d['first_name']=data['firstname'].strip().upper()
            d['last_name']=data['lastname'].strip().upper()
            d['license_type']=data['credentialtype'].strip().upper()
            d['license_status']=data['status'].strip().upper()
            if len(data['lastissuedate'].strip())==8:
                d['effective_date']=data['lastissuedate'][4:6]+'/'+data['lastissuedate'][-2:]+'/'+data['lastissuedate'][:4]
            else:
                d['effective_date']=data['lastissuedate']

            if len(data['expirationdate'].strip())==8:
                d['expire_date']=data['expirationdate'][4:6]+'/'+data['expirationdate'][-2:]+'/'+data['expirationdate'][:4]
            else:
                d['expire_date']=data['expirationdate']

            d['board_sanction']=data['actiontaken']

            fullname=data['firstname']+' '
            try:
                fullname = fullname+data['middlename']+' '
            except:
                pass
            fullname = fullname+data['lastname']

            if (f_name.upper() not in fullname.upper()) and (l_name.upper() not in fullname.upper()):
                d['license_type'] = 'Name Mismatch'
                return d
            
#             save_info(driver,state,li_number)
            return d
                                    

        sdata = sdata.astype(str)
        length = int(sdata.shape[0])
        if stop == True:
            stop = length

    # Write a file for the output
        # Write a file for the output

        file=Results(state)
        file.w(headers)
        result=[]  
        number_files=int(stop-start)
        for i in range(start, stop):
            print('processing '+state+ ': '+str(i+1)+' of '+str(number_files),'\n')
            
            li_number = str(sdata.iloc[i].loc['num'])
            f_name = sdata.iloc[i].loc['fn']
            l_name = sdata.iloc[i].loc['ln']

            try:
                output=single_scrape(url, li_number, f_name, l_name)
            except Exception as inst:
                print(inst)
                result.append((i,inst))
                continue
                
            output['num']=li_number
            output['fn']=f_name
            output['ln']=l_name
            output['state']=state
            result.append((i,output))        
            file.w(output,method='add')

        return result

    except Exception as inst:
        print('Scraper Exception - Something bad happened in scraper_function not single_scrape')
        print(inst)
        return True,inst    

    
#############################
##############################
############################
    
### State of Alaska

def scrape_AK(sdata, driver_params, start=0, stop=True):
    try:
        state='AK'
        url = "https://www.commerce.alaska.gov/cbp/DBDownloads/ProfessionalLicenseDownload.CSV"

        def single_scrape(sdata,url1):    
            print('Beginning file download with urllib2...')

            url = "https://www.commerce.alaska.gov/cbp/DBDownloads/ProfessionalLicenseDownload.CSV"
            loc = os.getcwd()+'/alert_data/AK_licenses_download.csv'
            urllib.request.urlretrieve(url, loc)
            print('File loaded...')
            df=pd.read_csv(loc,low_memory=False)
            dfm=df[['LicenseNum','STATE','Status','DateExpired']]
            merge=pd.merge(sdata.astype(str),df,how='left',left_on='num',right_on='LicenseNum')
            renamer={'LicenseNum':'SbwLicNum','STATE':'SbwLicState','Status':'SbwLicStatus','DateExpired':'SbwLicExpDate'}
            merge.rename(columns=renamer,inplace=True)

            cols=['num','state','fn','ln','SbwLicNum','SbwLicState','StateBoardFirst','StateBoardMiddle','StateBoardLast','SbwLicType'
            ,'SbwLicType2','SbwLicExpDate','SbwLicStatus','SbwLicQualifier','Notes']
            # merge=pd.merge(sdata.astype(str),df,how='left',left_on='num',right_on='LicenseNum')
            output=merge.reindex(columns=cols)
            
            return output

        try:
            output=single_scrape(sdata,url)
        except Exception as inst:
            print(inst)
            return True,inst
        
        dtime=str(datetime.datetime.now().strftime("%Y%m%d"))
        outfn='results/resultAK_'+dtime+'.txt'
        cwd=os.getcwd()
        outpn=cwd+'/'+outfn
        num=0
        if os.path.exists(outpn):
            while os.path.exists(outpn):
                num+=1
                outfn = 'results/resultAK_'+dtime+'_'+str(num)+'.txt'
                outpn=cwd+'/'+outfn

        output.to_csv(outfn,sep='|',index=False)
        
        result =list(output.T.to_dict().values())
            
        return result
        

    except Exception as inst:
        print('Scraper Exception - Something bad happened in scraper_function not single_scrape')
        print(inst)
        if 'driver' in locals():
            try:
                driver.quit()
            except:
                pass
        return True,inst
    
    
#################################################################################
#################################################################################
#################################################################################
# # # # New Mexico
def scrape_NM(sdata, driver_params, start=0, stop=True):
    try:
        state='NM'
        url = "http://docfinder.docboard.org/nm/"
        driver = get_driver(url,driver_params)

        def single_scrape(wbd, li_number, f_name, l_name):
            pause_time = 2 + random.random() * 2
            short_pause=random.randint(1,5) / 10 + random.random()
            d = feature_dict(state, li_number, f_name, l_name)
            driver=wbd

            wait = WebDriverWait(driver, 10)

            license_num_search_box_link = 'medlicno'
            search_button_link = 'body > blockquote > b > table:nth-child(3) > tbody > tr > td > form > input[type=submit]:nth-child(7)'

            # select the license number search box
            license_num_search_box = driver.find_element_by_name(license_num_search_box_link )

            # fill search box with license number
            license_num_search_box.clear()
            license_num_search_box.send_keys(li_number)

            # click the search button
            driver.find_element_by_css_selector(search_button_link).click()

            wait.until(EC.element_to_be_clickable((By.PARTIAL_LINK_TEXT,'Disclaimer')))

            results=''

            try:
                results=driver.find_element_by_css_selector('body > center:nth-child(1)').text.split('\n')[0]
            except NoSuchElementException:
                pass

            if 'NOT FOUND' in results:
                d['license_status'] = 'Not Found'
                return d

            options=driver.find_elements_by_css_selector('option')

            found=False
            if len(options)>0:
                for opt in options:
                    if (f_name not in opt.text) and (l_name not in opt.text):
                        continue
                    else:
                        opt.click()
                        found=True

            if found==False:

                try:
                    content=driver.find_element_by_css_selector('body > blockquote > center:nth-child(7) > table:nth-child(8)')
                    found=True
                except NoSuchElementException:
                    d['license_status']='None Found'
                    return d

            if found==False:
                d['license_status']='None Found'
                return d

            contents = driver.find_elements_by_css_selector('td')
            content = [x.text for x in contents] 
            for c in content:
                c1=c.upper()

                if "LICENSEE" in c1:
                    fullname = content[content.index(c)+1].strip().upper()
                    d['first_name'] =fullname.split()[0]
                    rest=fullname.split()[1:]
                    d['last_name'] = ' '.join(rest)

                # Extract License Type
                if "LICENSE TYPE" in c1:
                    d['license_type'] = content[content.index(c)+1].strip().upper()

                # Extract License Number
                if "LICENSE NUMBER" in c1:
                    d['license_number'] = content[content.index(c)+1].strip().upper()

                # Extract License status
                if "LICENSE STATUS" in c1:
                    d['license_status'] = content[content.index(c)+1].strip().upper()

                # Extract effective date
                if "LICENSE DATE" in c1:
                    d['effective_date'] = content[content.index(c)+1].strip().upper() 

                # Extract expire date
                if "LICENSE EXPIRE" in c1:
                    d['expire_date'] = content[content.index(c)+1].strip().upper() 

            try:
                # extract the "Additional Public Information" for sanction
                d['board_sanction'] = driver.find_element_by_css_selector('body > blockquote > center:nth-child(7) > table:nth-child(11) > tbody > tr:nth-child(1) > td:nth-child(1)').text.split(':')[-1].upper()
            except NoSuchElementException:
                pass

            if (l_name.upper() not in fullname.upper()) and (f_name.upper() not in fullname.upper()):
                d['license_type']='None Found'
                return d
            
            save_info(driver,state,li_number)
            return d
        
    
        sdata = sdata.astype(str)
        length = int(sdata.shape[0])
        if stop == True:
            stop = length

        # Write a file for the output

        file=Results(state)
        file.w(headers)
        result=[]  
        number_files=int(stop-start)
        for i in range(start, stop):
            print('processing '+state+ ': '+str(i+1)+' of '+str(number_files),'\n')
            
            if '--proxy-server' in driver_params:
                if i%loop_break==int(loop_break-1):
                    driver.quit()
                    driver = get_driver(url,driver_params)
                    driver = try_get_url(driver,driver_params,url)
            
            li_number = str(sdata.iloc[i].loc['num'])
            f_name = sdata.iloc[i].loc['fn']
            l_name = sdata.iloc[i].loc['ln']
            driver = try_get_url(driver,driver_params,url)
            time.sleep(pause_time)

            #extract the input parameters for the scraper function
            try:
                
                output=single_scrape(driver, li_number, f_name, l_name)
            except Exception as inst:

                result.append((i,inst))
                driver = try_get_url(driver,driver_params,url)
                time.sleep(pause_time)
                continue                

            output['num']=li_number
            output['fn']=f_name
            output['ln']=l_name
            output['state']=state
            result.append((i,output))        
            file.w(output,method='add')

        driver.quit()
        return result

    except Exception as inst:
        print('Scraper Exception - Something bad happened in scraper_function not single_scrape')
        print(inst)
        if 'driver' in locals():
            try:
                driver.quit()
            except:
                pass
        return True,inst
    

######################################################
######################################################   
### state of DELAWARE

def scrape_DE(sdata, driver_params, start=0, stop=True):
    try:
        state='DE'
        url="https://dpronline.delaware.gov/mylicense%20weblookup/Search.aspx"
        driver = get_driver(url,driver_params)
        driver = try_get_url(driver,driver_params,url)
        time.sleep(pause_time)
        def single_scrape(wbd, li_number, f_name, l_name):
            d = feature_dict(state, li_number, f_name, l_name)
            driver=wbd
            #web page text entry boxes:
            license_num_box_link = 't_web_lookup__license_no'
            last_name_box_link = 't_web_lookup__last_name'
            search_button_link = 'sch_button'

            # select the license number search box
            license_num_search_box = driver.find_element_by_id(license_num_box_link)

            # fill search box with license number
            license_num_search_box.clear()
            license_num_search_box.send_keys(li_number)

            # select the lastname search box
            last_name_search_box = driver.find_element_by_id(last_name_box_link)

            # # fill search box with last name
            last_name_search_box.clear()
            last_name_search_box.send_keys(l_name)



            # click the search button
            driver.find_element_by_id(search_button_link).click()
            time.sleep(pause_time)

            #searchnames creates list of permutations of possible names for each individual
            searchnames=[l_name+', '+f_name,l_name[0]+l_name[1:].lower()+', '+f_name[0]+f_name[1:].lower(),', '+f_name,', '+f_name[0]+f_name[1:].lower(),l_name+', ',l_name[0]+l_name[1:].lower()]
            found =False
            #reverse list of searchnames to check which name works better
            for name in searchnames[::-1]:
                try:
                    namefound=driver.find_element_by_partial_link_text(name)
                    link = namefound.get_attribute('href')
                    driver.get(link)
                    found=True
                    break
                except NoSuchElementException:
                    continue 

            if found == False:
                d['first_name'] = f_name
                d['last_name'] = l_name
                d['license_type']='None Found'
                return d
            else:
                d['first_name'] = f_name
                d['last_name'] = l_name
                d['license_number'] = driver.find_element_by_id('_ctl21__ctl1_license_no').text
                d['license_type'] = driver.find_element_by_id('_ctl21__ctl1_license_type').text     
                d['license_status'] = driver.find_element_by_id('_ctl21__ctl1_sec_lic_status').text
                d['expire_date'] = driver.find_element_by_id('_ctl21__ctl1_expiration_date').text
                d['effective_date'] = driver.find_element_by_id('_ctl21__ctl1_issue_date').text
                
                save_info(driver,state,li_number)
                return d
            
        sdata = sdata.astype(str)
        length = int(sdata.shape[0])
#         print('Number files = ',length)
        if stop == True:
            stop = length

        # Write a file for the output

        file=Results(state)
        file.w(headers)
        result=[]  
        number_files=int(stop-start)
        for i in range(start, stop):
            print('processing '+state+ ': '+str(i+1)+' of '+str(number_files),'\n')
            
            if '--proxy-server' in driver_params:
                if i%loop_break==int(loop_break-1):
                    driver.quit()
                    driver = get_driver(url,driver_params)
                    driver = try_get_url(driver,driver_params,url)
            
            li_number = str(sdata.iloc[i].loc['num'])
            f_name = sdata.iloc[i].loc['fn']
            l_name = sdata.iloc[i].loc['ln']
            driver = try_get_url(driver,driver_params,url)
            time.sleep(pause_time)

            #extract the input parameters for the scraper function
            try:
                
                output=single_scrape(driver, li_number, f_name, l_name)
            except Exception as inst:
                print(li_number, inst, traceback.format_exc())
                result.append((i,inst))
                driver = try_get_url(driver,driver_params,url)
                time.sleep(pause_time)
                continue                

            output['num']=li_number
            output['fn']=f_name
            output['ln']=l_name
            output['state']=state
            result.append((i,output))        
            file.w(output,method='add')

        driver.quit()
        return result

    except Exception as inst:
        print('Scraper Exception - Something bad happened in scraper_function not single_scrape')
        print(inst)
        if 'driver' in locals():
            try:
                driver.quit()
            except:
                pass
        return True,inst

######################################################
######################################################   
### state of Conneticut
def scrape_CT(sdata, driver_params, start=0, stop=True):
    try:
        state='CT'
        url="https://www.elicense.ct.gov/lookup/licenselookup.aspx"
        driver = get_driver(url,driver_params)
        driver = try_get_url(driver,driver_params,url)
        time.sleep(pause_time)
        def single_scrape(wbd, li_number, f_name, l_name):
            d = feature_dict(state, li_number, f_name, l_name)
            driver=wbd
            license_num_box_link = 'ctl00_MainContentPlaceHolder_ucLicenseLookup_ctl03_tbLicenseNumber'
            #first_name_box_link = 'BodyContent_tbFirstName'
            last_name_box_link = 'ctl00_MainContentPlaceHolder_ucLicenseLookup_ctl03_tbLastName_Contact'
            search_button_link = 'btnLookup'

            # select the license number search box
            license_num_search_box = driver.find_element_by_id(license_num_box_link)

            # fill search box with license number
            license_num_search_box.clear()
            license_num_search_box.send_keys(li_number)

            # select the lastname search box
            last_name_search_box = driver.find_element_by_id(last_name_box_link)

            # # fill search box with last name
            last_name_search_box.clear()
            last_name_search_box.send_keys(l_name)
            


            # click the search button
            driver.find_element_by_id(search_button_link).click()
            time.sleep(pause_time)
            
            #check if record exists
            found = False
            try:
                time.sleep(pause_time)
                detail_button = 'ctl00_MainContentPlaceHolder_ucLicenseLookup_gvSearchResults_ctl03_HyperLinkDetail'
                driver.find_element_by_id(detail_button).click()
                found=True
                
            except (NoSuchElementException,ElementClickInterceptedException):
                pass
            
            if found == False:
                
                d['first_name'] = f_name
                d['last_name'] = l_name
                d['license_type']='None Found'
                return d

            if found == True:
                
                time.sleep(pause_time)

                #get table info
                content = driver.find_element_by_id('Grid1')
                child_content = content.find_elements_by_css_selector('td') 
                child_headers = content.find_elements_by_css_selector('th')

                content_text = []
                for i in child_content:
                    content_text.append(i.text)

                header_text = []
                for i in child_headers:
                    header_text.append(i.text)

                interim = {}
                for name,info in zip(header_text,content_text):
                    interim[name] = info


                d['first_name'] = interim['License Name'].split()[0]
                d['last_name'] = interim['License Name'].split()[-1]
                d['license_number'] = interim['License Number']
                d['license_type'] = interim['License Type']    
                d['license_status'] = interim['License Status']
                d['expire_date'] = interim['Expiration Date']
                d['effective_date'] = interim['Granted Date']
                
                save_info(driver,state,li_number)
                return (d)
        
        sdata = sdata.astype(str)
        length = int(sdata.shape[0])
    #         print('Number files = ',length)
        if stop == True:
            stop = length

        # Write a file for the output

        file=Results(state)
        file.w(headers)
        result=[]  
        number_files=int(stop-start)
        for i in range(start, stop):
            print('processing '+state+ ': '+str(i+1)+' of '+str(number_files),'\n')
            
            if '--proxy-server' in driver_params:
                if i%loop_break==int(loop_break-1):
                    driver.quit()
                    driver = get_driver(url,driver_params)
                    driver = try_get_url(driver,driver_params,url)
            
            li_number = str(sdata.iloc[i].loc['num'])
            f_name = sdata.iloc[i].loc['fn']
            l_name = sdata.iloc[i].loc['ln']
            driver = try_get_url(driver,driver_params,url)
            time.sleep(pause_time)

            #extract the input parameters for the scraper function
            try:
                
                output=single_scrape(driver, li_number, f_name, l_name)
            except Exception as inst:
                print(li_number, inst, traceback.format_exc())
                result.append((i,inst))
                driver = try_get_url(driver,driver_params,url)
                time.sleep(pause_time)
                continue                

            output['num']=li_number
            output['fn']=f_name
            output['ln']=l_name
            output['state']=state
            result.append((i,output))        
            file.w(output,method='add')

        driver.quit()
        return result

    except Exception as inst:
        print('Scraper Exception - Something bad happened in scraper_function not single_scrape')
        print(inst)
        if 'driver' in locals():
            try:
                driver.quit()
            except:
                pass
        return True,inst



######################################################
######################################################   
### state of Vermont
def scrape_VT(sdata, driver_params, start=0, stop=True):
    try:
        state='VT'
        url="https://apps.health.vermont.gov/cavu/Lookup/LicenseLookup.aspx"
        driver = get_driver(url,driver_params)
        driver = try_get_url(driver,driver_params,url)
        time.sleep(pause_time)
        def single_scrape(wbd, li_number, f_name, l_name):
            d = feature_dict(state, li_number, f_name, l_name)
            driver=wbd
            #start state specific:
            #split li_number
            sp = re.split('\.|-',li_number)
            li_num_prefix = sp[0]
            li_num_suffix = sp[1]

            license_num_box_prefix = 'ctl00_MainContentPlaceHolder_ucLicenseLookup_ctl03_ddCredPrefix'
            license_num_box_suffix = 'ctl00_MainContentPlaceHolder_ucLicenseLookup_ctl03_tbLicenseNumber'
            last_name_box_link = 'ctl00_MainContentPlaceHolder_ucLicenseLookup_ctl03_tbLastName_Contact'
            search_button_link = 'btnLookup'

            #select lic prefix dropdown and input lic num prefix
            #drop = Select(driver.find_element_by_id(license_num_box_prefix))
            #drop.select_by_value(str(li_num_prefix))

            # select the license number search box
            license_num_search_box = driver.find_element_by_id(license_num_box_suffix)

            # fill search box with license number
            license_num_search_box.clear()
            license_num_search_box.send_keys(li_num_suffix)

            # select the lastname search box
            last_name_search_box = driver.find_element_by_id(last_name_box_link)

            # # fill search box with last name
            last_name_search_box.clear()
            last_name_search_box.send_keys(l_name)

            # click the search button
            driver.find_element_by_id(search_button_link).click()
            time.sleep(pause_time)

            #check if record exists
            found = False
            try:
                time.sleep(pause_time)
                detail_button = 'ctl00_MainContentPlaceHolder_ucLicenseLookup_gvSearchResults_ctl03_HyperLinkDetail'
                driver.find_element_by_id(detail_button).click()
                found=True

            except (NoSuchElementException,ElementClickInterceptedException):
                pass

            if found == False:

                d['first_name'] = f_name
                d['last_name'] = l_name
                d['license_type']='None Found'
                time.sleep(pause_time)
                return d
                
                

            if found == True:

                time.sleep(pause_time)

                #get lic table info
                content = driver.find_element_by_id('Grid1')
                child_content = content.find_elements_by_css_selector('td') 
                child_headers = content.find_elements_by_css_selector('th')

                content_text = []
                for i in child_content:
                    content_text.append(i.text)

                header_text = []
                for i in child_headers:
                    header_text.append(i.text)

                interim = {}
                for name,info in zip(header_text,content_text):
                    interim[name] = info



                d['license_number'] = interim['License']
                d['license_type'] = interim['License Type']    
                d['license_status'] = interim['Status']
                d['expire_date'] = interim['Expiration Date']
                d['effective_date'] = interim['First Date Licensed']

                #get name table info
                name_table = driver.find_element_by_id('Grid0')
                child_content_name = name_table.find_elements_by_css_selector('td') 
                child_headers_name = name_table.find_elements_by_css_selector('th')

                content_text_name = []
                for i in child_content_name:
                    content_text_name.append(i.text)

                header_text_name = []
                for i in child_headers_name:
                    header_text_name.append(i.text)

                interim_name = {}
                for name,info in zip(header_text_name,content_text_name):
                    interim_name[name] = info

                d['first_name'] = interim_name['Name'].split()[0]
                d['last_name'] = interim_name['Name'].split()[-1]

                
                return d
        
        
        # for scrape_master and all states
        sdata = sdata.astype(str)
        length = int(sdata.shape[0])
    #         print('Number files = ',length)
        if stop == True:
            stop = length

        # Write a file for the output

        file=Results(state)
        file.w(headers)
        result=[]  
        number_files=int(stop-start)
        for i in range(start, stop):
            print('processing '+state+ ': '+str(i+1)+' of '+str(number_files),'\n')
            
            if '--proxy-server' in driver_params:
                if i%loop_break==int(loop_break-1):
                    driver.quit()
                    driver = get_driver(url,driver_params)
                    driver = try_get_url(driver,driver_params,url)
            
            li_number = str(sdata.iloc[i].loc['num'])
            f_name = sdata.iloc[i].loc['fn']
            l_name = sdata.iloc[i].loc['ln']
            driver = try_get_url(driver,driver_params,url)
            time.sleep(pause_time)

            #extract the input parameters for the scraper function
            try:
                
                output=single_scrape(driver, li_number, f_name, l_name)
            except Exception as inst:
                print(li_number, inst, traceback.format_exc())
                result.append((i,inst))
                driver = try_get_url(driver,driver_params,url)
                time.sleep(pause_time)
                continue                

            output['num']=li_number
            output['fn']=f_name
            output['ln']=l_name
            output['state']=state
            result.append((i,output))        
            file.w(output,method='add')

        driver.quit()
        return result

    except Exception as inst:
        print('Scraper Exception - Something bad happened in scraper_function not single_scrape')
        print(inst)
        if 'driver' in locals():
            try:
                driver.quit()
            except:
                pass
        return True,inst


######################################################
######################################################   
### state of New Hampshire
def scrape_NH(sdata, driver_params, start=0, stop=True):
    try:
        state='NH'
        url="https://nhlicenses.nh.gov/verification/Search.aspx"
        driver = get_driver(url,driver_params)
        driver = try_get_url(driver,driver_params,url)
        time.sleep(pause_time)
        def single_scrape(wbd, li_number, f_name, l_name):
            d = feature_dict(state, li_number, f_name, l_name)
            driver=wbd
            #start state specific:
            license_num_box = 't_web_lookup__license_no'
            last_name_box_link = 't_web_lookup__last_name'
            search_button_link = 'sch_button'

            # select the license number search box
            license_num_search_box = driver.find_element_by_id(license_num_box)

            # fill search box with license number
            license_num_search_box.clear()
            license_num_search_box.send_keys(li_number)

            # select the lastname search box
            last_name_search_box = driver.find_element_by_id(last_name_box_link)

            # # fill search box with last name
            last_name_search_box.clear()
            last_name_search_box.send_keys(l_name)



            # click the search button
            driver.find_element_by_id(search_button_link).click()
            time.sleep(pause_time)

            #find table of results and extract items
            content = driver.find_element_by_id('datagrid_results')

            child_headers = content.find_elements_by_css_selector('th')
            child_content = content.find_elements_by_css_selector('td')

            header_text = []
            for i in child_headers:
                header_text.append(i.text)

            content_text = []
            for i in child_content:
                content_text.append(i.text)
            
            #test for results returned
            if len(content_text) > 1:    
                active_license = False

                #if there are active and inactive lic; bias towards active license
                if 'Active' in content_text:
                    active_license = True

                if active_license == True:
                    active_index=content_text.index('Active')
                    detail_link = child_content[(active_index - 4)].find_element_by_css_selector('a')
                    det_page = detail_link.get_attribute('href')
                    driver.get(det_page)

                    full_name_ret = '1_full_name'
                    lic_num_ret = '1_license_no'
                    lic_type_ret = '1_license_type'
                    lic_status_ret = '1_sec_lic_status'
                    lic_exp_ret = '1_expiration_date'
                    lic_eff_date_ret = '1_issue_date'

                    d['last_name'] = driver.find_element_by_xpath("//*[contains(@id, '1_full_name')]").text.split()[-1]
                    d['first_name'] = driver.find_element_by_xpath("//*[contains(@id, '1_full_name')]").text.split()[0]
                    d['license_number'] = driver.find_element_by_xpath("//*[contains(@id, '1_license_no')]").text
                    d['license_type'] = driver.find_element_by_xpath("//*[contains(@id, '1_license_type')]").text
                    d['license_status'] = driver.find_element_by_xpath("//*[contains(@id, '1_sec_lic_status')]").text
                    d['expire_date'] = driver.find_element_by_xpath("//*[contains(@id, '1_expiration_date')]").text
                    d['effective_date'] = driver.find_element_by_xpath("//*[contains(@id, '1_issue_date')]").text

                    return d

                if active_license == False:
                    detail_link = child_content[0].find_element_by_css_selector('a')
                    det_page = detail_link.get_attribute('href')
                    driver.get(det_page)

                    full_name_ret = '1_full_name'
                    lic_num_ret = '1_license_no'
                    lic_type_ret = '1_license_type'
                    lic_status_ret = '1_sec_lic_status'
                    lic_exp_ret = '1_expiration_date'
                    lic_eff_date_ret = '1_issue_date'

                    d['last_name'] = driver.find_element_by_xpath("//*[contains(@id, '1_full_name')]").text.split()[-1]
                    d['first_name'] = driver.find_element_by_xpath("//*[contains(@id, '1_full_name')]").text.split()[0]
                    d['license_number'] = driver.find_element_by_xpath("//*[contains(@id, '1_license_no')]").text
                    d['license_type'] = driver.find_element_by_xpath("//*[contains(@id, '1_license_type')]").text
                    d['license_status'] = driver.find_element_by_xpath("//*[contains(@id, '1_sec_lic_status')]").text
                    d['expire_date'] = driver.find_element_by_xpath("//*[contains(@id, '1_expiration_date')]").text
                    d['effective_date'] = driver.find_element_by_xpath("//*[contains(@id, '1_issue_date')]").text

                    return d

            if len(content_text) <= 1:
                d['first_name'] = f_name
                d['last_name'] = l_name
                d['license_type']='None Found'
                return d

        # for scrape_master and all states
        sdata = sdata.astype(str)
        length = int(sdata.shape[0])
    #         print('Number files = ',length)
        if stop == True:
            stop = length

        # Write a file for the output

        file=Results(state)
        file.w(headers)
        result=[]  
        number_files=int(stop-start)
        for i in range(start, stop):
            print('processing '+state+ ': '+str(i+1)+' of '+str(number_files),'\n')
            
            if '--proxy-server' in driver_params:
                if i%loop_break==int(loop_break-1):
                    driver.quit()
                    driver = get_driver(url,driver_params)
                    driver = try_get_url(driver,driver_params,url)
            
            li_number = str(sdata.iloc[i].loc['num'])
            f_name = sdata.iloc[i].loc['fn']
            l_name = sdata.iloc[i].loc['ln']
            driver = try_get_url(driver,driver_params,url)
            time.sleep(pause_time)

            #extract the input parameters for the scraper function
            try:
                
                output=single_scrape(driver, li_number, f_name, l_name)
            except Exception as inst:
                print(li_number, inst, traceback.format_exc())
                result.append((i,inst))
                driver = try_get_url(driver,driver_params,url)
                time.sleep(pause_time)
                continue                

            output['num']=li_number
            output['fn']=f_name
            output['ln']=l_name
            output['state']=state
            result.append((i,output))        
            file.w(output,method='add')

        driver.quit()
        return result

    except Exception as inst:
        print('Scraper Exception - Something bad happened in scraper_function not single_scrape')
        print(inst)
        if 'driver' in locals():
            try:
                driver.quit()
            except:
                pass
        return True,inst



######################################################
######################################################   
### District of Columbia (DC)
def scrape_DC(sdata, driver_params, start=0, stop=True):
    try:
        state='DC'
        url="https://app.hpla.doh.dc.gov/Weblookup/"
        driver = get_driver(url,driver_params)
        driver = try_get_url(driver,driver_params,url)
        time.sleep(pause_time)
        def single_scrape(wbd, li_number, f_name, l_name):
            d = feature_dict(state, li_number, f_name, l_name)
            driver=wbd
            #start state specific:
            license_num_box = 't_web_lookup__license_no'
            last_name_box_link = 't_web_lookup__last_name'
            search_button_link = 'sch_button'

            # select the license number search box
            license_num_search_box = driver.find_element_by_id(license_num_box)

            # fill search box with license number
            license_num_search_box.clear()
            license_num_search_box.send_keys(li_number)

            # select the lastname search box
            last_name_search_box = driver.find_element_by_id(last_name_box_link)

            # # fill search box with last name
            last_name_search_box.clear()
            last_name_search_box.send_keys(l_name)



            # click the search button
            driver.find_element_by_id(search_button_link).click()
            time.sleep(pause_time)
            

            content = driver.find_element_by_id('datagrid_results')

            child_headers = content.find_elements_by_css_selector('th')
            child_content = content.find_elements_by_css_selector('td')

            header_text = []
            for i in child_headers:
                header_text.append(i.text)

            content_text = []
            for i in child_content:
                content_text.append(i.text)

            if len(content_text) > 1:    
                active_license = False

                if 'Active' in content_text:
                    active_license = True

                if active_license == True:
                    active_index=content_text.index('Active')
                    detail_link = child_content[(active_index - 4)].find_element_by_css_selector('a')
                    det_page = detail_link.get_attribute('href')
                    driver.get(det_page)

                    d['last_name'] = driver.find_element_by_xpath("//*[contains(@id, 'full_name')]").text.split()[-1]
                    d['first_name'] = driver.find_element_by_xpath("//*[contains(@id, 'full_name')]").text.split()[0]
                    d['license_number'] = driver.find_element_by_xpath("//*[starts-with(@id, 'license_no')]").text
                    d['license_type'] = driver.find_element_by_xpath("//*[starts-with(@id, 'license_type')]").text
                    d['license_status'] = driver.find_element_by_xpath("//*[starts-with(@id, 'sec_lic_status')]").text
                    d['expire_date'] = driver.find_element_by_xpath("//*[starts-with(@id, 'expiration_date')]").text
                    d['effective_date'] = driver.find_element_by_xpath("//*[starts-with(@id, 'issue_date')]").text

                    return d

                if active_license == False:
                    detail_link = child_content[0].find_element_by_css_selector('a')
                    det_page = detail_link.get_attribute('href')
                    driver.get(det_page)

                    d['last_name'] = driver.find_element_by_xpath("//*[contains(@id, 'full_name')]").text.split()[-1]
                    d['first_name'] = driver.find_element_by_xpath("//*[contains(@id, 'full_name')]").text.split()[0]
                    d['license_number'] = driver.find_element_by_xpath("//*[starts-with(@id, 'license_no')]").text
                    d['license_type'] = driver.find_element_by_xpath("//*[starts-with(@id, 'license_type')]").text
                    d['license_status'] = driver.find_element_by_xpath("//*[starts-with(@id, 'sec_lic_status')]").text
                    d['expire_date'] = driver.find_element_by_xpath("//*[starts-with(@id, 'expiration_date')]").text
                    d['effective_date'] = driver.find_element_by_xpath("//*[starts-with(@id, 'issue_date')]").text

                    return d

            if len(content_text) <= 1:
                d['first_name'] = f_name
                d['last_name'] = l_name
                d['license_type']='None Found'
                return d
        
        # for scrape_master and all states
        sdata = sdata.astype(str)
        length = int(sdata.shape[0])
    #         print('Number files = ',length)
        if stop == True:
            stop = length

        # Write a file for the output

        file=Results(state)
        file.w(headers)
        result=[]  
        number_files=int(stop-start)
        for i in range(start, stop):
            print('processing '+state+ ': '+str(i+1)+' of '+str(number_files),'\n')
            
            if '--proxy-server' in driver_params:
                if i%loop_break==int(loop_break-1):
                    driver.quit()
                    driver = get_driver(url,driver_params)
                    driver = try_get_url(driver,driver_params,url)
            
            li_number = str(sdata.iloc[i].loc['num'])
            f_name = sdata.iloc[i].loc['fn']
            l_name = sdata.iloc[i].loc['ln']
            driver = try_get_url(driver,driver_params,url)
            time.sleep(pause_time)

            #extract the input parameters for the scraper function
            try:
                
                output=single_scrape(driver, li_number, f_name, l_name)
            except Exception as inst:
                print(li_number, inst, traceback.format_exc())
                result.append((i,inst))
                driver = try_get_url(driver,driver_params,url)
                time.sleep(pause_time)
                continue                

            output['num']=li_number
            output['fn']=f_name
            output['ln']=l_name
            output['state']=state
            result.append((i,output))        
            file.w(output,method='add')

        driver.quit()
        return result

    except Exception as inst:
        print('Scraper Exception - Something bad happened in scraper_function not single_scrape')
        print(inst)
        if 'driver' in locals():
            try:
                driver.quit()
            except:
                pass
        return True,inst

######################################################
######################################################   
### State of Rhode Island
def scrape_RI(sdata, driver_params, start=0, stop=True):
    try:
        state='RI'
        url="http://209.222.157.144/RIDOH_Verification/Search.aspx?facility=N&SubmitComplaint=Y"
        driver = get_driver(url,driver_params)
        driver = try_get_url(driver,driver_params,url)
        time.sleep(pause_time)
        def single_scrape(wbd, li_number, f_name, l_name):
            d = feature_dict(state, li_number, f_name, l_name)
            driver=wbd
            #start state specific:
            license_num_box = 't_web_lookup__license_no'
            last_name_box_link = 't_web_lookup__last_name'
            search_button_link = 'sch_button'

            # select the license number search box
            license_num_search_box = driver.find_element_by_id(license_num_box)

            # fill search box with license number
            license_num_search_box.clear()
            license_num_search_box.send_keys(li_number)

            # select the lastname search box
            last_name_search_box = driver.find_element_by_id(last_name_box_link)

            # # fill search box with last name
            last_name_search_box.clear()
            last_name_search_box.send_keys(l_name)



            # click the search button
            driver.find_element_by_id(search_button_link).click()
            time.sleep(pause_time)


            content = driver.find_element_by_id('datagrid_results')

            child_headers = content.find_elements_by_css_selector('th')
            child_content = content.find_elements_by_css_selector('td')

            content_text = []
            for i in child_content:
                content_text.append(i.text)
                
            
            if len(content_text) > 9:
                lic_index = content_text.index(li_number)
                link = child_content[lic_index-1].find_element_by_css_selector('a')
                link = link.get_attribute('href')
                driver.get(link)

                d['last_name'] = driver.find_element_by_xpath("//*[contains(@id, 'full_name')]").text.split()[-1]
                d['first_name'] = driver.find_element_by_xpath("//*[contains(@id, 'full_name')]").text.split()[0]
                d['license_number'] = driver.find_element_by_xpath("//*[contains(@id, '1_license_no')]").text
                d['license_type'] = driver.find_element_by_xpath("//*[contains(@id, '1_license_type')]").text
                d['license_status'] = driver.find_element_by_xpath("//*[contains(@id, '1_sec_lic_status')]").text
                d['expire_date'] = driver.find_element_by_xpath("//*[contains(@id, '1_expiration_date')]").text
                d['effective_date'] = driver.find_element_by_xpath("//*[contains(@id, '1_issue_date')]").text

                return d

            if len(content_text) <= 9:
                d['first_name'] = f_name
                d['last_name'] = l_name
                d['license_type']='None Found'
                
                return d

        # for scrape_master and all states
        sdata = sdata.astype(str)
        length = int(sdata.shape[0])
    #         print('Number files = ',length)
        if stop == True:
            stop = length

        # Write a file for the output

        file=Results(state)
        file.w(headers)
        result=[]  
        number_files=int(stop-start)
        for i in range(start, stop):
            print('processing '+state+ ': '+str(i+1)+' of '+str(number_files),'\n')
            
            if '--proxy-server' in driver_params:
                if i%loop_break==int(loop_break-1):
                    driver.quit()
                    driver = get_driver(url,driver_params)
                    driver = try_get_url(driver,driver_params,url)
            
            li_number = str(sdata.iloc[i].loc['num'])
            f_name = sdata.iloc[i].loc['fn']
            l_name = sdata.iloc[i].loc['ln']
            driver = try_get_url(driver,driver_params,url)
            time.sleep(pause_time)

            #extract the input parameters for the scraper function
            try:
                
                output=single_scrape(driver, li_number, f_name, l_name)
            except Exception as inst:
                print(li_number, inst, traceback.format_exc())
                result.append((i,inst))
                driver = try_get_url(driver,driver_params,url)
                time.sleep(pause_time)
                continue                

            output['num']=li_number
            output['fn']=f_name
            output['ln']=l_name
            output['state']=state
            result.append((i,output))        
            file.w(output,method='add')

        driver.quit()
        return result

    except Exception as inst:
        print('Scraper Exception - Something bad happened in scraper_function not single_scrape')
        print(inst)
        if 'driver' in locals():
            try:
                driver.quit()
            except:
                pass
        return True,inst

    
##########################################################################################
##########################################################################################
#########################################################################################
### Wyoming
    
def scrape_WY(sdata, driver_params, start=0, stop=True):
    try:
        state='WY'
        url="https://wybomprod.glsuite.us/GLSuiteWeb/Clients/WYBOM/Public/Licenseesearch.aspx?SearchType=Physician"
        driver = get_driver(url,driver_params)
        driver = try_get_url(driver,driver_params,url)
        time.sleep(pause_time)
        def single_scrape(wbd, li_number, f_name, l_name):
            d = feature_dict(state, li_number, f_name, l_name)
            driver=wbd
            #start state specific:
            license_num_box = 'bodyContent_txtLicNum'
            last_name_box_link = 'bodyContent_txtLastName'
            search_button_link = 'bodyContent_btnSubmit'

            # select the license number search box
            license_num_search_box = driver.find_element_by_id(license_num_box)

            # fill search box with license number
            license_num_search_box.clear()
            license_num_search_box.send_keys(li_number)

            # select the lastname search box
            last_name_search_box = driver.find_element_by_id(last_name_box_link)

            # # fill search box with last name
            last_name_search_box.clear()
            last_name_search_box.send_keys(l_name)



            # click the search button
            driver.find_element_by_id(search_button_link).click()
            time.sleep(pause_time)

            content = driver.find_element_by_id('bodyContent_tblResults')
            
            child_headers = content.find_elements_by_css_selector('td')
            
            
            
            if len(child_headers) > 0:
                
                content_header = []
                for i in child_headers:
                    content_header.append(i.text)
                    
                date_str = re.compile('[0-9]*/[0-9]*/[0-9]*')
                
                for i in content_header:
                    if i == 'Licensee Name':
                        d['last_name'] = content_header[content_header.index(i) + 1].split()[0]
                        d['first_name'] = content_header[content_header.index(i) + 1].split()[1]
                    if i == 'License Number':
                        d['License Number'] = content_header[content_header.index(i) + 1] 
                    if i == 'Specialty':
                        d['license_type'] = content_header[content_header.index(i) + 1]
                    if i == 'Date Licensed':
                        d['effective_date'] = content_header[content_header.index(i) + 1]
                    if i == 'License Status':
                        exp_field = content_header[content_header.index(i) + 1]
                        if 'Expired' in exp_field:
                            d['license_status'] = 'Expired'
                        else:
                            interim['license_status'] = 'Active'
                        exp_date = re.findall(date_str,exp_field)
                        d['expire_date'] = exp_date[0]     
                
                return d

            else:
                d['first_name'] = f_name
                d['last_name'] = l_name
                d['license_type']='None Found'
                
                return d
    # for scrape_master and all states
        sdata = sdata.astype(str)
        length = int(sdata.shape[0])
    #         print('Number files = ',length)
        if stop == True:
            stop = length

        # Write a file for the output

        file=Results(state)
        file.w(headers)
        result=[]  
        number_files=int(stop-start)
        for i in range(start, stop):
            print('processing '+state+ ': '+str(i+1)+' of '+str(number_files),'\n')
            
            if '--proxy-server' in driver_params:
                if i%loop_break==int(loop_break-1):
                    driver.quit()
                    driver = get_driver(url,driver_params)
                    driver = try_get_url(driver,driver_params,url)
            
            li_number = str(sdata.iloc[i].loc['num'])
            f_name = sdata.iloc[i].loc['fn']
            l_name = sdata.iloc[i].loc['ln']
            driver = try_get_url(driver,driver_params,url)
            time.sleep(pause_time)

            #extract the input parameters for the scraper function
            try:
                
                output=single_scrape(driver, li_number, f_name, l_name)
            except Exception as inst:
                print(li_number, inst, traceback.format_exc())
                result.append((i,inst))
                driver = try_get_url(driver,driver_params,url)
                time.sleep(pause_time)
                continue                

            output['num']=li_number
            output['fn']=f_name
            output['ln']=l_name
            output['state']=state
            result.append((i,output))        
            file.w(output,method='add')

        driver.quit()
        return result

    except Exception as inst:
        print('Scraper Exception - Something bad happened in scraper_function not single_scrape')
        print(inst)
        if 'driver' in locals():
            try:
                driver.quit()
            except:
                pass
        return True,inst


