suppressPackageStartupMessages(library(readxl))
suppressMessages(install.packages("RSelenium", repos='http://cran.us.r-project.org', dependencies = T))
suppressPackageStartupMessages(library(rvest))
suppressPackageStartupMessages(library(XML))
suppressPackageStartupMessages(library(RSelenium))
suppressPackageStartupMessages(library(RCurl))
suppressPackageStartupMessages(library(httr))
suppressPackageStartupMessages(library(data.table))
suppressPackageStartupMessages(library(jsonlite))
suppressPackageStartupMessages(library(lubridate))
suppressPackageStartupMessages(library(plyr))
suppressPackageStartupMessages(library(stringr))
suppressPackageStartupMessages(library(qdapRegex))
suppressPackageStartupMessages(library(reticulate))

# Source the script containing the functions (make sure file path is accurate)
# This will load a ton of functions into the Environment
function_fn<-"functions/FUNCTIONS_LCM.R"
source(function_fn)

start <- Sys.time()

arg<-commandArgs(trailingOnly=TRUE)

if (length(arg)==1){
    singlestate<-toString(arg)
    dpath = paste0(dirname(getwd()),'/to_scrape*.xlsx')
    scrape_fn<-Sys.glob(dpath)
    all_scrape<-read_excel(scrape_fn, col_types ="text")
    to_scrape<-data.frame(all_scrape[all_scrape$state==singlestate,],stringsAsFactors = FALSE)
}else{
    source_python("stream_data.py")
    data <- stream_data()
    mat<-sapply(data,c)
    col_names<-c(mat[1,])
    data_table<-mat[2:nrow(mat),]
    to_scrape<-data.frame(data_table,stringsAsFactors = FALSE)
}

# to_scrape<-to_scrape[which(to_scrape$VetWeek=='New'),]
# to_scrape=subset(to_scrape, select = -c(VetWeek) )

# Rename these 4 columns and sort by state, remove row numbers
colnames(to_scrape)<-c("num", "state", "fn", "ln")
to_scrape<-to_scrape[order(to_scrape$state),]
row.names(to_scrape)<-NULL
to_scrape<-unique(to_scrape)


is_function = function (expr) {
    if (! is_assign(expr))
        return(FALSE)
    value = expr[[3]]
    is.call(value) && as.character(value[[1]]) == 'function'
}

function_name = function (expr)
    as.character(expr[[2]])

is_assign = function (expr)
    is.call(expr) && as.character(expr[[1]]) %in% c('=', '<-', 'assign')

file_parsed <- parse(function_fn)
functions <- Filter(is_function, file_parsed)
function_names <- unlist(Map(function_name, functions))

scrape_names<-grep("^scrape_", function_names,value=TRUE)
states<-gsub("[^::A-Z::]","", scrape_names)
states<-sort(states)

#For testing purposes
# states<-c("OR")
# states<-c( "AZ", "GA", "IA", "IL", "KS", "KY", "LA", "MA", "MD", "MO", "NC", "NH", "NM", "NV", "OR", "PA", "RI", "TN", "UT", "VT", "WA", "WI")
# states<-c("UT", "VT", "WA", "WI", "WY")
# rst_notworking<-c()
# rst_working<-c()
errors<-c()
messages<-c()

for (state in states){
    state<-gsub(" ", "", state, fixed = TRUE)
    nextflag<<-FALSE
    VOLDEMORT<<-FALSE
    TOMRIDDLE<<-FALSE
    HEWHOSHALLNOTBENAMED<<-FALSE
    attr_name=paste0('scrape_',state)
    out1<-tryCatch({
        get(attr_name)
        },
        error=function(cond) {
            cat(state,'not in R','\n')
            VOLDEMORT<<-TRUE
            return(NA)
        }
        )
    if (VOLDEMORT==TRUE){
        message('skipped to next')
#         rst_notworking<-c(rst_notworking,state)
        next
    }
    scraper<-out1
    state_data<-subset_state(to_scrape, state) # add start and stop if needed 

    if (nrow(state_data)==0){
        next
    }
    
#     eCaps <- list(chromeOptions = list(
#           args = c('--headless', '--no-sandbox', '--disable-dev-shm-usage')
#             ))
    
    portnum<-85L
    PORT_OPEN<-FALSE
    i<-0
    while ((!PORT_OPEN)&(i<20)){
        rD<-tryCatch({
        rd <- rsDriver(port = portnum,browser = "chrome", version = 'latest',chromever = "80.0.3987.16", verbose = FALSE,geckover = NULL)
        PORT_OPEN<<-TRUE
        rd
            },error=function(err){
            Sys.sleep(1)
            portnum<<-portnum+10L
            i<-i+1
        })
        }
    remDr <- rD[["client"]]
    Sys.sleep(5)
    out<-tryCatch(
        {
           output<- scraper(state_data)
           output
        },
        error=function(cond) {
            rD[["server"]]$stop()
            message('There was an error')
            # Choose a return value in case of error
            message(cond)
            TOMRIDDLE<<-TRUE
            error_statement<<-cond
            nextflag<<-TRUE
            return(NA)
        },
        message = function(mes) {
            rD[["server"]]$stop()
            m=paste0(state,' has a message')
            message(m)
            message(mes)
            HEWHOSHALLNOTBENAMED<<-TRUE
            message_statement<<-mes
            nextflag<<-TRUE
#             rst_notworking<-c(rst_notworking,state)
            return(NA) # when we get partial results have return =output)
        }
    )
    rD[["server"]]$stop()
    Sys.sleep(5)
    if (TOMRIDDLE==TRUE){
        state_error<-paste(state,'|',error_statement)
        errors<-c(errors,state_error)
        errorfname<-paste0('stops/error',state,'.txt')
        write.table(state_error,errorfname,sep="|", row.names = FALSE,col.names = TRUE,na="")
    }
    if (HEWHOSHALLNOTBENAMED==TRUE){
        state_message<-paste(state,'|',message_statement)
        messages<-c(messages,state_message)
        messfname<-paste0('stops/message',state,'.txt')
        write.table(state_message,messfname,sep="|", row.names = FALSE,col.names = TRUE,na="")
    }
    if (nextflag==TRUE){
        rD[["server"]]$stop()
        num<-0
        fname<-paste0('results/result',state,'.txt')
        if (file.exists(fname)){
            while (file.exists(fname)){
                num<-num+1
                fname<-paste0('results/result',state,'_',toString(num),'.txt')
            }
        }
        # Add and fix if we can handle partial output
#         write.table(out,fname,sep="|", row.names = FALSE,col.names = TRUE,na="") 
        next
    }
    result<-out
    dtime<-format(Sys.time(), "%Y%m%d")
    num<-0
    fname<-paste0('results/result',state,'_',dtime,'.txt')
    if (file.exists(fname)){
        while (file.exists(fname)){
            num<-num+1
            fname<-paste0('results/result',state,'_',dtime,'_',toString(num),'.txt')
        }
    }
#     rst_working<-c(rst_working,state)
    write.table(result,fname,sep="|", row.names = FALSE,col.names = TRUE,na="")
}

allmessfname<-paste0('stops/allmessages.txt')    
allerrorfname<-paste0('stops/allerrors.txt')
# workingstateresultfname<-paste0('results/R_Scraper_working.txt')
# notworkingstateresultfname<-paste0('results/R_Scraper_notworking.txt')
# write.table(rst_working,workingstateresultfname,sep="|", row.names = FALSE,col.names = TRUE,na="")
# write.table(rst_notworking,notworkingstateresultfname,sep="|", row.names = FALSE,col.names = TRUE,na="")
if (!is.null(errors)){
    write.table(errors,allerrorfname,sep="|", row.names = FALSE,col.names = TRUE,na="") 
}
if (!is.null(messages)){
   write.table(messages,allmessfname,sep="|", row.names = FALSE,col.names = TRUE,na="") 
}

end <- Sys.time()
diff<-c(as.integer(end-start))
fname<-paste0('r_scraper_time(s)','.txt')
write.table(diff,fname)