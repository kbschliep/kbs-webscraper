'''
Runs all python scrapers and outputs to results/ folder
if passing from cmd can pass 2 additional arguments
arg 1 State to run e.g. AZ or az or Az
arg 2 Number of files to run for state. e.g. 5 

'''

__author__ = "Karl Schliep"
__maintainer__ = "Karl Schliep"
__email__ = "kschliep@tistatech.com"


import pandas as pd
import functions.pyscraper_functions as ws
import re
import os, sys
from selenium import webdriver
import time
import glob
import pickle
import datetime

def byte_to_df(bytestream):
    rowdata=[s.split(',')[:4] for s in bytestream.decode().split('\r\n')]
    cols=rowdata[0]
    fill=rowdata[1:]
    data=pd.DataFrame(data=fill,columns=cols).dropna()
    return data

def get_states():
    '''
    Gets all states from the pyscraper_function.py, FUNCTIONS_LCM.r and the crawlers
    returns listaof states for which we currently have scrapers
    '''
    reg_pyscrape=re.compile(r'def scrape_[A-Z]{2}')
    path_pyfunct = os.getcwd()+'/functions/pyscraper_functions.py'
    spyfunct=[]
    with open(path_pyfunct) as fp:
        for cnt, line in enumerate(fp):
            try:
                if '#' in line.split()[0]:
                    continue
            except IndexError:
                continue

            scrape=reg_pyscrape.findall(line)   
            if scrape!=[]:
                scrapers=scrape[0].split('_')[1]
                spyfunct.append(scrapers)

    reg_rscrape=re.compile(r'scrape_[A-Z]{2}')
    path_pyfunct = os.getcwd()+'/functions/FUNCTIONS_LCM.r'
    srfunct=[]
    with open(path_pyfunct) as fp:
        for cnt, line in enumerate(fp):
            try:
                if '#' in line.split()[0]:
                    continue
            except IndexError:
                continue

            scrape=reg_rscrape.findall(line)   
            if scrape!=[]:
                rscrapers=scrape[0].split('_')[1]
                srfunct.append(rscrapers)

    sfunct=sorted(spyfunct+srfunct)
    return sfunct

if len(sys.argv)>=2:
    extra=''
    start_num=0
    state=str(sys.argv[1]).upper()
    dpath = os.path.abspath(os.path.join(os.getcwd(),"..",'*.xlsx'))
    path=glob.glob(dpath)[0]
    # import the state specific scraping function
    funname='scrape_'+state
    try:
        scraper= getattr(ws, funname)
    except AttributeError:
        escaperstr='scrape_'+state+' not in pyscraper_functions module'
        print(escaperstr)
        
    # Make a df of all values for the state
    to_scrape=pd.read_excel(path)
    data=to_scrape.loc[to_scrape.state==state].reset_index(drop=True)
    state_params=data.iloc[:,:].astype(str)
    state_params=state_params.applymap(lambda s: s.upper())
#     to_scrape=pd.read_excel(path)
#     state_params=to_scrape.loc[to_scrape.state==state]
    
    #Iterate over each entry in the state and make a list of the output
    number_files=state_params.shape[0]
    if len(sys.argv)==3:
        
        if state_params.shape[0]<number_files:
            number_files=state_params.shape[0]
        else:
            number_files=number_files=int(sys.argv[2])
            
    if len(sys.argv)==4:
        if sys.argv[2]=='start':
            extra='1'
            number_files=state_params.shape[0]
            start_num=int(sys.argv[3])
            
        else:
            start_num=int(sys.argv[2])
            number_files=int(sys.argv[3])
            if state_params.shape[0]<number_files:
                number_files=state_params.shape[0]
            else:
                number_files=state_params.shape[0]

    #Open a driver
    driver_params=[]#['--headless','--no-sandbox','--disable-gpu']
    #Run the scraper
    print('Starting ', state)
    print('###########')
    try:
        d=scraper(state_params, driver_params, start_num, number_files)
    except Exception as inst:
        print('not working')
        print(inst)  
    matching = []
    try:
        matching =  [s[0] for s in d if not isinstance(s[1], dict) ]# find rows where output is not a dict
    except:
        pass
            
    if matching!=[]:
        print('Errors present in '+state)
        print('Rows with Errors = ')
        print(matching)
        errs=[d[s] for s in matching]
        print(errs)
        dtime=str(datetime.datetime.now().strftime("%Y%m%d"))
        fn=dtime+'_'+state+'_'+dtime+'.txt'
        with open(fn, 'wb') as file:
            file.write(''.join(errs))
    
else:
#     # Gets pathnames for all .csv 
#     dpath = os.path.abspath(os.path.join(os.getcwd(),"..",'*.xlsx'))'
#     path=glob.glob(dpath)[0]
    
    start = time.time()
    
    inputstream = sys.stdin.buffer.read()

    to_scrape=byte_to_df(inputstream)

    out=to_scrape.to_csv(index=False).encode()

    # sys.stdout.buffer.write(out)
    data_states=to_scrape.state.str.upper().unique().tolist()
    states_set=set(data_states)
    scraper_set=set(get_states())
    states=sorted(list(states_set.intersection(scraper_set)))

        # Test States
    #     states=['']

        #Iterate over each unique state in csv and record skipped states to a file
    state_ls=[]
    lang_ls=[]
    stat_ls=[]
    err_ls=[]
    for state in states:     
        # import the state specific scraping function
        state_ls.append(state)
        funname='scrape_'+state
        try:
            scraper= getattr(ws, funname)
        except AttributeError:
            lang_ls.append('nonstandard or R')
            stat_ls.append('')
            err_ls.append('')
            continue

        lang_ls.append('Python')

        # Make a df of all values for the state
        data=to_scrape.loc[to_scrape.state==state].reset_index(drop=True)
        state_params=data.iloc[:,:].astype(str)

        #Iterate over each entry in the state and make a list of the output
        number_files=state_params.shape[0]
        print('\n','-'*50,'\n')
        print('Starting ', state,'\n')

        driver_params=[]#['--headless','--no-sandbox','--disable-gpu']
        start_num=int(0)
        try:
            d=scraper(state_params, driver_params)
        except Exception as e:
            print(e)
            print('Something broke')
        
        if d[0]==True:
            
            fname='stops/message'+state+'.txt'
            with open(fname, 'w') as file:
                writeout=state+','+str(d[1])
                file.write(writeout)
            stat_ls.append('not working')
            err_ls.append(d[1])
            time.sleep(1) 
            continue

        # if element in scraper breaks
        matching = []
        try:
            matching =  [s[0] for s in d if not isinstance(s[1], dict) ]# find rows where output is not a dict
        except:
            pass

        if matching!=[]:
            print('Errors present in '+state)
            print('Rows with Errors = ')
            print(matching)
            stat_ls.append('Mostly Working')
            err_ls.append([d[s] for s in matching])
        else:
            stat_ls.append('Working')
            err_ls.append('none')

        time.sleep(2)   
    
    scraper_status=pd.DataFrame([state_ls,lang_ls,stat_ls,err_ls],index=['State','Language','Status','Errors']).T
    
    dtime=str(datetime.datetime.now().strftime("%Y%m%d"))
    fn = 'scraper_status_'+dtime+'.txt'
    cwd=os.getcwd()
    pf= cwd+'/'+fn
    num=0 
    if os.path.exists(pf):
        while os.path.exists(pf):
            num+=1
            fn = 'scraper_status_'+dtime+'_'+str(num)+'.txt'
            pf= cwd+'/'+fn
    
    scraper_status.to_csv(fn,sep='|',index=False)

    end=time.time()
    
    dtime=str(datetime.datetime.now().strftime("%Y%m%d"))
    fn1 = 'pyscraper_time(s)_'+dtime+'.txt'
    cwd=os.getcwd()
    pf1= cwd+'/'+fn1
    num=0 
    if os.path.exists(pf1):
        while os.path.exists(pf1):
            num+=1
            fn1 = 'pyscraper_time(s)_'+dtime+'_'+str(num)+'.txt'
            pf1= cwd+'/'+fn1
    
    with open(fn1, 'w') as file:
        file.write(str(int(end-start)))
    
# sys.stdout.buffer.write(pickle.dumps(d))