library(readxl)
library(data.table)
function_fn<-"functions/FUNCTIONS_LCM.R"
source(function_fn)

arg = commandArgs(trailingOnly=TRUE)
print(typeof(arg))
singlestate<-arg
if (length(singlestate)==1){

    dpath = paste0(dirname(getwd()),'/to_scrape*.xlsx')
    scrape_fn<-Sys.glob(dpath)
    all_scrape<-read_excel(scrape_fn, col_types ="text")
    to_scrape<-data.frame(all_scrape[all_scrape$state==singlestate,],stringsAsFactors = FALSE)

    }

# Rename these 4 columns and sort by state, remove row numbers
colnames(to_scrape)<-c("num", "state", "fn", "ln")
to_scrape<-to_scrape[order(to_scrape$state),]
row.names(to_scrape)<-NULL
to_scrape<-unique(to_scrape)


is_function = function (expr) {
    if (! is_assign(expr))
        return(FALSE)
    value = expr[[3]]
    is.call(value) && as.character(value[[1]]) == 'function'
}

function_name = function (expr)
    as.character(expr[[2]])

is_assign = function (expr)
    is.call(expr) && as.character(expr[[1]]) %in% c('=', '<-', 'assign')

file_parsed <- parse(function_fn)
functions <- Filter(is_function, file_parsed)
function_names <- unlist(Map(function_name, functions))

scrape_names<-grep("^scrape_", function_names,value=TRUE)
states<-gsub("[^::A-Z::]","", scrape_names)
states<-sort(states)
# states<-c('OR')

for (state in states){

    state_data<-subset_state(to_scrape, state) # add start and stop if needed 
    print(state_data)
    }
    
    
# library(reticulate)
# library(data.table)
# source_python("stream_data.py")
# data <- stream_data()
# mat<-sapply(data,c)
# col_names<-c(mat[1,])
# data_table<-mat[2:nrow(mat),]
# stream_results<-data.frame(data_table,stringsAsFactors = FALSE)
# message(stream_results)
