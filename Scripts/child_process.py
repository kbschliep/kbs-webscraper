import pickle
import sys
import time
import pandas as pd

def byte_to_df(bytestream):
    rowdata=[s.split(',') for s in bytestream.decode().split('\r\n')]
    cols=rowdata[0]
    fill=rowdata[1:]
    data=pd.DataFrame(data=fill,columns=cols).dropna()
    return data

results = sys.stdin.buffer.read()

data=byte_to_df(results)
out=data.to_csv(index=False).encode()

sys.stdout.buffer.write(out)