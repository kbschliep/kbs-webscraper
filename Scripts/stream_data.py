import pandas as pd
import sys
import numpy as np

def stream_data():
    results = sys.stdin.buffer.read()

    rowdata=[s.split(',')[:4] for s in results.decode().split('\r\n')]
    data=pd.DataFrame(data=rowdata).dropna()
    
    return data