'''
Compares unique states in output vs input
If there are differences it prints and saves the differences
'''

__author__ = "Karl Schliep"
__maintainer__ = "Karl Schliep"
__email__ = "kschliep@tistatech.com"
### Date = 1-28-2020
### Version = 4

import os
import glob
import shutil
import time
import subprocess as sub
import pandas as pd
import numpy as np
import datetime
import re
import sys

class cd:
    """Context manager for changing the current working directory"""
    def __init__(self, newPath):
        self.newPath = os.path.expanduser(newPath)

    def __enter__(self):
        self.savedPath = os.getcwd()
        os.chdir(self.newPath)
        return self

    def __exit__(self, etype, value, traceback):
        os.chdir(self.savedPath)

def get_states(by_lang=False):
    '''
    Gets all states from the pyscraper_function.py, FUNCTIONS_LCM.r and the crawlers
    returns listaof states for which we currently have scrapers
    '''
    reg_pyscrape=re.compile(r'def scrape_[A-Z]{2}')
    path_pyfunct = os.getcwd()+'/Scripts/functions/pyscraper_functions.py'
    spyfunct=[]
    with open(path_pyfunct) as fp:
        for cnt, line in enumerate(fp):
            try:
                if '#' in line.split()[0]:
                    continue
            except IndexError:
                continue

            scrape=reg_pyscrape.findall(line)   
            if scrape!=[]:
                scrapers=scrape[0].split('_')[1]
                spyfunct.append(scrapers)

    reg_rscrape=re.compile(r'scrape_[A-Z]{2}')
    path_pyfunct = os.getcwd()+'/Scripts/functions/FUNCTIONS_LCM.R'
    srfunct=[]
    with open(path_pyfunct) as fp:
        for cnt, line in enumerate(fp):
            try:
                if '#' in line.split()[0]:
                    continue
            except IndexError:
                continue

            scrape=reg_rscrape.findall(line)   
            if scrape!=[]:
                rscrapers=scrape[0].split('_')[1]
                srfunct.append(rscrapers)

    sfunct=sorted(spyfunct+srfunct)
    if by_lang==True:
        return sorted(spyfunct),sorted(srfunct)
    return sfunct

def remove_punct(ds1,items):
    ds=ds1.copy()
    pncts='!"#$%&\'()*+,-./:;<=>?@[\\]^_`{}~' # '|' pipe not in here
    punct=''
    if items=='all':
        punct = pncts
    else:
        ls_items=[]
        for item in items:
            if item in pncts:
                ls_items.append(item)
        punct=''.join(ls_items)
    if len(punct)>0:
        transtab = str.maketrans(dict.fromkeys(punct, ''))
        ds = '|'.join(ds.tolist()).translate(transtab).split('|')
    if len(ds1)!=len(ds):
        print('Length mismatch')
        return
    return pd.Series(ds)
def change_num(ds1,filt_types,filts):
    if not isinstance(filt_types,list):
        filt_types=[filt_types]
    if not isinstance(filts,list):
        filts=[filts]
    ds=ds1.copy()
    for ftype,f in zip(filt_types,filts):
        if ftype =='punct':
            ds=remove_punct(ds,f)
        if ftype =='replace':
            ds=ds.str.replace(f[0],f[1])
    return ds

def make_upper(df1,cols):
    df=df1.copy()
    for col in cols:
        df[col]=df[col].astype(str).str.upper().str.strip()
    return df

def make_fullname(df1,cols,name='Fullname'):
    df=df1.copy()
    values = ['']*len(cols)
    d=dict(zip(cols,values))
    df_naf=df.fillna(value=d)
    ccols=df_naf.columns
    df_nafc=make_upper(df_naf,ccols)
    fname=''
    for col in cols:
        fname=fname+df_nafc[col]+' '
    df_nafc[name]=fname
    df_nafc[name]=df_nafc[name].str.replace('  ',' ')
    return df_nafc[name]

def num_check(row,col1,col2):
    left=str(row[col1])
    right=str(row[col2])
    match=False
    if left.upper() in right.upper():
        match=True
    if right.upper() in left.upper():
        match=True
    return match

def name_check(row,lfullname,leftcols,rfullname,rightcols):
    match=False
    lmatches=[]
    for i,lc in enumerate(leftcols):
        lmatch=False
        left=str(row[lc])
        right=str(row[rfullname])

        if left.upper() in right.upper():
            lmatch=True
        if right.upper() in left.upper():
            lmatch=True
        lmatches.append(lmatch)
    if sum(lmatches)/len(leftcols)>.49:
        match=True
    rmatches=[] 
    for i,rc in enumerate(rightcols):
        rmatch=False
        left=str(row[rc])
        right=str(row[lfullname])

        if left.upper() in right.upper():
            rmatch=True
        if right.upper() in left.upper():
            rmatch=True
        rmatches.append(rmatch)
        
    if sum(rmatches)/len(rightcols)>.49:
        match=True
#     if match==False:
#         print(row[['LICENSE_STATE','LICENSE_NUMBER']].tolist())
    return match

# Start Program

dpath = os.getcwd()+'/results/scrape_results*.txt'

path_results=glob.glob(dpath)[-1]
allresults_df=pd.read_csv(path_results, delimiter="|")

results_df=allresults_df[~allresults_df.SbwLicStatus.isnull()].copy()
missed_df=allresults_df[allresults_df.SbwLicStatus.isnull()].copy()
# results_df=allresults_df.copy()
states_results=results_df.LICENSE_STATE.str.upper().unique().tolist()

dpath = os.getcwd()+'\\'+'to_scrape*.xlsx'
path_orig=glob.glob(dpath)[0]
orig_df=pd.read_excel(path_orig)
states_orig=orig_df.state.str.upper().unique().tolist()

diff=orig_df.groupby('state').count().num.subtract(results_df.groupby('LICENSE_STATE').count().LICENSE_NUMBER)
states_not_in_results=diff.index[diff.isna()].to_list()

sfunct = get_states()
sorig = set(states_orig).intersection(set(sfunct))
sresult = set(states_results)
state_diff = list(sorig-sresult)


missed_data=missed_df.loc[missed_df.LICENSE_STATE.isin(sorig)].sort_values('LICENSE_STATE').reset_index(drop=True)
results_data=results_df.loc[results_df.LICENSE_STATE.isin(sorig)]
total_scrapable=allresults_df.loc[allresults_df.LICENSE_STATE.isin(sorig)]
total_data=allresults_df.loc[allresults_df.LICENSE_STATE.isin(sorig)].sort_values('LICENSE_STATE').reset_index(drop=True)

scrape_numbers=results_data.LICENSE_STATE.value_counts().sort_index().to_list()
scrape_results=results_data.LICENSE_STATE.value_counts().sort_index().index.to_list()
total_numbers=total_data.LICENSE_STATE.value_counts().sort_index().to_list()
total_results=total_data.LICENSE_STATE.value_counts().sort_index().index.to_list()
missed_results=missed_data.LICENSE_STATE.value_counts().sort_index().index.to_list()
missed_numbers=missed_data.LICENSE_STATE.value_counts().sort_index().to_list()
missed=[]
for i,state in enumerate(total_results):
    found=False
    for j,state1 in enumerate(scrape_results):
        if state1==state:
            found=True
            missed.append((state ,str(scrape_numbers[j]) ,str(total_numbers[i]-scrape_numbers[j]) ,str(total_numbers[i]) ,
                           str(round((scrape_numbers[j]/total_numbers[i])*100))+'%' , str(round(((total_numbers[i]-scrape_numbers[j])/total_numbers[i])*100))+'%'))
            break
        else:
            pass
        
    if found==False:
        missed.append((state ,str(0) ,str(total_numbers[i]) ,str(total_numbers[i]) ,str(0)+'%' ,str(100)+'%'))

missed.sort(key = lambda x: int(x[2]),reverse=True)
missed.insert(0,('state','# Scraped','# Missed','Total','% Scraped','% Missed'))
# missed=[(missed_results[i], total_numbers[i]-missed_numbers[i],total_numbers[i],str(round((total_numbers[i]-missed_numbers[i])/total_numbers[i]*100))+'%') for i in range(0, len(missed_results))] 

results_df['ALERT_NUM']=change_num(results_df.LICENSE_NUMBER,['punct','replace','replace'],['all',('\s',''),('(?<=\D)0+|^0+','')])
results_df['SCRAPE_NUM']=change_num(results_df.SbwLicNum,['punct','replace','replace'],['all',('\s',''),('(?<=\D)0+|^0+','')])
results_df['NUM_CHECK']=results_df.apply(num_check,args=['ALERT_NUM','SCRAPE_NUM'],axis=1)
num_check_fail=results_df.loc[results_df.NUM_CHECK==False].groupby('LICENSE_STATE').count()['NUM_CHECK']

state_num_fail=num_check_fail.index.tolist()
count_num_fail=num_check_fail.tolist()
num_failed=[(state_num_fail[i],count_num_fail[i]) for i in range(0,len(state_num_fail))]

cols=['APS_FIRSTNAME','APS_LASTNAME']
scols=['StateBoardFirst','StateBoardMiddle','StateBoardLast']
results_df['ALERT_FULLNAME']=make_fullname(results_df,cols)
results_df['SCRAPE_FULLNAME']=make_fullname(results_df,scols)
results_df['NAME_CHECK'] = results_df.apply(name_check,args=['ALERT_FULLNAME',cols,'SCRAPE_FULLNAME',scols],axis=1)

name_check_fail=results_df.loc[results_df.NAME_CHECK==False].groupby('LICENSE_STATE').count()['NAME_CHECK']
state_name_fail=name_check_fail.index.tolist()
count_name_fail=name_check_fail.tolist()
        
name_failed=[(state_name_fail[i],count_name_fail[i]) for i in range(0,len(state_name_fail))]

metrics=[('Total Records',str(orig_df.shape[0])),
         ('Total Scrapable',str(total_scrapable.shape[0])),
         ('Total Not Scrapable', str(orig_df.shape[0]-total_scrapable.shape[0])),
         ('Total Scrape Found',str(results_data.shape[0])),
         ('Total Not Found',str(missed_data.shape[0])),
         ('Total RPA Found',str(results_df.shape[0]-results_data.shape[0])),
         ('Total Found',str(results_df.shape[0])),
         ('Percent Scraped',str(round(results_data.shape[0]/total_scrapable.shape[0]*100,1))),
         ('Total Percent',str(round(results_df.shape[0]/orig_df.shape[0]*100,1)))  
         ]

if num_failed!=[]:
    print('Number Match Failed')
    for i in num_failed:
        print(i)

print('\n','-'*20,'\n')
        
if name_failed!=[]:
    print('Name Match failed')
    for i in name_failed:
        print(i)
        
print('\n','-'*20,'\n')
        
if state_diff==[]:
    print('All states accounted for!')
    if len(missed)==0:
        print('All records accounted for!')
    else:
        print('Alert records not accounted for')
        for i in missed:
            print(f'{i[0]:^10}',f'{i[1]:^10}',f'{i[2]:^10}',f'{i[3]:^10}',f'{i[4]:^10}',f'{i[5]:^10}')
        
        for i,met in enumerate(metrics):
            if i>=len(metrics)-2:
                print(f'{met[0]:<24}',f'=',f'{met[1]:>12}','%')
            else:
                print(f'{met[0]:<24}',f'=',f'{met[1]:>12}')
else:
    print('Alert ', state_diff, ' in original but not in results')
    if len(missed)==0:
        print('All records accounted for!')
    else:
        print('Alert records not gotten')
        for i in missed:
            print(f'{i[0]:^10}',f'{i[1]:^10}',f'{i[2]:^10}',f'{i[3]:^10}',f'{i[4]:^10}',f'{i[5]:^10}')
        
        print('-'*20)
        
        for i,met in enumerate(metrics):
            if i>=len(metrics)-2:
                print(f'{met[0]:<24}',f'=',f'{met[1]:>12}','%')
            else:
                print(f'{met[0]:<24}',f'=',f'{met[1]:>12}')

if missed_data.shape[0]>1:
    dtime=str(datetime.datetime.now().strftime("%Y%m%d"))
    outfn='missed_results_'+dtime+'.txt'
    cwd=os.getcwd()
    outpn=cwd+'/'+outfn
    num=0
    if os.path.exists(outpn):
        while os.path.exists(outpn):
            num+=1
            outfn = 'missed_results_'+dtime+'_'+str(num)+'.txt'
            outpn=cwd+'/'+outfn
    missed_data.to_csv(outfn,sep='|',index=False)

dtime=str(datetime.datetime.now().strftime("%Y%m%d"))
outfn='coverage_'+dtime+'.txt'
cwd=os.getcwd()
outpn=cwd+'/'+outfn
num=0
if os.path.exists(outpn):
    while os.path.exists(outpn):
        num+=1
        outfn = 'coverage_'+dtime+'_'+str(num)+'.txt'
        outpn=cwd+'/'+outfn
with open(outfn,'w') as f:
    for row in missed:
        f.write(','.join(row))
        f.write('\n')
        
dtime=str(datetime.datetime.now().strftime("%Y%m%d"))
outfn='metrics_'+dtime+'.txt'
cwd=os.getcwd()
outpn=cwd+'/'+outfn
num=0
if os.path.exists(outpn):
    while os.path.exists(outpn):
        num+=1
        outfn = 'metrics_'+dtime+'_'+str(num)+'.txt'
        outpn=cwd+'/'+outfn
with open(outfn,'w') as f:
    for row in metrics:
        f.write(','.join(row))
        f.write('\n')