'''
Looks for data in all the nested folders and combines it.
Works for txt and csv data
If multiple people it takes the newest results added (later sorted)
E.g MD vs MD_nurses -duplicates taken from MD_nurses since it is sorted later than MD
'''

__author__ = "Karl Schliep"
__maintainer__ = "Karl Schliep"
__email__ = "kschliep@tistatech.com"
### Date = 12-9-2019
### Version = 4

import os, glob, re
import pandas as pd
import datetime
import numpy as np
from io import StringIO
# Get data from result locations
path = os.getcwd()+'/Scripts/results/*.txt' # Change '' as needed
file_list=sorted(glob.glob(path))
path1 = os.getcwd()+'/Scripts/results/*.csv' # Change '' as needed
file_list2=sorted(glob.glob(path1))

# combine results into 1 big list
all_list=sorted(file_list+file_list2)
r = re.compile('.+\\\\result[A-Z]{2}.+(txt|csv)$') # searches for anything_12#a!2_of_the_form\\result$$_asd23.txt(csv)
filt_list = list(filter(r.match, all_list))
p_list=[x.split('\\')[-1] for x in filt_list]
# print(p_list)
# loop through all files and append into a big list of dataframes
dfs=[]
for i,file in enumerate(filt_list):
    print(i,file.split('\\')[-1])
    with open(file,'rb') as f:
        r=StringIO(f.read().decode('utf-8').replace('"',''))
    try:
        df=pd.read_csv(r, delimiter="|", dtype={'num': object,'SbwLicNum':object},engine='python',index_col=False)
        assert df.shape[1] == 15
        dfs.append(df)
    except (AssertionError,UnicodeDecodeError) as e:
#         print(e)
        try:
            dfs.append(pd.read_csv(file, delimiter="|", dtype={'num': object,'SbwLicNum':object},engine='python',encoding='unicode_escape',index_col=False))
        except Exception as e:
            print(file,'Had an exception')
            print(e)
        continue
        
# combine data and drop duplicates based on license number, state, first name, and last name
# only exact matches of those 4 are replaced taken the last value read
# that is data that was appended later on MD vs MD_nurses, nurses data is kept
merged_dfs=pd.concat(dfs,sort=False).reset_index(drop=True)
merged_dfs['SbwLicStatus']=merged_dfs['SbwLicStatus'].fillna('ZZZZZ')
sort_merged_dfs=merged_dfs.sort_values(by=['state','num','fn','ln','SbwLicStatus'])
# =merged_dfs.loc[~merged_dfs.duplicated(subset=['num','state','fn','ln'],keep=False)]
# dups=merged_dfs.loc[merged_dfs.duplicated(subset=['num','state','fn','ln'],keep=False)]
# dups_status=dups.loc[~dups.SbwLicStatus.isnull()].drop_duplicates(subset=['num','state','fn','ln'],keep=False)
# dups_no_status=dups.loc[dups.SbwLicStatus.isnull()].drop_duplicates(subset=['num','state','fn','ln'])
# unique_dfs=pd.concat([no_dups,dups_status,dups_no_status]).drop_duplicates(subset=['num','state','fn','ln'],keep='first').sort_values(by=['state'])
unique_dfs=sort_merged_dfs.drop_duplicates(subset=['num','state','fn','ln'],keep='first').replace('ZZZZZ',np.nan)

dpath = os.getcwd()+'\\'+'to_scrape*.xlsx'
path_orig=glob.glob(dpath)[0]
orig_df=pd.read_excel(path_orig).dropna(how='all',axis=1).astype(str).iloc[:,:4]

resulting_df=pd.merge(orig_df,unique_dfs,how='left').sort_values(by=['state','num'])

# Renames columns and formats for output, saved to results folder
renamer={"num":"LICENSE_NUMBER","state":"LICENSE_STATE","fn":"APS_FIRSTNAME","ln":"APS_LASTNAME"}
formatted_df=resulting_df.rename(columns=renamer).fillna('').astype(str).iloc[:,:15]
# formatted_df.SbwLicNum=formatted_df.SbwLicNum.str.replace("\.0$","")
dtime=str(datetime.datetime.now().strftime("%Y%m%d"))
outfn='results\scrape_results_'+dtime+'.txt'
cwd=os.getcwd()
outpn=cwd+'/'+outfn
num=0
if os.path.exists(outpn):
    while os.path.exists(outpn):
        num+=1
        outfn = 'results\scrape_results_'+dtime+'_'+str(num)+'.txt'
        outpn=cwd+'/'+outfn
        
formatted_df.to_csv(outfn,sep='|',index=False)