#### rescrape   
## Created by Karl Schliep
## email:kschliep@tistatech.com
## 11/1/2019
''' Program to clear old results to old folders and initiate both python and r scripts'''

import os
import glob
import shutil
import time
import subprocess as sub
import pandas as pd
import numpy as np
import datetime
import re
import sys

def get_states(by_lang=False):
    '''
    Gets all states from the pyscraper_function.py, FUNCTIONS_LCM.r and the crawlers
    returns listaof states for which we currently have scrapers
    '''
    reg_pyscrape=re.compile(r'def scrape_[A-Z]{2}')
    path_pyfunct = os.getcwd()+'/Scripts/functions/pyscraper_functions.py'
    spyfunct=[]
    with open(path_pyfunct) as fp:
        for cnt, line in enumerate(fp):
            try:
                if '#' in line.split()[0]:
                    continue
            except IndexError:
                continue

            scrape=reg_pyscrape.findall(line)   
            if scrape!=[]:
                scrapers=scrape[0].split('_')[1]
                spyfunct.append(scrapers)

    reg_rscrape=re.compile(r'scrape_[A-Z]{2}')
    path_pyfunct = os.getcwd()+'/Scripts/functions/FUNCTIONS_LCM.R'
    srfunct=[]
    with open(path_pyfunct) as fp:
        for cnt, line in enumerate(fp):
            try:
                if '#' in line.split()[0]:
                    continue
            except IndexError:
                continue

            scrape=reg_rscrape.findall(line)   
            if scrape!=[]:
                rscrapers=scrape[0].split('_')[1]
                srfunct.append(rscrapers)

    sfunct=sorted(spyfunct+srfunct)
    if by_lang==True:
        return sorted(spyfunct),sorted(srfunct)
    return sfunct

def find_files(location, criteria):
    betterloc=repr(location)
    if ('\\\\' not in betterloc):
        location=str(betterloc).replace("'",'').replace('"','')
    folder_path = os.getcwd()+'/'+location
    if (folder_path[-1]!='\\') or (folder_path[-1]!='/'):
        folder_path=folder_path+'/'
    criterionpath=folder_path+criteria

    file_paths=glob.glob(criterionpath)
    if file_paths ==[]:
        print('No files found at '+criterionpath)
        return None
    return file_paths

def move_if_exist(location,criteria,verbose=True):
    list_of_filepaths=find_files(location,criteria)
    verbose_ls=[]
    if list_of_filepaths==None:
        return
    for file in list_of_filepaths:
        pathfolder=os.path.dirname(file)
        filename=os.path.basename(file)
        filen=filename.split('.')[0]
        filex=filename.split('.')[1]
        num=0
        if os.path.exists(file):
            newpathfile=pathfolder+'/old/'+filename
            if os.path.exists(newpathfile):
                while os.path.exists(newpathfile):
                    num+=1
                    newpathfile=pathfolder+'/old/'+filen+'_'+str(num)+'.'+filex
                    
            verbose_ls.append(filename+' moved to '+os.path.dirname(newpathfile))
            shutil.move(file,newpathfile)
            while not os.path.exists(newpathfile):
                time.sleep(.1)
    if verbose==True:
        for item in verbose_ls:
            print(item)
            
def moveto_if_exist(location,destination,criteria,verbose=True):
    list_of_filepaths=find_files(location,criteria)
    verbose_ls=[]
    if list_of_filepaths==None:
        return
    for file in list_of_filepaths:
        pathfolder=os.path.dirname(file)
        filename=os.path.basename(file)
        if os.path.exists(file):
            if not os.path.exists(pathfolder):
                os.mkdir(pathfolder)
            newpathfile=os.getcwd()+'/'+destination+filename
            verbose_ls.append(filename+' moved to '+os.path.dirname(newpathfile))
            shutil.move(file,newpathfile)
            while not os.path.exists(newpathfile):
                time.sleep(.1)
    if verbose==True:
        for item in verbose_ls:
            print(item)
            
class cd:
    """Context manager for changing the current working directory"""
    def __init__(self, newPath):
        self.newPath = os.path.expanduser(newPath)

    def __enter__(self):
        self.savedPath = os.getcwd()
        os.chdir(self.newPath)
        return self

    def __exit__(self, etype, value, traceback):
        os.chdir(self.savedPath)
        
divisions = 3
stateswewant=get_states()
pystates,rstates=get_states(by_lang=True)

input1=''
if len(sys.argv)>1:
    try:
        int(sys.argv[1])
        input1='number'
    except ValueError:
        input1='string'
    
if input1=='string':
    if sys.argv[1] in get_states():
        stateswewant=[sys.argv[1]]
    else:
        int(sys.argv[1])
if input1=='number':
    divisions=int(sys.argv[1])

if len(sys.argv)==3:
    divisions=int(sys.argv[2])
    stateswewant=[sys.argv[1]]
    
dpath_retry = os.getcwd()+'\missed_results*.txt'

path_results=glob.glob(dpath_retry)[-1]
missed_data_df=pd.read_csv(path_results,delimiter='|')
renamer={"LICENSE_NUMBER":"num","LICENSE_STATE":"state","APS_FIRSTNAME":"fn","APS_LASTNAME":"ln"}
missed_data_df=missed_data_df.rename(columns=renamer)

missed_data=missed_data_df[missed_data_df.state.isin(stateswewant)].copy()

if missed_data.shape[0]>0:
    div_data=np.array_split(missed_data,divisions)

    child_processpy=[]
    child_processr=[]
    print('Number of divisions = ',str(len(div_data)))
    for i,div in enumerate(div_data):
        with cd('Scripts') as cwd:
            print(i,div.state.unique())
            print('R scraping =',len(set(div.state.unique()).intersection(rstates))>0)
            if len(set(div.state.unique()).intersection(rstates))>0:
                child_processr.append(sub.Popen(['rscript','r_state_scrape.r'], stdin=sub.PIPE))
                child_processr[-1].stdin.write(div.to_csv(index=False).encode())
                child_processr[-1].stdin.close()
                time.sleep(3)
            print('Py scraping =',len(set(div.state.unique()).intersection(pystates))>0)
            if len(set(div.state.unique()).intersection(pystates))>0:
                child_processpy.append(sub.Popen(['python','state_scrape.py'], stdin=sub.PIPE))
                child_processpy[-1].stdin.write(div.to_csv(index=False).encode())
                child_processpy[-1].stdin.close()
                time.sleep(3)

    for cp in child_processpy:
        cp.wait()
        cp.kill()

    for cp in child_processr:
        cp.wait()
        cp.kill()

p3=sub.Popen(['python','merge_results.py'])
p3.wait()
p4=sub.Popen(['python','check_results.py'])
p4.wait()

# moveto_if_exist('','old_data/','*.xlsx')