import os
import glob
import shutil
import time
import subprocess as sub
import pandas as pd
import pickle
dpath = os.getcwd()+'\\'+'to_scrape*.xlsx'
path=glob.glob(dpath)[0]
to_scrape=pd.read_excel(path)
with cd('Scripts') as cwd:
    result = sub.Popen(['python', 'child_process.py'], input=pickle.dumps(to_scrape), stdout=sub.PIPE, stderr=sub.PIPE)
returned_df = pickle.loads(result.stdout)
print(returned_df)