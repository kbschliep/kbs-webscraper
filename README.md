# LCM Vetting
-----------------

### Introduction
This program contains _(__old__ both R and)_ Python scripts made to scrape the states medical websites of information on medical professionals. 

### How to Use
 
`python SCRAPE_MASTER.py`

divides the input data into 3 (default) and calls each subprocess 3 times (6 total process, 3 R and 3 Py)

`python SCRAPE_MASTER.py IL 5 ` Divides input data where state is IL into 5 and runs 5 subprocess (only in R)

_(__old__ 
`rscript r_state_scrape.r WY` Runs the scrape_WY function from functions/FUNCTIONS_LCM.r (can't take number or divisions) )_

### Installation

This program requires many functions. The easiest way to get it to work is to use conda environments

Using Anaconda prompt or a CMD with conda in the environment PATH type

`conda update --all` 

to update conda (you will be asked if you want to make changes hit y and then enter).


#### _(__OLD__) Method 1_

Create environment from .yml file. Type

`conda env create -f webscraper.yml` 

into the Anaconda Prompt (you will be asked if you want to make changes hit y and then enter). Make sure you're operating from the folder containing webscraper.yml or you can change that to the path to the webscraper.yml file. 

Once installed you can activate it by typing

`activate webscraper`

#### Method 2

Create environment from scratch. Type

`conda create -n webscraper python=3.7.1 pip`

that will create a base environment. Then activate the environment. Type

`activate webscraper`

Now add all the packages you need in the environment 

The python packages. Type

`conda install pandas numpy beautifulsoup4 requests xlrd lxml selenium`

when asked hit y

Again type

`conda install -c conda-forge tesserocr`


_I hope that's all of them_

_(__OLD__)_
The R packages. Type

`conda install -c conda-forge r-readxl r-rvest r-xml r-rcurl r-httr r-data.table r-jsonlite r-lubridate r-plyr r-stringr r-qdapregex r-reticulate r-catools`

when asked hit y

The last thing needed to run the files is to have chromedriver.exe installed in your environment.

Download chromedriver from http://chromedriver.chromium.org/ that matches your version of chrome and extract the zipped file.(click the three vertical dots in the top right corner of the browser and go to help-> About Google Chrome. or go to chrome://settings/help. The Chrome version will look like Version 76.0.3809.100 (Official Build) (64-bit))

Inside you will find a chromedriver.exe file which you need to move to the newly created conda environment.

My chromedriver.exe is located in C:\Users\KarlSchliep\Anaconda3\envs\webscraper

#### NOTE
Inside Scripts/r_state_scrape.R on line 117 the chrome version needs to match your chrome version. 

### Details
The structure is the SCRAPE_MASTER.py file calls subprocesses to run R and Python based scraping functions. These subprocesses (state_scrape.py and r_state_scrape.r) call functions (pyfunctions.py and FUNCTIONS_LCM.r) to scrape the data sent to them via stdin (works for Windows). The SCRAPE_MASTER.py file is called from the cmd line and can take inputs of a single number (the number of times you want to divide the work into separate calls to the functions) or a state code to scrape a single state (can add a number after to change the number of divisions, default is 3).  Similarly both subprocesses files can be called by states.

### Contributing
The current goals of this project is to convert all R-based webscrapers into Python webscrapers. __DONE__
This can be done by looking at the FUNCTIONS_LCM.R in Scripts\functions\ and finding a scraper that you want to convert.
The new python-based scraper will be added as a function in pyscraper_functions.py also in Scripts\functions\.
The easiest way to add contributions to the pyscraper_functions.py is to copy and paste an entire function. For example

```# state of DELAWARE
# we can add names attribute directly from variable 'names'

def scrape_DE(sdata, driver_params, start=0, stop=True):

    try:
        state='DE'
        url = "https://dpronline.delaware.gov/mylicense%20weblookup/Search.aspx"
        
        # Function to create driver with given driver parameters
        driver = get_driver(url,driver_params)
        
        # Physician webscraping function within state webscraping function
        # There can be different function for different license types that 
        # follow the same function layout.
        def single_scrape(wbd, li_number, f_name, l_name):
            
            # Initialize output dictionary d
            d = feature_dict(state, li_number, f_name, l_name)
            # Assign input webdriver to variable driver
            driver=wbd

            # define strings used in searching for person
            license_num_box_link = 'licenseNumberCore'
            #             first_name_box_link = 'firstName'
            #             last_name_box_link = 'lastName'
            search_button_link = '#command > fieldset > p.buttons > input[type=submit]:nth-child(1)'

            # select the license number search box
            license_num_search_box = driver.find_element_by_name(license_num_box_link)

            # fill search box with license number
            license_num_search_box.clear()
            license_num_search_box.send_keys(li_number_core)

            # select the license number search box
            license_num_search_box1 = driver.find_element_by_name(license_num_box_link1)

            # click the search button
            # driver.find_element_by_css_selector(search_button_link).click()
            time.sleep(pause_time)
            
            ############################################################
            ######## All the scraping specific stuff with checks #######
            ############################################################
            searches=[li_number,f_name+' '+l_name,f_name[0]+f_name[1:].lower()+' '+l_name[0]+l_name[1:].lower(),f_name,f_name[0]+f_name[1:].lower()]
            found =False
            for name in searches:
            #                 print(name)
                try:
                    linkfound=driver.find_element_by_partial_link_text(name)
                    link = linkfound.get_attribute('href')
                    driver.get(link)
                    time.sleep(pause_time)
                    found=True
                    break
                except (NoSuchElementException, WebDriverException) as e:
            #                     print(e)
                    pass
            
            # If nothing found return dictionary output
            if not found:
                d['license_type']='None Found'
                return d

            # Pull license info from table into content
            content=driver.find_element_by_css_selector('#main > table').text.split('\n')
            
            # look through every row of content and if some keyword exists pull the information after it.
            # I found this is the best way to ensure only scraping the information you want in case
            # something changes.
            for c in content:
                if 'Name:' in c:
                    fullname=c.split(':')[-1].strip().upper()
                    d['last_name']=fullname.split()[-1].strip()
                    d['first_name']=fullname.split()[0].strip()

                # Extract License type
                if "License Type:" in c:
                    d['license_type'] = c.split(':')[-1].strip().upper()

                if "License Number:" in c:
                    d['license_number'] = c.split(':')[-1].strip().upper()

                if "License Status:" in c:
                    d['license_status'] = c.split(':')[-1].strip().upper()

                if "Original Issue Date:" in c:
                    d['effective_date'] = c.split(':')[-1].strip().upper()

                # Extract expire date
                if "Expiration Date:" in c:
                    d['expire_date'] = c.split(':')[-1].strip().upper()

                # board actions
                if "Agency and Disciplinary Action" in c:
                    d['board_sanction'] = c.split(':')[-1].strip().upper()
            
            # Verify name is orrect
            if (f_name not in fullname) and (l_name not in fullname):
                d=feature_dict(state, li_number, f_name, l_name)
                d['license_type']='None found'
                return d
            
            # function to copy html of webpage and save it
            save_info(driver,state,li_number)
            return d
                                    
        #########################################
        #### scrape_DE actual script ###########
        #######################################
        
        # Read in data as str
        sdata = sdata.astype(str)
        length = int(sdata.shape[0])
        if stop == True:
            stop = length

        # Write a file for the output
        # Calls Results class and opens or creates a file
        file=Results(state)
        # calls "w" funciton in Results to write to file
        file.w(headers)
        
        # Initiates output back to state_scrape.py
        result=[]  
        number_files=int(stop-start)
        
        # Iterate through each record in file
        for i in range(start, stop):
            print('processing '+state+ ': '+str(i+1)+' of '+str(number_files),'\n')
            
            # If website fails to load it tries a proxy
            if '--proxy-server' in driver_params:
                if i%loop_break==int(loop_break-1):
                    driver.quit()
                    driver = get_driver(url,driver_params)
                    driver = try_get_url(driver,driver_params,url)
            
            li_number = str(sdata.iloc[i].loc['num'])
            f_name = sdata.iloc[i].loc['fn']
            l_name = sdata.iloc[i].loc['ln']
            
            # Try to load the website
            driver = try_get_url(driver,driver_params,url)
            time.sleep(pause_time)
            
            # Try to run the single scrape function, output is dictionary of scraped data to write
            try:
                output=single_scrape(driver, li_number, f_name, l_name)
            except Exception as inst:
                # If there are errors in the single_scrape functions these are passed to result list
                # the driver is sent back to the url landing page and we continue to the next record
                print(inst)
                result.append((i,inst))
                driver = try_get_url(driver,driver_params,url)
                time.sleep(pause_time)
                continue
                
            # Add original searched info to output dictionary
            output['num']=li_number
            output['fn']=f_name
            output['ln']=l_name
            output['state']=state
            result.append((i,output))        
            file.w(output,method='add')

        driver.quit()
        return result
    
    # If error exists in scrape_DE this catches it and closes things so the next scraper will work
    # this also returns that the function failed to state_scrape.py to write an error note
    except Exception as inst:
        print('Scraper Exception - Something bad happened in scraper_function not single_scrape')
        print(inst)
        if 'driver' in locals():
            try:
                driver.quit()
            except:
                pass
        return True,inst    ```